// Confuzzler.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <string>
#include <cstring>
#include <algorithm>
#include <cassert>
#include <random>
#include <unordered_set>
#include <set>
#include <format>
#include <map>
#include <iostream>
#include <fstream>

#include "llvmGeneration.h"
#include "ILDataManager.h"

ILDataManager iLDataManager;

namespace {
	using prime_type = uint32_t;
}

static const prime_type Prime_array[] = {
	 2 ,    3 ,    5 ,    7,     11,     13,     17,     19,     23,     29,
	 31,    37,    41,    43,    47,     53,     59,     61,     67,     71,
	 73,    79,    83,    89,    97,    101,    103,    107,    109,    113,
	127,   131,   137,   139,   149,    151,    157,    163,    167,    173,
	179,   181,   191,   193,   197,    199,    211,    223,    227,    229,
	233,   239,   241,   251,   257,    263,    269,    271,    277,    281,
	283,   293,   307,   311,   313,    317,    331,    337,    347,    349,
	353,   359,   367,   373,   379,    383,    389,    397,    401,    409,
	419,   421,   431,   433,   439,    443,    449,    457,    461,    463,
	467,   479,   487,   491,   499,    503,    509,    521,    523,    541,
	547,   557,   563,   569,   571,    577,    587,    593,    599,    601,
	607,   613,   617,   619,   631,    641,    643,    647,    653,    659,
	661,   673,   677,   683,   691,    701,    709,    719,    727,    733,
	739,   743,   751,   757,   761,    769,    773,    787,    797,    809,
	811,   821,   823,   827,   829,    839,    853,    857,    859,    863,
	877,   881,   883,   887,   907,    911,    919,    929,    937,    941,
	947,   953,   967,   971,   977,    983,    991,    997
};


class Logger {
public:
	bool disabled = false;
	void StatusMessage(std::string message);
	void ErrorMessage(std::string message);

	void ToggleLogging()
	{
		disabled = !disabled;
	}
};


void Logger::StatusMessage(std::string message) {
	if (disabled) return;
	std::cout << "[*] " << message << std::endl;
}

void Logger::ErrorMessage(std::string message) {
	if (disabled) return;
	std::cout << "[!] " << message << std::endl;
}


std::string getLineNumbersString(std::string key);
void editLocalVariables(const std::vector<ILDataManager::functionData>* functionDataVec);
void printHelper(std::vector<std::string> confuzzedOutput);
int main();
std::string obfusZero();
prime_type getPrime(prime_type DifferentFrom);


std::vector<int> captureVariablesFromLine(std::string line)
{
	std::vector<int> captures;
	std::string workingCapture;
	bool capturing = false;
	for (int i = 0; i < line.size(); i++)
	{
		auto currentChar = line[i];
		if (capturing)
		{
			if (!std::isdigit(currentChar))
			{
				captures.push_back(std::atoi(workingCapture.c_str()));
				workingCapture.clear();
				capturing = false;
				continue;
			}
			workingCapture.push_back(currentChar);
		}
		if (currentChar == '%')
		{
			capturing = true;
		}
	}
	return captures;
}

struct IlVariableOccurence
{
	int number;
	int length;
	int position;
};


std::vector<IlVariableOccurence> captureVariablesAndPositionsFromLine(std::string line)
{
	std::vector<IlVariableOccurence> captures;
	std::string workingCapture;
	bool capturing = false;
	int capturePosition = 0;
	int captureLength = 0;
	for (int i = 0; i < line.size(); i++)
	{
		auto currentChar = line[i];
		if (capturing)
		{
			if (!std::isdigit(currentChar))
			{
				// Only push captures that contain at least one digit after the '%'
				if (captureLength > 0)
				{
					captures.push_back(IlVariableOccurence{ std::atoi(workingCapture.c_str()), captureLength, capturePosition });
				}
				workingCapture.clear();
				capturing = false;
				capturePosition = 0;
				captureLength = 0;
				continue;
			}
			workingCapture.push_back(currentChar);
			captureLength++;
		}
		if (currentChar == '%')
		{
			capturing = true;
			capturePosition = i;
		}
	}
	return captures;
}

bool isReturnLine(std::string line)
{
	std::string reading;
	for (int i = 0; i < line.size(); i++)
	{
		auto currentChar = line[i];
		if (std::isalnum(currentChar))
		{
			reading.push_back(currentChar);
		}
		if (reading.size() == 3)
		{
			if (reading == "ret")
			{
				return true;
			}
			return false;
		}
	}
	return false;

}

std::string llvmAddressFromInt(int i)
{
	return std::format("%{}", i);
}

void editLocalVariables(const std::vector<ILDataManager::functionData>* functionDataVec) {

	auto FunctionDataVec = *functionDataVec;

	// Loop through the Functions in REVERSE, since we insert new lines!!
	for (int i = FunctionDataVec.size() - 1; i > -1; i--)
	{
		auto functionData = FunctionDataVec[i];
		int returnLine = functionData.functionReturnLine;
		int functionStart = functionData.functionBodyStartLine;


		std::vector<ILDataManager::keywordData> keywordDataVec = iLDataManager.lookupTable["alloca"];

		std::unordered_set<int> uniqueVariables;

		for (int j = functionStart; j < iLDataManager.IlLines.size(); j++)
		{
			if (j == returnLine)
			{
				// Only work on the first function for now -- stop looping at the first ret command
				break;

			}

			std::string line = iLDataManager.IlLines[j];

			std::vector<int> captures = captureVariablesFromLine(line);

			for (int k = 0; k < captures.size(); k++)
			{
				uniqueVariables.insert(captures[k]);
			}

		}

		// Assign fake variable a number that is 1 more than the highest real variable
		int newAddress;
		std::vector<int> VariableIntegers;
		for (auto x : uniqueVariables) {
			VariableIntegers.push_back(x);
		}
		auto HighestVariable = VariableIntegers.back();
		newAddress = HighestVariable + 1;


		// Get a range for fake variables
		std::mt19937 mt;
		std::uniform_int_distribution<int> fakeVariableRange(1, 10);
		int numberOfFakeVariables = fakeVariableRange(mt);

		// Loop to insert fake variables and write their instructions
		auto LineNumberRange = std::uniform_int_distribution<int>(functionStart, returnLine);
		for (int i = 0; i < numberOfFakeVariables; i++)
		{
			newAddress = newAddress + i;
			int allocLine = LineNumberRange(mt);
			std::uniform_int_distribution<int> StoreLineRange(allocLine + 1, returnLine);

			int storeLine = (allocLine != returnLine) ? StoreLineRange(mt) : returnLine;

			iLDataManager.IlLines.insert(iLDataManager.IlLines.begin() + allocLine, llvmGeneration::dumpStackVariable(llvmAddressFromInt(newAddress).c_str()));

			returnLine += 1;

			iLDataManager.IlLines.insert(iLDataManager.IlLines.begin() + storeLine, llvmGeneration::dumpAssignment(llvmAddressFromInt(newAddress).c_str(), "27"));
			returnLine += 1;

			LineNumberRange = std::uniform_int_distribution<int>(functionStart, returnLine);
		}


		// Re-name variables to correct order         
		struct LineAndOccurence
		{
			int lineNumber;
			IlVariableOccurence VariableOccurence;
		};

		// At the moment, many variables are out of order -- Map of LineAndOccurence is used to identify occurences of a variable in the file,
		// and variablesInOrderOfAppearance is used to store current out-of-order variables in the order they appear.

		std::map<int, std::vector<LineAndOccurence>> variableNumberToVariableOccurence;
		std::vector<int> variablesInOrderOfAppearance;

		int lineNumber = 0;
		for (std::string line : iLDataManager.IlLines)
		{
			// Get variable occurences, which save the info required to replace them with new variable numbers
			std::vector<IlVariableOccurence> captures = captureVariablesAndPositionsFromLine(line);

			// Add or update a new map entry with the variable and a list of its occurences
			for (IlVariableOccurence variable : captures)
			{
				if (variableNumberToVariableOccurence.find(variable.number) != variableNumberToVariableOccurence.end())
				{
					variableNumberToVariableOccurence[variable.number].push_back(LineAndOccurence{ lineNumber, variable });
				}
				else
				{
					// If the variable is being added for the first time, also add it to the in order of appearance list
					variablesInOrderOfAppearance.push_back(variable.number);
					variableNumberToVariableOccurence.insert(std::make_pair(variable.number, std::vector<LineAndOccurence>{LineAndOccurence{ lineNumber, variable }}));
				}

			}

			// If we're at a function return, apply the changes and reset the map and vector for the next function
			if (isReturnLine(line))
			{
				// Loop over variables in order of appearance, and update their variable names to reorderedNumber
				for (int i = 0; i < variablesInOrderOfAppearance.size(); i++) {

					for (LineAndOccurence variable : variableNumberToVariableOccurence[variablesInOrderOfAppearance[i]])
					{
						iLDataManager.IlLines[variable.lineNumber].replace(variable.VariableOccurence.position, variable.VariableOccurence.length + 1, llvmAddressFromInt(i + 1));

					}
				}

				// Re-initialize collections
				variableNumberToVariableOccurence = std::map<int, std::vector<LineAndOccurence>>();
				variablesInOrderOfAppearance.clear();
			}

			lineNumber++;
		}

	}

}


// Write modified IR to new file 
void printHelper(std::vector<std::string> confuzzedOutput)
{

	std::ofstream resFile{ "res.ll" };

	if (!resFile) {
		std::cerr << "Failed to open res.ll for writing!\n";
		return;
	}

	for (std::string line : confuzzedOutput)
	{
		resFile << line + "\n";
	}

}

int FuzzMe() {

	Logger logger;

	logger.StatusMessage("Welcome to the Confuzzler!");

	iLDataManager.Initialize("./Tests/helloworld.ll");

	logger.StatusMessage("Beginning null literal replacement...");

	// Insert extra lines after our first edited line

	iLDataManager.IlLines[iLDataManager.FunctionDataVector.back().functionReturnLine] = iLDataManager.FunctionDataVector.back().getFormattedReturnLine();
	iLDataManager.IlLines.insert(iLDataManager.IlLines.begin() + iLDataManager.FunctionDataVector.back().functionReturnLine + 1, iLDataManager.FunctionDataVector.back().prefix + llvmGeneration::dumpLabel("raptor"));
	iLDataManager.IlLines.insert(iLDataManager.IlLines.begin() + iLDataManager.FunctionDataVector.back().functionReturnLine + 2, iLDataManager.FunctionDataVector.back().getFormattedReturnLine());

	logger.StatusMessage("Starting to shuffle local variables...");

	editLocalVariables(&iLDataManager.FunctionDataVector);

	logger.StatusMessage("Saving new file...");

	printHelper(iLDataManager.IlLines);

	logger.StatusMessage("Done!");

	return 0;
}

// TODO: add another function to invoke a shell script that links confuzzed output to binary

int main()
{
	// TESTING - START
	FuzzMe();
	// TESTING - END

	// Logger class object
	Logger logger;

	// Start up banner
	logger.StatusMessage("Welcome to the Confuzzler!");

	// Populate iLData.IlLines vector and lookupTable map with data from our file
	iLDataManager.Initialize("./Tests/predicates.ll");

	// Start up menu
	int choice;

	std::cout << "Please choose an obfuscation option:" << std::endl;
	std::cout << "1. Opaque predicate insertion " << std::endl;
	std::cout << "2. Local variable shuffling" << std::endl;
	std::cout << "3. String encryption/encoding" << std::endl;
	std::cout << "4. All of the above!!!" << std::endl;
	std::cout << "Enter your selection: ";
	std::cin >> choice;
	std::cout << "\n" << std::endl;

	switch (choice) {
	case 1:

		logger.StatusMessage("Beginning null literal replacement...");

		// Insert extra lines after our first edited line
		iLDataManager.IlLines[iLDataManager.FunctionDataVector.back().functionReturnLine] = iLDataManager.FunctionDataVector.back().getFormattedReturnLine();
		iLDataManager.IlLines.insert(iLDataManager.IlLines.begin() + iLDataManager.FunctionDataVector.back().functionReturnLine + 1, iLDataManager.FunctionDataVector.back().prefix + "Fake line, followed by duplicated line below");
		iLDataManager.IlLines.insert(iLDataManager.IlLines.begin() + iLDataManager.FunctionDataVector.back().functionReturnLine + 2, iLDataManager.FunctionDataVector.back().getFormattedReturnLine());

		// TODO: Fix ObfsZero and call it proper here

		return 1;
		break;

	case 2:

		logger.StatusMessage("Starting to shuffle local variables...");

		editLocalVariables(&iLDataManager.FunctionDataVector);

		printHelper(iLDataManager.IlLines);

		return 1;
		break;

	case 3:

		logger.StatusMessage("Performing pseudocryptography...");
		logger.StatusMessage("SYKE not implemented yet...");

		// TODO: Implement and call fauxcrypto proper here

		return 1;
		break;

	case 4:

		logger.StatusMessage("Running all techniques...");
		logger.StatusMessage("Feature coming soon!!");

		return 1;
		break;

		// TODO: Add support for user to confuzz their file with all 3 techniques

	default:

		logger.ErrorMessage("Invalid selection; exiting...");
	}

	return 0;
}


// Return comma separated line numbers where this token appears
std::string getLineNumbersString(std::string key)
{
	auto entry = iLDataManager.lookupTable[key];
	std::string linesOutput;
	for (ILDataManager::keywordData data : entry)
	{
		linesOutput.append(std::to_string(data.lineIndex + 1).c_str()).append(",");
	}
	return linesOutput;
}


prime_type getPrime(prime_type DifferentFrom = 0) {

	std::random_device random_device;
	std::mt19937 engine(random_device());
	std::uniform_int_distribution<int> distribution(0, sizeof(Prime_array) / sizeof(prime_type));

	const auto Prime = Prime_array[distribution(engine)];

	return Prime;
}


std::string obfusZero()
{

	int res;
	std::string LHS, RHS;

	prime_type p1 = getPrime(),
		p2 = getPrime(p1);

	std::cout << p1 + "\n";
	std::cout << p2 + "\n";

	if (p1 == 0 || p2 == 0)
	{
		std::cout << "p1 or p2 is 0 when it shouldn't be!!" << "\n";
	}

	// a can be any strictly positive integer
	int a1 = p1 - 1;
	int a2 = p2 - 1;

	// x & y indexes
	size_t x = static_cast<size_t>(rand() % 98) + 2;
	size_t y = static_cast<size_t>(rand() % 98) + 2;;

	// Replacing 0 by:
	// p1 * ((x || a1)^2) != p2 * ((y || a2)^2)
	// with p1 != p2 and a1 != 0 and a2 != 0
	if (p1 != p2 && a1 != 0 && a2 != 0) {

		// Left hand side
		size_t tmp1 = (x | a1);
		size_t tmp2 = static_cast<size_t>(std::pow(tmp1, 2));
		auto LHS = (p1 * tmp2);


		// Right hand side
		size_t tmp3 = (y | a2);
		size_t tmp4 = static_cast<size_t>(std::pow(tmp3, 2));
		auto RHS = p2 * tmp4;
	}

	// Expected result should be 0
	res = LHS.compare(RHS);

	// TODO: generate the corresponding IR for the work above

	return std::to_string(res);

}


/*
{
	operation1: [arg1, arg2, arg3],
	operation2: [arg1, <operation2>, arg3]
}


*/
