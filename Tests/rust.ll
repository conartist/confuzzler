; ModuleID = 'rust.2ca23981-cgu.0'
source_filename = "rust.2ca23981-cgu.0"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" = type { %"alloc::alloc::Global", %"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", i64 }
%"alloc::alloc::Global" = type {}
%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" = type { %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>" }
%"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>" = type { i64, [3 x i64] }
%"core::mem::maybe_uninit::MaybeUninit<alloc::alloc::Global>" = type { [0 x i8] }
%"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" = type { %"core::mem::manually_drop::ManuallyDrop<alloc::alloc::Global>", %"core::marker::PhantomData<alloc::boxed::Box<(std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>)>>", { i64, ptr }, i64 }
%"core::mem::manually_drop::ManuallyDrop<alloc::alloc::Global>" = type { %"alloc::alloc::Global" }
%"core::marker::PhantomData<alloc::boxed::Box<(std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>)>>" = type {}
%"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>" = type { %"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" }
%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" = type { %"core::marker::PhantomData<alloc::collections::btree::node::marker::KV>", { i64, ptr }, i64 }
%"core::marker::PhantomData<alloc::collections::btree::node::marker::KV>" = type {}
%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>" = type { [1 x i64], ptr, [1 x i64] }
%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>" = type { i8, [15 x i8] }
%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Os" = type { [1 x i32], i32 }
%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::SimpleMessage" = type { [1 x i64], ptr }
%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Custom" = type { [1 x i64], ptr }
%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Simple" = type { [1 x i8], i8 }
%"std::process::Command" = type { %"std::sys::unix::process::process_common::Command" }
%"std::sys::unix::process::process_common::Command" = type { %"std::sys_common::process::CommandEnv", { ptr, i64 }, { ptr, i64 }, { ptr, i64 }, %"alloc::vec::Vec<alloc::ffi::c_str::CString>", %"std::sys::unix::process::process_common::Argv", %"alloc::vec::Vec<alloc::boxed::Box<dyn core::ops::function::FnMut() -> core::result::Result<(), std::io::error::Error> + core::marker::Send + core::marker::Sync>>", { i32, i32 }, { i32, i32 }, { i32, i32 }, { i32, i32 }, { i32, i32 }, { i32, i32 }, i8, i8, i8, [5 x i8] }
%"std::sys_common::process::CommandEnv" = type { %"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", i8, i8, [6 x i8] }
%"alloc::vec::Vec<alloc::ffi::c_str::CString>" = type { { i64, ptr }, i64 }
%"std::sys::unix::process::process_common::Argv" = type { %"alloc::vec::Vec<*const i8>" }
%"alloc::vec::Vec<*const i8>" = type { { i64, ptr }, i64 }
%"alloc::vec::Vec<alloc::boxed::Box<dyn core::ops::function::FnMut() -> core::result::Result<(), std::io::error::Error> + core::marker::Send + core::marker::Sync>>" = type { { i64, ptr }, i64 }
%"core::mem::manually_drop::ManuallyDrop<core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>>" = type { %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>" }
%"core::mem::maybe_uninit::MaybeUninit<core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>>" = type { [4 x i64] }
%"std::process::Output" = type { %"alloc::vec::Vec<u8>", %"alloc::vec::Vec<u8>", i32, [1 x i32] }
%"alloc::vec::Vec<u8>" = type { { i64, ptr }, i64 }
%"core::option::Option<std::ffi::os_str::OsString>" = type { [1 x i64], ptr, [1 x i64] }
%"core::ptr::metadata::PtrComponents<()>" = type { ptr, {} }
%"core::ptr::metadata::PtrRepr<()>" = type { [1 x i64] }
%"core::ptr::metadata::PtrComponents<u8>" = type { ptr, {} }
%"core::ptr::metadata::PtrRepr<u8>" = type { [1 x i64] }
%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>" = type { [1 x i64], ptr, [1 x i64] }
%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>" = type { %"core::marker::PhantomData<alloc::collections::btree::node::marker::Edge>", { i64, ptr }, i64 }
%"core::marker::PhantomData<alloc::collections::btree::node::marker::Edge>" = type {}
%"core::result::Result<std::process::Output, std::io::error::Error>" = type { [1 x i64], ptr, [5 x i64] }
%"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>" = type { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>" }
%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>" = type { %"core::marker::PhantomData<alloc::collections::btree::node::marker::Edge>", { i64, ptr }, i64 }
%"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>" = type { [3 x i64] }
%"alloc::collections::btree::mem::replace::PanicGuard" = type {}
%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>" = type { i64, [3 x i64] }
%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>::Ok" = type { [1 x i64], %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>" }
%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>" = type { %"core::marker::PhantomData<alloc::collections::btree::node::marker::KV>", { i64, ptr }, i64 }
%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>" = type { %"core::marker::PhantomData<alloc::collections::btree::node::marker::KV>", { i64, ptr }, i64 }
%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>" = type { %"core::marker::PhantomData<alloc::collections::btree::node::marker::Edge>", { i64, ptr }, i64 }
%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>" = type { i64, [3 x i64] }
%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>::Err" = type { [1 x i64], %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>" }
%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>::Ok" = type { [1 x i64], %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }
%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>" = type { i64, [3 x i64] }
%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>" = type { i64, [2 x i64] }
%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Leaf" = type { [1 x i64], { i64, ptr } }
%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>::Leaf" = type { [1 x i64], %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>" }
%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Internal" = type { [1 x i64], { i64, ptr } }
%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>::Internal" = type { [1 x i64], %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>" }
%"alloc::collections::btree::node::LeafNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" = type { ptr, [11 x %"core::mem::maybe_uninit::MaybeUninit<std::ffi::os_str::OsString>"], [11 x %"core::mem::maybe_uninit::MaybeUninit<core::option::Option<std::ffi::os_str::OsString>>"], i16, i16, [2 x i16] }
%"core::mem::maybe_uninit::MaybeUninit<std::ffi::os_str::OsString>" = type { [3 x i64] }
%"core::mem::maybe_uninit::MaybeUninit<core::option::Option<std::ffi::os_str::OsString>>" = type { [3 x i64] }
%"alloc::collections::btree::node::InternalNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" = type { %"alloc::collections::btree::node::LeafNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", [12 x ptr] }
%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>::Err" = type { [1 x i64], { i64, ptr } }
%"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>" = type { i64, [3 x i64] }
%"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Root" = type { [1 x i64], { i64, ptr } }
%"core::option::Option<(alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>)>" = type { [1 x i64], ptr, [4 x i64] }
%"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>" = type { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }
%"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>" = type { [3 x i64] }
%"[closure@alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>::deallocating_next_unchecked<alloc::alloc::Global>::{closure#0}]" = type { %"alloc::alloc::Global" }
%"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Edge" = type { [1 x i64], %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>" }
%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>" = type { [1 x i64], ptr, [1 x i64] }
%"core::ops::control_flow::ControlFlow<core::option::Option<core::convert::Infallible>, alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>" = type { i64, [3 x i64] }
%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>" = type { [2 x i64], i64 }
%"core::ptr::metadata::PtrRepr<[u8]>" = type { [2 x i64] }
%"core::ptr::metadata::PtrRepr<[alloc::boxed::Box<dyn core::ops::function::FnMut() -> core::result::Result<(), std::io::error::Error> + core::marker::Send + core::marker::Sync>]>" = type { [2 x i64] }
%"core::ptr::metadata::PtrRepr<[alloc::ffi::c_str::CString]>" = type { [2 x i64] }
%"core::ptr::metadata::PtrRepr<[*const i8]>" = type { [2 x i64] }
%"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>" = type { [3 x i64] }

@alloc130 = private unnamed_addr constant <{ [40 x i8] }> <{ [40 x i8] c"internal error: entered unreachable code" }>, align 1
@alloc131 = private unnamed_addr constant <{ [90 x i8] }> <{ [90 x i8] c"/rustc/d5a82bbd26e1ad8b7401f6a718a9c57c96905483/library/std/src/io/error/repr_bitpacked.rs" }>, align 1
@alloc132 = private unnamed_addr constant <{ ptr, [16 x i8] }> <{ ptr @alloc131, [16 x i8] c"Z\00\00\00\00\00\00\00\18\01\00\00\0D\00\00\00" }>, align 8
@vtable.0 = private unnamed_addr constant <{ ptr, [16 x i8], ptr, ptr, ptr }> <{ ptr @"_ZN4core3ptr85drop_in_place$LT$std..rt..lang_start$LT$$LP$$RP$$GT$..$u7b$$u7b$closure$u7d$$u7d$$GT$17h684eecfe36f26fb0E", [16 x i8] c"\08\00\00\00\00\00\00\00\08\00\00\00\00\00\00\00", ptr @"_ZN4core3ops8function6FnOnce40call_once$u7b$$u7b$vtable.shim$u7d$$u7d$17h4993a0a72855c69bE", ptr @"_ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hcd759a653706691bE", ptr @"_ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hcd759a653706691bE" }>, align 8
@alloc138 = private unnamed_addr constant <{ [80 x i8] }> <{ [80 x i8] c"/rustc/d5a82bbd26e1ad8b7401f6a718a9c57c96905483/library/core/src/alloc/layout.rs" }>, align 1
@alloc139 = private unnamed_addr constant <{ ptr, [16 x i8] }> <{ ptr @alloc138, [16 x i8] c"P\00\00\00\00\00\00\00\C4\01\00\00)\00\00\00" }>, align 8
@str.1 = internal constant [25 x i8] c"attempt to divide by zero"
@vtable.2 = private unnamed_addr constant <{ ptr, [16 x i8], ptr }> <{ ptr @"_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17he5cda979aa03a59eE", [16 x i8] c"\08\00\00\00\00\00\00\00\08\00\00\00\00\00\00\00", ptr @"_ZN58_$LT$std..io..error..Error$u20$as$u20$core..fmt..Debug$GT$3fmt17hd6baae1594d2a2f7E" }>, align 8
@alloc146 = private unnamed_addr constant <{ [43 x i8] }> <{ [43 x i8] c"called `Option::unwrap()` on a `None` value" }>, align 1
@alloc147 = private unnamed_addr constant <{ [95 x i8] }> <{ [95 x i8] c"/rustc/d5a82bbd26e1ad8b7401f6a718a9c57c96905483/library/alloc/src/collections/btree/navigate.rs" }>, align 1
@alloc145 = private unnamed_addr constant <{ ptr, [16 x i8] }> <{ ptr @alloc147, [16 x i8] c"_\00\00\00\00\00\00\00L\02\00\00\0D\00\00\00" }>, align 8
@alloc148 = private unnamed_addr constant <{ ptr, [16 x i8] }> <{ ptr @alloc147, [16 x i8] c"_\00\00\00\00\00\00\00\BA\00\00\00\15\00\00\00" }>, align 8
@alloc149 = private unnamed_addr constant <{ [76 x i8] }> <{ [76 x i8] c"/rustc/d5a82bbd26e1ad8b7401f6a718a9c57c96905483/library/alloc/src/raw_vec.rs" }>, align 1
@alloc150 = private unnamed_addr constant <{ ptr, [16 x i8] }> <{ ptr @alloc149, [16 x i8] c"L\00\00\00\00\00\00\00\F7\00\00\00;\00\00\00" }>, align 8
@alloc151 = private unnamed_addr constant <{ [2 x i8] }> <{ [2 x i8] c"sh" }>, align 1
@alloc152 = private unnamed_addr constant <{ [2 x i8] }> <{ [2 x i8] c"-c" }>, align 1
@alloc153 = private unnamed_addr constant <{ [10 x i8] }> <{ [10 x i8] c"echo hello" }>, align 1
@alloc154 = private unnamed_addr constant <{ [25 x i8] }> <{ [25 x i8] c"failed to execute process" }>, align 1
@alloc155 = private unnamed_addr constant <{ [7 x i8] }> <{ [7 x i8] c"rust.rs" }>, align 1
@alloc156 = private unnamed_addr constant <{ ptr, [16 x i8] }> <{ ptr @alloc155, [16 x i8] c"\07\00\00\00\00\00\00\00\10\00\00\00\12\00\00\00" }>, align 8

; <alloc::collections::btree::map::BTreeMap<K,V,A> as core::iter::traits::collect::IntoIterator>::into_iter
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN119_$LT$alloc..collections..btree..map..BTreeMap$LT$K$C$V$C$A$GT$$u20$as$u20$core..iter..traits..collect..IntoIterator$GT$9into_iter17h2b31531af56d7fb4E"(ptr sret(%"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %0, ptr %self) unnamed_addr #0 {
start:
  %tmp1 = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::alloc::Global>", align 1
  %tmp = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::alloc::Global>", align 1
  %_32 = alloca { i64, ptr }, align 8
  %_28 = alloca %"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_22 = alloca %"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_13 = alloca %"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %full_range = alloca %"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_4 = alloca { i64, ptr }, align 8
  %value = alloca %"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %me = alloca %"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %value, ptr align 8 %self, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_28, ptr align 8 %value, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %me, ptr align 8 %_28, i64 24, i1 false)
  %1 = getelementptr inbounds { i64, ptr }, ptr %_32, i32 0, i32 1
  store ptr null, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %_32, i32 0, i32 0
  %3 = load i64, ptr %2, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %_32, i32 0, i32 1
  %5 = load ptr, ptr %4, align 8
; call core::mem::replace
  %6 = call { i64, ptr } @_ZN4core3mem7replace17hd43817dbf2300226E(ptr align 8 %me, i64 %3, ptr %5)
  store { i64, ptr } %6, ptr %_4, align 8
  %7 = getelementptr inbounds { i64, ptr }, ptr %_4, i32 0, i32 1
  %8 = load ptr, ptr %7, align 8
  %9 = ptrtoint ptr %8 to i64
  %10 = icmp eq i64 %9, 0
  %_8 = select i1 %10, i64 0, i64 1
  %11 = icmp eq i64 %_8, 1
  br i1 %11, label %bb1, label %bb4

bb1:                                              ; preds = %start
  %12 = getelementptr inbounds { i64, ptr }, ptr %_4, i32 0, i32 0
  %root.0 = load i64, ptr %12, align 8
  %13 = getelementptr inbounds { i64, ptr }, ptr %_4, i32 0, i32 1
  %root.1 = load ptr, ptr %13, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Owned,K,V,Type>::into_dying
  %14 = call { i64, ptr } @"_ZN5alloc11collections5btree4node76NodeRef$LT$alloc..collections..btree..node..marker..Owned$C$K$C$V$C$Type$GT$10into_dying17h77214fe9655b199fE"(i64 %root.0, ptr %root.1)
  %_11.0 = extractvalue { i64, ptr } %14, 0
  %_11.1 = extractvalue { i64, ptr } %14, 1
; call alloc::collections::btree::navigate::<impl alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::LeafOrInternal>>::full_range
  call void @"_ZN5alloc11collections5btree8navigate178_$LT$impl$u20$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$GT$10full_range17h0cee78f197f2fc4cE"(ptr sret(%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %full_range, i64 %_11.0, ptr %_11.1)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_13, ptr align 8 %full_range, i64 64, i1 false)
  %15 = getelementptr inbounds %"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %me, i32 0, i32 3
  %_14 = load i64, ptr %15, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 1 %tmp, ptr align 1 %me, i64 0, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_13, i64 64, i1 false)
  %16 = getelementptr inbounds %"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %0, i32 0, i32 2
  store i64 %_14, ptr %16, align 8
  br label %bb6

bb4:                                              ; preds = %start
; call alloc::collections::btree::navigate::LazyLeafRange<BorrowType,K,V>::none
  call void @"_ZN5alloc11collections5btree8navigate39LazyLeafRange$LT$BorrowType$C$K$C$V$GT$4none17h7c9883f987bb6c7aE"(ptr sret(%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %_22)
  call void @llvm.memcpy.p0.p0.i64(ptr align 1 %tmp1, ptr align 1 %me, i64 0, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_22, i64 64, i1 false)
  %17 = getelementptr inbounds %"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %0, i32 0, i32 2
  store i64 0, ptr %17, align 8
  br label %bb6

bb6:                                              ; preds = %bb1, %bb4
  ret void
}

; <core::ptr::non_null::NonNull<T> as core::convert::From<core::ptr::unique::Unique<T>>>::from
; Function Attrs: inlinehint nonlazybind uwtable
define internal ptr @"_ZN119_$LT$core..ptr..non_null..NonNull$LT$T$GT$$u20$as$u20$core..convert..From$LT$core..ptr..unique..Unique$LT$T$GT$$GT$$GT$4from17h40bfc4ab431dc539E"(ptr %unique) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  store ptr %unique, ptr %0, align 8
  %1 = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
  ret ptr %1
}

; <<alloc::collections::btree::map::IntoIter<K,V,A> as core::ops::drop::Drop>::drop::DropGuard<K,V,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN174_$LT$$LT$alloc..collections..btree..map..IntoIter$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$..drop..DropGuard$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h68759c0f5d27a4c2E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_7 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %kv = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %_2 = alloca %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", align 8
  br label %bb1

bb1:                                              ; preds = %bb3, %start
  %_8 = load ptr, ptr %self, align 8, !nonnull !3, !align !4, !noundef !3
; call alloc::collections::btree::map::IntoIter<K,V,A>::dying_next
  call void @"_ZN5alloc11collections5btree3map25IntoIter$LT$K$C$V$C$A$GT$10dying_next17h53e185146a41908aE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>") %_2, ptr align 8 %_8)
  %0 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", ptr %_2, i32 0, i32 1
  %1 = load ptr, ptr %0, align 8
  %2 = ptrtoint ptr %1 to i64
  %3 = icmp eq i64 %2, 0
  %_4 = select i1 %3, i64 0, i64 1
  %4 = icmp eq i64 %_4, 1
  br i1 %4, label %bb3, label %bb5

bb3:                                              ; preds = %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %kv, ptr align 8 %_2, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_7, ptr align 8 %kv, i64 24, i1 false)
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::drop_key_val
  call void @"_ZN5alloc11collections5btree4node173Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$12drop_key_val17h20aa62e5ca1dbd04E"(ptr %_7)
  br label %bb1

bb5:                                              ; preds = %bb1
  ret void
}

; std::sys_common::backtrace::__rust_begin_short_backtrace
; Function Attrs: noinline nonlazybind uwtable
define internal void @_ZN3std10sys_common9backtrace28__rust_begin_short_backtrace17hd23015351ff2ceabE(ptr %f) unnamed_addr #2 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; call core::ops::function::FnOnce::call_once
  call void @_ZN4core3ops8function6FnOnce9call_once17h3e2534c22c5c3902E(ptr %f)
  call void asm sideeffect "", "~{memory}"(), !srcloc !5
  br label %bb4

bb4:                                              ; preds = %start
  ret void

bb2:                                              ; No predecessors!
  %1 = load ptr, ptr %0, align 8
  %2 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %3 = load i32, ptr %2, align 8
  %4 = insertvalue { ptr, i32 } undef, ptr %1, 0
  %5 = insertvalue { ptr, i32 } %4, i32 %3, 1
  resume { ptr, i32 } %5
}

; std::io::error::repr_bitpacked::decode_repr
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN3std2io5error14repr_bitpacked11decode_repr17h65ad2d8cd0204a41E(ptr sret(%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>") %0, ptr %ptr) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %1 = alloca ptr, align 8
  %2 = alloca { ptr, i32 }, align 8
  %_36 = alloca i8, align 1
  %_33 = alloca ptr, align 8
  %self1 = alloca ptr, align 8
  %f = alloca ptr, align 8
  %self = alloca i8, align 1
  %kind = alloca i8, align 1
  %bits = alloca i64, align 8
  store i8 1, ptr %_36, align 1
  store ptr %ptr, ptr %bits, align 8
  br label %bb14

bb14:                                             ; preds = %start
  %_7 = load i64, ptr %bits, align 8
  %_6 = and i64 %_7, 3
  switch i64 %_6, label %bb1 [
    i64 2, label %bb2
    i64 3, label %bb3
    i64 0, label %bb5
    i64 1, label %bb6
  ]

bb1:                                              ; preds = %bb14
; invoke core::panicking::panic
  invoke void @_ZN4core9panicking5panic17h545818946343732bE(ptr align 1 @alloc130, i64 40, ptr align 8 @alloc132) #11
          to label %unreachable unwind label %cleanup

bb2:                                              ; preds = %bb14
  %_11 = load i64, ptr %bits, align 8
  %_9 = ashr i64 %_11, 32
  %code = trunc i64 %_9 to i32
  %3 = getelementptr inbounds %"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Os", ptr %0, i32 0, i32 1
  store i32 %code, ptr %3, align 4
  store i8 0, ptr %0, align 8
  br label %bb8

bb3:                                              ; preds = %bb14
  %_15 = load i64, ptr %bits, align 8
  %_14 = lshr i64 %_15, 32
  %kind_bits = trunc i64 %_14 to i32
; invoke std::io::error::repr_bitpacked::kind_from_prim
  %4 = invoke i8 @_ZN3std2io5error14repr_bitpacked14kind_from_prim17h1e648cf2bb3405bbE(i32 %kind_bits)
          to label %bb4 unwind label %cleanup, !range !6

bb5:                                              ; preds = %bb14
  store ptr %ptr, ptr %self1, align 8
  %_51 = load ptr, ptr %self1, align 8
  %5 = getelementptr inbounds %"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::SimpleMessage", ptr %0, i32 0, i32 1
  store ptr %_51, ptr %5, align 8
  store i8 2, ptr %0, align 8
  br label %bb8

bb6:                                              ; preds = %bb14
  %6 = getelementptr i8, ptr %ptr, i64 -1
  store ptr %6, ptr %1, align 8
  %_66 = load ptr, ptr %1, align 8
  br label %bb21

bb13:                                             ; preds = %cleanup
  %7 = load i8, ptr %_36, align 1, !range !7, !noundef !3
  %8 = trunc i8 %7 to i1
  br i1 %8, label %bb12, label %bb10

cleanup:                                          ; preds = %bb15, %bb3, %bb20, %bb21, %bb1
  %9 = landingpad { ptr, i32 }
          cleanup
  %10 = extractvalue { ptr, i32 } %9, 0
  %11 = extractvalue { ptr, i32 } %9, 1
  %12 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %10, ptr %12, align 8
  %13 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %11, ptr %13, align 8
  br label %bb13

unreachable:                                      ; preds = %bb1
  unreachable

bb21:                                             ; preds = %bb6
; invoke core::ptr::mut_ptr::<impl *mut T>::with_metadata_of
  %self2 = invoke ptr @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$16with_metadata_of17h7ccf771fd8534e89E"(ptr %_66, ptr %ptr)
          to label %bb20 unwind label %cleanup

bb20:                                             ; preds = %bb21
  store i8 0, ptr %_36, align 1
  store ptr %self2, ptr %_33, align 8
  %14 = load ptr, ptr %_33, align 8
; invoke <std::io::error::repr_bitpacked::Repr as core::ops::drop::Drop>::drop::{{closure}}
  %_31 = invoke align 8 ptr @"_ZN78_$LT$std..io..error..repr_bitpacked..Repr$u20$as$u20$core..ops..drop..Drop$GT$4drop28_$u7b$$u7b$closure$u7d$$u7d$17hf09f9958e8268f64E"(ptr %14)
          to label %bb7 unwind label %cleanup

bb7:                                              ; preds = %bb20
  %15 = getelementptr inbounds %"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Custom", ptr %0, i32 0, i32 1
  store ptr %_31, ptr %15, align 8
  store i8 3, ptr %0, align 8
  br label %bb8

bb8:                                              ; preds = %bb2, %bb19, %bb5, %bb7
  %16 = load i8, ptr %_36, align 1, !range !7, !noundef !3
  %17 = trunc i8 %16 to i1
  br i1 %17, label %bb11, label %bb9

bb4:                                              ; preds = %bb3
  store i8 %4, ptr %self, align 1
  store ptr %bits, ptr %f, align 8
  %18 = load i8, ptr %self, align 1, !range !6, !noundef !3
  %19 = icmp eq i8 %18, 41
  %_39 = select i1 %19, i64 0, i64 1
  %20 = icmp eq i64 %_39, 0
  br i1 %20, label %bb15, label %bb17

bb15:                                             ; preds = %bb4
  %_41 = load ptr, ptr %f, align 8, !nonnull !3, !align !4, !noundef !3
; invoke std::io::error::repr_bitpacked::decode_repr::{{closure}}
  %21 = invoke i8 @"_ZN3std2io5error14repr_bitpacked11decode_repr28_$u7b$$u7b$closure$u7d$$u7d$17h20ad4cf2fe80cafbE"(ptr align 8 %_41)
          to label %bb18 unwind label %cleanup, !range !8

bb17:                                             ; preds = %bb4
  %x = load i8, ptr %self, align 1, !range !8, !noundef !3
  store i8 %x, ptr %kind, align 1
  br label %bb19

bb16:                                             ; No predecessors!
  unreachable

bb19:                                             ; preds = %bb18, %bb17
  %_21 = load i8, ptr %kind, align 1, !range !8, !noundef !3
  %22 = getelementptr inbounds %"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Simple", ptr %0, i32 0, i32 1
  store i8 %_21, ptr %22, align 1
  store i8 1, ptr %0, align 8
  br label %bb8

bb18:                                             ; preds = %bb15
  store i8 %21, ptr %kind, align 1
  br label %bb19

bb10:                                             ; preds = %bb12, %bb13
  %23 = load ptr, ptr %2, align 8
  %24 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %25 = load i32, ptr %24, align 8
  %26 = insertvalue { ptr, i32 } undef, ptr %23, 0
  %27 = insertvalue { ptr, i32 } %26, i32 %25, 1
  resume { ptr, i32 } %27

bb12:                                             ; preds = %bb13
  br label %bb10

bb9:                                              ; preds = %bb11, %bb8
  ret void

bb11:                                             ; preds = %bb8
  br label %bb9
}

; std::io::error::repr_bitpacked::decode_repr::{{closure}}
; Function Attrs: inlinehint nonlazybind uwtable
define internal i8 @"_ZN3std2io5error14repr_bitpacked11decode_repr28_$u7b$$u7b$closure$u7d$$u7d$17h20ad4cf2fe80cafbE"(ptr align 8 %_1) unnamed_addr #1 {
start:
; call core::hint::unreachable_unchecked
  call void @_ZN4core4hint21unreachable_unchecked17hf62cde02ee11bb42E() #11
  unreachable
}

; std::io::error::repr_bitpacked::kind_from_prim
; Function Attrs: inlinehint nonlazybind uwtable
define internal i8 @_ZN3std2io5error14repr_bitpacked14kind_from_prim17h1e648cf2bb3405bbE(i32 %0) unnamed_addr #1 {
start:
  %1 = alloca i8, align 1
  %ek = alloca i32, align 4
  store i32 %0, ptr %ek, align 4
  %_5 = load i32, ptr %ek, align 4
  %_4 = icmp eq i32 %_5, 0
  br i1 %_4, label %bb1, label %bb2

bb2:                                              ; preds = %start
  %_10 = load i32, ptr %ek, align 4
  %_9 = icmp eq i32 %_10, 1
  br i1 %_9, label %bb3, label %bb4

bb1:                                              ; preds = %start
  store i8 0, ptr %1, align 1
  br label %bb83

bb83:                                             ; preds = %bb82, %bb81, %bb79, %bb77, %bb75, %bb73, %bb71, %bb69, %bb67, %bb65, %bb63, %bb61, %bb59, %bb57, %bb55, %bb53, %bb51, %bb49, %bb47, %bb45, %bb43, %bb41, %bb39, %bb37, %bb35, %bb33, %bb31, %bb29, %bb27, %bb25, %bb23, %bb21, %bb19, %bb17, %bb15, %bb13, %bb11, %bb9, %bb7, %bb5, %bb3, %bb1
  %2 = load i8, ptr %1, align 1, !range !6, !noundef !3
  ret i8 %2

bb4:                                              ; preds = %bb2
  %_15 = load i32, ptr %ek, align 4
  %_14 = icmp eq i32 %_15, 2
  br i1 %_14, label %bb5, label %bb6

bb3:                                              ; preds = %bb2
  store i8 1, ptr %1, align 1
  br label %bb83

bb6:                                              ; preds = %bb4
  %_20 = load i32, ptr %ek, align 4
  %_19 = icmp eq i32 %_20, 3
  br i1 %_19, label %bb7, label %bb8

bb5:                                              ; preds = %bb4
  store i8 2, ptr %1, align 1
  br label %bb83

bb8:                                              ; preds = %bb6
  %_25 = load i32, ptr %ek, align 4
  %_24 = icmp eq i32 %_25, 4
  br i1 %_24, label %bb9, label %bb10

bb7:                                              ; preds = %bb6
  store i8 3, ptr %1, align 1
  br label %bb83

bb10:                                             ; preds = %bb8
  %_30 = load i32, ptr %ek, align 4
  %_29 = icmp eq i32 %_30, 5
  br i1 %_29, label %bb11, label %bb12

bb9:                                              ; preds = %bb8
  store i8 4, ptr %1, align 1
  br label %bb83

bb12:                                             ; preds = %bb10
  %_35 = load i32, ptr %ek, align 4
  %_34 = icmp eq i32 %_35, 6
  br i1 %_34, label %bb13, label %bb14

bb11:                                             ; preds = %bb10
  store i8 5, ptr %1, align 1
  br label %bb83

bb14:                                             ; preds = %bb12
  %_40 = load i32, ptr %ek, align 4
  %_39 = icmp eq i32 %_40, 7
  br i1 %_39, label %bb15, label %bb16

bb13:                                             ; preds = %bb12
  store i8 6, ptr %1, align 1
  br label %bb83

bb16:                                             ; preds = %bb14
  %_45 = load i32, ptr %ek, align 4
  %_44 = icmp eq i32 %_45, 8
  br i1 %_44, label %bb17, label %bb18

bb15:                                             ; preds = %bb14
  store i8 7, ptr %1, align 1
  br label %bb83

bb18:                                             ; preds = %bb16
  %_50 = load i32, ptr %ek, align 4
  %_49 = icmp eq i32 %_50, 9
  br i1 %_49, label %bb19, label %bb20

bb17:                                             ; preds = %bb16
  store i8 8, ptr %1, align 1
  br label %bb83

bb20:                                             ; preds = %bb18
  %_55 = load i32, ptr %ek, align 4
  %_54 = icmp eq i32 %_55, 10
  br i1 %_54, label %bb21, label %bb22

bb19:                                             ; preds = %bb18
  store i8 9, ptr %1, align 1
  br label %bb83

bb22:                                             ; preds = %bb20
  %_60 = load i32, ptr %ek, align 4
  %_59 = icmp eq i32 %_60, 11
  br i1 %_59, label %bb23, label %bb24

bb21:                                             ; preds = %bb20
  store i8 10, ptr %1, align 1
  br label %bb83

bb24:                                             ; preds = %bb22
  %_65 = load i32, ptr %ek, align 4
  %_64 = icmp eq i32 %_65, 12
  br i1 %_64, label %bb25, label %bb26

bb23:                                             ; preds = %bb22
  store i8 11, ptr %1, align 1
  br label %bb83

bb26:                                             ; preds = %bb24
  %_70 = load i32, ptr %ek, align 4
  %_69 = icmp eq i32 %_70, 13
  br i1 %_69, label %bb27, label %bb28

bb25:                                             ; preds = %bb24
  store i8 12, ptr %1, align 1
  br label %bb83

bb28:                                             ; preds = %bb26
  %_75 = load i32, ptr %ek, align 4
  %_74 = icmp eq i32 %_75, 14
  br i1 %_74, label %bb29, label %bb30

bb27:                                             ; preds = %bb26
  store i8 13, ptr %1, align 1
  br label %bb83

bb30:                                             ; preds = %bb28
  %_80 = load i32, ptr %ek, align 4
  %_79 = icmp eq i32 %_80, 15
  br i1 %_79, label %bb31, label %bb32

bb29:                                             ; preds = %bb28
  store i8 14, ptr %1, align 1
  br label %bb83

bb32:                                             ; preds = %bb30
  %_85 = load i32, ptr %ek, align 4
  %_84 = icmp eq i32 %_85, 16
  br i1 %_84, label %bb33, label %bb34

bb31:                                             ; preds = %bb30
  store i8 15, ptr %1, align 1
  br label %bb83

bb34:                                             ; preds = %bb32
  %_90 = load i32, ptr %ek, align 4
  %_89 = icmp eq i32 %_90, 17
  br i1 %_89, label %bb35, label %bb36

bb33:                                             ; preds = %bb32
  store i8 16, ptr %1, align 1
  br label %bb83

bb36:                                             ; preds = %bb34
  %_95 = load i32, ptr %ek, align 4
  %_94 = icmp eq i32 %_95, 18
  br i1 %_94, label %bb37, label %bb38

bb35:                                             ; preds = %bb34
  store i8 17, ptr %1, align 1
  br label %bb83

bb38:                                             ; preds = %bb36
  %_100 = load i32, ptr %ek, align 4
  %_99 = icmp eq i32 %_100, 19
  br i1 %_99, label %bb39, label %bb40

bb37:                                             ; preds = %bb36
  store i8 18, ptr %1, align 1
  br label %bb83

bb40:                                             ; preds = %bb38
  %_105 = load i32, ptr %ek, align 4
  %_104 = icmp eq i32 %_105, 20
  br i1 %_104, label %bb41, label %bb42

bb39:                                             ; preds = %bb38
  store i8 19, ptr %1, align 1
  br label %bb83

bb42:                                             ; preds = %bb40
  %_110 = load i32, ptr %ek, align 4
  %_109 = icmp eq i32 %_110, 21
  br i1 %_109, label %bb43, label %bb44

bb41:                                             ; preds = %bb40
  store i8 20, ptr %1, align 1
  br label %bb83

bb44:                                             ; preds = %bb42
  %_115 = load i32, ptr %ek, align 4
  %_114 = icmp eq i32 %_115, 22
  br i1 %_114, label %bb45, label %bb46

bb43:                                             ; preds = %bb42
  store i8 21, ptr %1, align 1
  br label %bb83

bb46:                                             ; preds = %bb44
  %_120 = load i32, ptr %ek, align 4
  %_119 = icmp eq i32 %_120, 23
  br i1 %_119, label %bb47, label %bb48

bb45:                                             ; preds = %bb44
  store i8 22, ptr %1, align 1
  br label %bb83

bb48:                                             ; preds = %bb46
  %_125 = load i32, ptr %ek, align 4
  %_124 = icmp eq i32 %_125, 24
  br i1 %_124, label %bb49, label %bb50

bb47:                                             ; preds = %bb46
  store i8 23, ptr %1, align 1
  br label %bb83

bb50:                                             ; preds = %bb48
  %_130 = load i32, ptr %ek, align 4
  %_129 = icmp eq i32 %_130, 25
  br i1 %_129, label %bb51, label %bb52

bb49:                                             ; preds = %bb48
  store i8 24, ptr %1, align 1
  br label %bb83

bb52:                                             ; preds = %bb50
  %_135 = load i32, ptr %ek, align 4
  %_134 = icmp eq i32 %_135, 26
  br i1 %_134, label %bb53, label %bb54

bb51:                                             ; preds = %bb50
  store i8 25, ptr %1, align 1
  br label %bb83

bb54:                                             ; preds = %bb52
  %_140 = load i32, ptr %ek, align 4
  %_139 = icmp eq i32 %_140, 27
  br i1 %_139, label %bb55, label %bb56

bb53:                                             ; preds = %bb52
  store i8 26, ptr %1, align 1
  br label %bb83

bb56:                                             ; preds = %bb54
  %_145 = load i32, ptr %ek, align 4
  %_144 = icmp eq i32 %_145, 28
  br i1 %_144, label %bb57, label %bb58

bb55:                                             ; preds = %bb54
  store i8 27, ptr %1, align 1
  br label %bb83

bb58:                                             ; preds = %bb56
  %_150 = load i32, ptr %ek, align 4
  %_149 = icmp eq i32 %_150, 29
  br i1 %_149, label %bb59, label %bb60

bb57:                                             ; preds = %bb56
  store i8 28, ptr %1, align 1
  br label %bb83

bb60:                                             ; preds = %bb58
  %_155 = load i32, ptr %ek, align 4
  %_154 = icmp eq i32 %_155, 30
  br i1 %_154, label %bb61, label %bb62

bb59:                                             ; preds = %bb58
  store i8 29, ptr %1, align 1
  br label %bb83

bb62:                                             ; preds = %bb60
  %_160 = load i32, ptr %ek, align 4
  %_159 = icmp eq i32 %_160, 31
  br i1 %_159, label %bb63, label %bb64

bb61:                                             ; preds = %bb60
  store i8 30, ptr %1, align 1
  br label %bb83

bb64:                                             ; preds = %bb62
  %_165 = load i32, ptr %ek, align 4
  %_164 = icmp eq i32 %_165, 32
  br i1 %_164, label %bb65, label %bb66

bb63:                                             ; preds = %bb62
  store i8 31, ptr %1, align 1
  br label %bb83

bb66:                                             ; preds = %bb64
  %_170 = load i32, ptr %ek, align 4
  %_169 = icmp eq i32 %_170, 33
  br i1 %_169, label %bb67, label %bb68

bb65:                                             ; preds = %bb64
  store i8 32, ptr %1, align 1
  br label %bb83

bb68:                                             ; preds = %bb66
  %_175 = load i32, ptr %ek, align 4
  %_174 = icmp eq i32 %_175, 34
  br i1 %_174, label %bb69, label %bb70

bb67:                                             ; preds = %bb66
  store i8 33, ptr %1, align 1
  br label %bb83

bb70:                                             ; preds = %bb68
  %_180 = load i32, ptr %ek, align 4
  %_179 = icmp eq i32 %_180, 35
  br i1 %_179, label %bb71, label %bb72

bb69:                                             ; preds = %bb68
  store i8 34, ptr %1, align 1
  br label %bb83

bb72:                                             ; preds = %bb70
  %_185 = load i32, ptr %ek, align 4
  %_184 = icmp eq i32 %_185, 39
  br i1 %_184, label %bb73, label %bb74

bb71:                                             ; preds = %bb70
  store i8 35, ptr %1, align 1
  br label %bb83

bb74:                                             ; preds = %bb72
  %_190 = load i32, ptr %ek, align 4
  %_189 = icmp eq i32 %_190, 37
  br i1 %_189, label %bb75, label %bb76

bb73:                                             ; preds = %bb72
  store i8 39, ptr %1, align 1
  br label %bb83

bb76:                                             ; preds = %bb74
  %_195 = load i32, ptr %ek, align 4
  %_194 = icmp eq i32 %_195, 36
  br i1 %_194, label %bb77, label %bb78

bb75:                                             ; preds = %bb74
  store i8 37, ptr %1, align 1
  br label %bb83

bb78:                                             ; preds = %bb76
  %_200 = load i32, ptr %ek, align 4
  %_199 = icmp eq i32 %_200, 38
  br i1 %_199, label %bb79, label %bb80

bb77:                                             ; preds = %bb76
  store i8 36, ptr %1, align 1
  br label %bb83

bb80:                                             ; preds = %bb78
  %_205 = load i32, ptr %ek, align 4
  %_204 = icmp eq i32 %_205, 40
  br i1 %_204, label %bb81, label %bb82

bb79:                                             ; preds = %bb78
  store i8 38, ptr %1, align 1
  br label %bb83

bb82:                                             ; preds = %bb80
  store i8 41, ptr %1, align 1
  br label %bb83

bb81:                                             ; preds = %bb80
  store i8 40, ptr %1, align 1
  br label %bb83
}

; std::rt::lang_start
; Function Attrs: nonlazybind uwtable
define hidden i64 @_ZN3std2rt10lang_start17h95a848735ce62164E(ptr %main, i64 %argc, ptr %argv, i8 %sigpipe) unnamed_addr #0 {
start:
  %_9 = alloca ptr, align 8
  %_5 = alloca i64, align 8
  store ptr %main, ptr %_9, align 8
; call std::rt::lang_start_internal
  %0 = call i64 @_ZN3std2rt19lang_start_internal17hd16e6ff7bf05a444E(ptr align 1 %_9, ptr align 8 @vtable.0, i64 %argc, ptr %argv, i8 %sigpipe)
  store i64 %0, ptr %_5, align 8
  %v = load i64, ptr %_5, align 8
  ret i64 %v
}

; std::rt::lang_start::{{closure}}
; Function Attrs: inlinehint nonlazybind uwtable
define internal i32 @"_ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hcd759a653706691bE"(ptr align 8 %_1) unnamed_addr #1 {
start:
  %self = alloca i8, align 1
  %_4 = load ptr, ptr %_1, align 8, !nonnull !3, !noundef !3
; call std::sys_common::backtrace::__rust_begin_short_backtrace
  call void @_ZN3std10sys_common9backtrace28__rust_begin_short_backtrace17hd23015351ff2ceabE(ptr %_4)
; call <() as std::process::Termination>::report
  %0 = call i8 @"_ZN54_$LT$$LP$$RP$$u20$as$u20$std..process..Termination$GT$6report17hb90ba8952d02e25eE"()
  store i8 %0, ptr %self, align 1
  %_6 = load i8, ptr %self, align 1
  %1 = zext i8 %_6 to i32
  ret i32 %1
}

; std::ffi::os_str::<impl core::convert::AsRef<std::ffi::os_str::OsStr> for str>::as_ref
; Function Attrs: inlinehint nonlazybind uwtable
define internal { ptr, i64 } @"_ZN3std3ffi6os_str85_$LT$impl$u20$core..convert..AsRef$LT$std..ffi..os_str..OsStr$GT$$u20$for$u20$str$GT$6as_ref17h6edd23f2e71f6e8aE"(ptr align 1 %self.0, i64 %self.1) unnamed_addr #1 {
start:
  %0 = alloca { ptr, i64 }, align 8
  %1 = alloca { ptr, i64 }, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %1, i32 0, i32 0
  store ptr %self.0, ptr %2, align 8
  %3 = getelementptr inbounds { ptr, i64 }, ptr %1, i32 0, i32 1
  store i64 %self.1, ptr %3, align 8
  %4 = getelementptr inbounds { ptr, i64 }, ptr %1, i32 0, i32 0
  %_6.0 = load ptr, ptr %4, align 8, !nonnull !3, !align !9, !noundef !3
  %5 = getelementptr inbounds { ptr, i64 }, ptr %1, i32 0, i32 1
  %_6.1 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { ptr, i64 }, ptr %0, i32 0, i32 0
  store ptr %_6.0, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %0, i32 0, i32 1
  store i64 %_6.1, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %0, i32 0, i32 0
  %_3.0 = load ptr, ptr %8, align 8, !nonnull !3, !align !9, !noundef !3
  %9 = getelementptr inbounds { ptr, i64 }, ptr %0, i32 0, i32 1
  %_3.1 = load i64, ptr %9, align 8
  %10 = insertvalue { ptr, i64 } undef, ptr %_3.0, 0
  %11 = insertvalue { ptr, i64 } %10, i64 %_3.1, 1
  ret { ptr, i64 } %11
}

; std::process::Command::arg
; Function Attrs: nonlazybind uwtable
define internal align 8 ptr @_ZN3std7process7Command3arg17h138386b456ed717fE(ptr align 8 %self, ptr align 1 %0, i64 %1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %2 = alloca { ptr, i32 }, align 8
  %arg = alloca { ptr, i64 }, align 8
  %3 = getelementptr inbounds { ptr, i64 }, ptr %arg, i32 0, i32 0
  store ptr %0, ptr %3, align 8
  %4 = getelementptr inbounds { ptr, i64 }, ptr %arg, i32 0, i32 1
  store i64 %1, ptr %4, align 8
; invoke <&T as core::convert::AsRef<U>>::as_ref
  %5 = invoke { ptr, i64 } @"_ZN55_$LT$$RF$T$u20$as$u20$core..convert..AsRef$LT$U$GT$$GT$6as_ref17h37fb207455acf265E"(ptr align 8 %arg)
          to label %bb1 unwind label %cleanup

bb4:                                              ; preds = %cleanup
  %6 = load ptr, ptr %2, align 8
  %7 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %8 = load i32, ptr %7, align 8
  %9 = insertvalue { ptr, i32 } undef, ptr %6, 0
  %10 = insertvalue { ptr, i32 } %9, i32 %8, 1
  resume { ptr, i32 } %10

cleanup:                                          ; preds = %bb1, %start
  %11 = landingpad { ptr, i32 }
          cleanup
  %12 = extractvalue { ptr, i32 } %11, 0
  %13 = extractvalue { ptr, i32 } %11, 1
  %14 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %12, ptr %14, align 8
  %15 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %13, ptr %15, align 8
  br label %bb4

bb1:                                              ; preds = %start
  %_7.0 = extractvalue { ptr, i64 } %5, 0
  %_7.1 = extractvalue { ptr, i64 } %5, 1
; invoke std::sys::unix::process::process_common::Command::arg
  invoke void @_ZN3std3sys4unix7process14process_common7Command3arg17hcd35fd9c76fd3186E(ptr align 8 %self, ptr align 1 %_7.0, i64 %_7.1)
          to label %bb2 unwind label %cleanup

bb2:                                              ; preds = %bb1
  ret ptr %self
}

; std::process::Command::new
; Function Attrs: nonlazybind uwtable
define internal void @_ZN3std7process7Command3new17h01276f3f68ec9555E(ptr sret(%"std::process::Command") %0, ptr align 1 %1, i64 %2) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %3 = alloca { ptr, i32 }, align 8
  %_2 = alloca %"std::sys::unix::process::process_common::Command", align 8
  %program = alloca { ptr, i64 }, align 8
  %4 = getelementptr inbounds { ptr, i64 }, ptr %program, i32 0, i32 0
  store ptr %1, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i64 }, ptr %program, i32 0, i32 1
  store i64 %2, ptr %5, align 8
; invoke <&T as core::convert::AsRef<U>>::as_ref
  %6 = invoke { ptr, i64 } @"_ZN55_$LT$$RF$T$u20$as$u20$core..convert..AsRef$LT$U$GT$$GT$6as_ref17h37fb207455acf265E"(ptr align 8 %program)
          to label %bb1 unwind label %cleanup

bb4:                                              ; preds = %cleanup
  %7 = load ptr, ptr %3, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %3, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11

cleanup:                                          ; preds = %bb1, %start
  %12 = landingpad { ptr, i32 }
          cleanup
  %13 = extractvalue { ptr, i32 } %12, 0
  %14 = extractvalue { ptr, i32 } %12, 1
  %15 = getelementptr inbounds { ptr, i32 }, ptr %3, i32 0, i32 0
  store ptr %13, ptr %15, align 8
  %16 = getelementptr inbounds { ptr, i32 }, ptr %3, i32 0, i32 1
  store i32 %14, ptr %16, align 8
  br label %bb4

bb1:                                              ; preds = %start
  %_4.0 = extractvalue { ptr, i64 } %6, 0
  %_4.1 = extractvalue { ptr, i64 } %6, 1
; invoke std::sys::unix::process::process_common::Command::new
  invoke void @_ZN3std3sys4unix7process14process_common7Command3new17h2ed3339155d090bcE(ptr sret(%"std::sys::unix::process::process_common::Command") %_2, ptr align 1 %_4.0, i64 %_4.1)
          to label %bb2 unwind label %cleanup

bb2:                                              ; preds = %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_2, i64 208, i1 false)
  ret void
}

; core::mem::maybe_uninit::MaybeUninit<T>::assume_init_drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3mem12maybe_uninit20MaybeUninit$LT$T$GT$16assume_init_drop17h1c4eaa00bb087e26E"(ptr align 8 %self) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<core::option::Option<std::ffi::os_str::OsString>>
  call void @"_ZN4core3ptr75drop_in_place$LT$core..option..Option$LT$std..ffi..os_str..OsString$GT$$GT$17h1f41830fc3f0a80bE"(ptr %self)
  ret void
}

; core::mem::maybe_uninit::MaybeUninit<T>::assume_init_drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3mem12maybe_uninit20MaybeUninit$LT$T$GT$16assume_init_drop17hde1e8179dd2c8220E"(ptr align 8 %self) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<std::ffi::os_str::OsString>
  call void @"_ZN4core3ptr47drop_in_place$LT$std..ffi..os_str..OsString$GT$17h3de6d589d944a230E"(ptr %self)
  ret void
}

; core::mem::replace
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN4core3mem7replace17h6f2a57b6a070d255E(ptr sret(%"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>") %result, ptr align 8 %dest, ptr %src) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %slot = alloca %"core::mem::manually_drop::ManuallyDrop<core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>>", align 8
  %self = alloca %"core::mem::maybe_uninit::MaybeUninit<core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>>", align 8
  %tmp = alloca %"core::mem::maybe_uninit::MaybeUninit<core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>>", align 8
  %src1 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %dest, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %self, ptr align 8 %tmp, i64 32, i1 false)
  br label %bb4

bb4:                                              ; preds = %start
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %slot, ptr align 8 %self, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %result, ptr align 8 %slot, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %src1, ptr align 8 %src, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %dest, ptr align 8 %src1, i64 32, i1 false)
  ret void

bb3:                                              ; No predecessors!
  br i1 true, label %bb2, label %bb1

bb1:                                              ; preds = %bb2, %bb3
  %1 = load ptr, ptr %0, align 8
  %2 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %3 = load i32, ptr %2, align 8
  %4 = insertvalue { ptr, i32 } undef, ptr %1, 0
  %5 = insertvalue { ptr, i32 } %4, i32 %3, 1
  resume { ptr, i32 } %5

bb2:                                              ; preds = %bb3
  br label %bb1
}

; core::mem::replace
; Function Attrs: inlinehint nonlazybind uwtable
define internal { i64, ptr } @_ZN4core3mem7replace17hd43817dbf2300226E(ptr align 8 %dest, i64 %src.0, ptr %src.1) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %tmp = alloca { i64, ptr }, align 8
  %src = alloca { i64, ptr }, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %dest, i64 16, i1 false)
  %1 = getelementptr inbounds { i64, ptr }, ptr %tmp, i32 0, i32 0
  %self.0 = load i64, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %tmp, i32 0, i32 1
  %self.1 = load ptr, ptr %2, align 8
  br label %bb4

bb4:                                              ; preds = %start
  %3 = getelementptr inbounds { i64, ptr }, ptr %src, i32 0, i32 0
  store i64 %src.0, ptr %3, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %src, i32 0, i32 1
  store ptr %src.1, ptr %4, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %dest, ptr align 8 %src, i64 16, i1 false)
  %5 = insertvalue { i64, ptr } undef, i64 %self.0, 0
  %6 = insertvalue { i64, ptr } %5, ptr %self.1, 1
  ret { i64, ptr } %6

bb3:                                              ; No predecessors!
  br i1 true, label %bb2, label %bb1

bb1:                                              ; preds = %bb2, %bb3
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11

bb2:                                              ; preds = %bb3
  br label %bb1
}

; core::ops::function::FnOnce::call_once{{vtable.shim}}
; Function Attrs: inlinehint nonlazybind uwtable
define internal i32 @"_ZN4core3ops8function6FnOnce40call_once$u7b$$u7b$vtable.shim$u7d$$u7d$17h4993a0a72855c69bE"(ptr %_1) unnamed_addr #1 {
start:
  %_2 = alloca {}, align 1
  %0 = load ptr, ptr %_1, align 8, !nonnull !3, !noundef !3
; call core::ops::function::FnOnce::call_once
  %1 = call i32 @_ZN4core3ops8function6FnOnce9call_once17hc57b0b981bafe0ebE(ptr %0)
  ret i32 %1
}

; core::ops::function::FnOnce::call_once
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN4core3ops8function6FnOnce9call_once17h3e2534c22c5c3902E(ptr %_1) unnamed_addr #1 {
start:
  %_2 = alloca {}, align 1
  call void %_1()
  ret void
}

; core::ops::function::FnOnce::call_once
; Function Attrs: inlinehint nonlazybind uwtable
define internal i32 @_ZN4core3ops8function6FnOnce9call_once17hc57b0b981bafe0ebE(ptr %0) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %1 = alloca { ptr, i32 }, align 8
  %_2 = alloca {}, align 1
  %_1 = alloca ptr, align 8
  store ptr %0, ptr %_1, align 8
; invoke std::rt::lang_start::{{closure}}
  %2 = invoke i32 @"_ZN3std2rt10lang_start28_$u7b$$u7b$closure$u7d$$u7d$17hcd759a653706691bE"(ptr align 8 %_1)
          to label %bb1 unwind label %cleanup

bb3:                                              ; preds = %cleanup
  %3 = load ptr, ptr %1, align 8
  %4 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  %5 = load i32, ptr %4, align 8
  %6 = insertvalue { ptr, i32 } undef, ptr %3, 0
  %7 = insertvalue { ptr, i32 } %6, i32 %5, 1
  resume { ptr, i32 } %7

cleanup:                                          ; preds = %start
  %8 = landingpad { ptr, i32 }
          cleanup
  %9 = extractvalue { ptr, i32 } %8, 0
  %10 = extractvalue { ptr, i32 } %8, 1
  %11 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 0
  store ptr %9, ptr %11, align 8
  %12 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  store i32 %10, ptr %12, align 8
  br label %bb3

bb1:                                              ; preds = %start
  ret i32 %2
}

; core::ptr::drop_in_place<std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr101drop_in_place$LT$std..io..error..ErrorData$LT$alloc..boxed..Box$LT$std..io..error..Custom$GT$$GT$$GT$17h7ea2541984c01b00E"(ptr %_1) unnamed_addr #0 {
start:
  %0 = load i8, ptr %_1, align 8, !range !10, !noundef !3
  %_2 = zext i8 %0 to i64
  switch i64 %_2, label %bb2 [
    i64 0, label %bb1
    i64 1, label %bb1
    i64 2, label %bb1
  ]

bb2:                                              ; preds = %start
  %1 = getelementptr inbounds %"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>::Custom", ptr %_1, i32 0, i32 1
; call core::ptr::drop_in_place<alloc::boxed::Box<std::io::error::Custom>>
  call void @"_ZN4core3ptr68drop_in_place$LT$alloc..boxed..Box$LT$std..io..error..Custom$GT$$GT$17h90c5cca08b22d518E"(ptr %1)
  br label %bb1

bb1:                                              ; preds = %bb2, %start, %start, %start
  ret void
}

; core::ptr::drop_in_place<alloc::boxed::Box<dyn core::error::Error+core::marker::Send+core::marker::Sync>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr118drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..error..Error$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h4a2ab39d8c76271fE"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %1 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  %_4.0 = load ptr, ptr %1, align 8
  %2 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %_4.1 = load ptr, ptr %2, align 8, !nonnull !3, !align !4, !noundef !3
  %3 = getelementptr inbounds ptr, ptr %_4.1, i64 0
  %4 = load ptr, ptr %3, align 8, !invariant.load !3, !nonnull !3
  invoke void %4(ptr %_4.0)
          to label %bb3 unwind label %cleanup

bb4:                                              ; preds = %cleanup
  %5 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  %6 = load ptr, ptr %5, align 8, !nonnull !3, !noundef !3
  %7 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %8 = load ptr, ptr %7, align 8, !nonnull !3, !align !4, !noundef !3
; invoke alloc::alloc::box_free
  invoke void @_ZN5alloc5alloc8box_free17hed4c17db642cc9b9E(ptr %6, ptr align 8 %8) #12
          to label %bb2 unwind label %abort

cleanup:                                          ; preds = %start
  %9 = landingpad { ptr, i32 }
          cleanup
  %10 = extractvalue { ptr, i32 } %9, 0
  %11 = extractvalue { ptr, i32 } %9, 1
  %12 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %10, ptr %12, align 8
  %13 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %11, ptr %13, align 8
  br label %bb4

bb3:                                              ; preds = %start
  %14 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  %15 = load ptr, ptr %14, align 8, !nonnull !3, !noundef !3
  %16 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %17 = load ptr, ptr %16, align 8, !nonnull !3, !align !4, !noundef !3
; call alloc::alloc::box_free
  call void @_ZN5alloc5alloc8box_free17hed4c17db642cc9b9E(ptr %15, ptr align 8 %17)
  ret void

abort:                                            ; preds = %bb4
  %18 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb2:                                              ; preds = %bb4
  %19 = load ptr, ptr %0, align 8
  %20 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %21 = load i32, ptr %20, align 8
  %22 = insertvalue { ptr, i32 } undef, ptr %19, 0
  %23 = insertvalue { ptr, i32 } %22, i32 %21, 1
  resume { ptr, i32 } %23
}

; core::ptr::drop_in_place<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString,core::option::Option<std::ffi::os_str::OsString>>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr152drop_in_place$LT$alloc..collections..btree..map..BTreeMap$LT$std..ffi..os_str..OsString$C$core..option..Option$LT$std..ffi..os_str..OsString$GT$$GT$$GT$17h2e39b46051ec6fd7E"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::collections::btree::map::BTreeMap<K,V,A> as core::ops::drop::Drop>::drop
  call void @"_ZN99_$LT$alloc..collections..btree..map..BTreeMap$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17he33172d07d554a1cE"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString,core::option::Option<std::ffi::os_str::OsString>>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr152drop_in_place$LT$alloc..collections..btree..map..IntoIter$LT$std..ffi..os_str..OsString$C$core..option..Option$LT$std..ffi..os_str..OsString$GT$$GT$$GT$17h4d79f7da3a3c05bcE"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::collections::btree::map::IntoIter<K,V,A> as core::ops::drop::Drop>::drop
  call void @"_ZN99_$LT$alloc..collections..btree..map..IntoIter$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17hd8dee439ba401171E"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr203drop_in_place$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$17h54cde62a526280d0E"(ptr %_1.0, ptr align 8 %_1.1) unnamed_addr #0 {
start:
  %0 = getelementptr inbounds ptr, ptr %_1.1, i64 0
  %1 = load ptr, ptr %0, align 8, !invariant.load !3, !nonnull !3
  call void %1(ptr %_1.0)
  ret void
}

; core::ptr::drop_in_place<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr228drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h73c66f97244cf994E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %1 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  %_4.0 = load ptr, ptr %1, align 8
  %2 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %_4.1 = load ptr, ptr %2, align 8, !nonnull !3, !align !4, !noundef !3
  %3 = getelementptr inbounds ptr, ptr %_4.1, i64 0
  %4 = load ptr, ptr %3, align 8, !invariant.load !3, !nonnull !3
  invoke void %4(ptr %_4.0)
          to label %bb3 unwind label %cleanup

bb4:                                              ; preds = %cleanup
  %5 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  %6 = load ptr, ptr %5, align 8, !nonnull !3, !noundef !3
  %7 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %8 = load ptr, ptr %7, align 8, !nonnull !3, !align !4, !noundef !3
; invoke alloc::alloc::box_free
  invoke void @_ZN5alloc5alloc8box_free17hf6cafae124ae9d34E(ptr %6, ptr align 8 %8) #12
          to label %bb2 unwind label %abort

cleanup:                                          ; preds = %start
  %9 = landingpad { ptr, i32 }
          cleanup
  %10 = extractvalue { ptr, i32 } %9, 0
  %11 = extractvalue { ptr, i32 } %9, 1
  %12 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %10, ptr %12, align 8
  %13 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %11, ptr %13, align 8
  br label %bb4

bb3:                                              ; preds = %start
  %14 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  %15 = load ptr, ptr %14, align 8, !nonnull !3, !noundef !3
  %16 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %17 = load ptr, ptr %16, align 8, !nonnull !3, !align !4, !noundef !3
; call alloc::alloc::box_free
  call void @_ZN5alloc5alloc8box_free17hf6cafae124ae9d34E(ptr %15, ptr align 8 %17)
  ret void

abort:                                            ; preds = %bb4
  %18 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb2:                                              ; preds = %bb4
  %19 = load ptr, ptr %0, align 8
  %20 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %21 = load i32, ptr %20, align 8
  %22 = insertvalue { ptr, i32 } undef, ptr %19, 0
  %23 = insertvalue { ptr, i32 } %22, i32 %21, 1
  resume { ptr, i32 } %23
}

; core::ptr::drop_in_place<[alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>]>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr238drop_in_place$LT$$u5b$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$u5d$$GT$17h5bdd31d476922e16E"(ptr %_1.0, i64 %_1.1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_9 = alloca ptr, align 8
  %_4 = alloca i64, align 8
  br i1 false, label %bb7, label %bb12

bb7:                                              ; preds = %start
  store i64 0, ptr %_4, align 8
  br label %bb6

bb12:                                             ; preds = %start
  store ptr %_1.0, ptr %_9, align 8
  %1 = load ptr, ptr %_9, align 8
  %_10 = getelementptr inbounds { ptr, ptr }, ptr %1, i64 %_1.1
  br label %bb11

bb11:                                             ; preds = %bb10, %bb12
  %2 = load ptr, ptr %_9, align 8
  %_14 = icmp eq ptr %2, %_10
  br i1 %_14, label %bb1, label %bb10

bb10:                                             ; preds = %bb11
  %_13 = load ptr, ptr %_9, align 8
  %3 = load ptr, ptr %_9, align 8
  %4 = getelementptr inbounds { ptr, ptr }, ptr %3, i64 1
  store ptr %4, ptr %_9, align 8
; invoke core::ptr::drop_in_place<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>
  invoke void @"_ZN4core3ptr228drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h73c66f97244cf994E"(ptr %_13)
          to label %bb11 unwind label %cleanup

bb1:                                              ; preds = %bb6, %bb11
  ret void

bb9:                                              ; preds = %bb8, %cleanup
  %5 = load ptr, ptr %_9, align 8
  %_12 = icmp eq ptr %5, %_10
  br i1 %_12, label %bb2, label %bb8

cleanup:                                          ; preds = %bb10
  %6 = landingpad { ptr, i32 }
          cleanup
  %7 = extractvalue { ptr, i32 } %6, 0
  %8 = extractvalue { ptr, i32 } %6, 1
  %9 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %7, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %8, ptr %10, align 8
  br label %bb9

bb8:                                              ; preds = %bb9
  %_11 = load ptr, ptr %_9, align 8
  %11 = load ptr, ptr %_9, align 8
  %12 = getelementptr inbounds { ptr, ptr }, ptr %11, i64 1
  store ptr %12, ptr %_9, align 8
; invoke core::ptr::drop_in_place<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>
  invoke void @"_ZN4core3ptr228drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h73c66f97244cf994E"(ptr %_11) #12
          to label %bb9 unwind label %abort

bb2:                                              ; preds = %bb4, %bb9
  %13 = load ptr, ptr %0, align 8
  %14 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %15 = load i32, ptr %14, align 8
  %16 = insertvalue { ptr, i32 } undef, ptr %13, 0
  %17 = insertvalue { ptr, i32 } %16, i32 %15, 1
  resume { ptr, i32 } %17

abort:                                            ; preds = %bb3, %bb8
  %18 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb6:                                              ; preds = %bb5, %bb7
  %19 = load i64, ptr %_4, align 8
  %_8 = icmp eq i64 %19, %_1.1
  br i1 %_8, label %bb1, label %bb5

bb5:                                              ; preds = %bb6
  %20 = load i64, ptr %_4, align 8
  %_7 = getelementptr inbounds [0 x { ptr, ptr }], ptr %_1.0, i64 0, i64 %20
  %21 = load i64, ptr %_4, align 8
  %22 = add i64 %21, 1
  store i64 %22, ptr %_4, align 8
; invoke core::ptr::drop_in_place<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>
  invoke void @"_ZN4core3ptr228drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h73c66f97244cf994E"(ptr %_7)
          to label %bb6 unwind label %cleanup1

bb4:                                              ; preds = %bb3, %cleanup1
  %23 = load i64, ptr %_4, align 8
  %_6 = icmp eq i64 %23, %_1.1
  br i1 %_6, label %bb2, label %bb3

cleanup1:                                         ; preds = %bb5
  %24 = landingpad { ptr, i32 }
          cleanup
  %25 = extractvalue { ptr, i32 } %24, 0
  %26 = extractvalue { ptr, i32 } %24, 1
  %27 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %25, ptr %27, align 8
  %28 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %26, ptr %28, align 8
  br label %bb4

bb3:                                              ; preds = %bb4
  %29 = load i64, ptr %_4, align 8
  %_5 = getelementptr inbounds [0 x { ptr, ptr }], ptr %_1.0, i64 0, i64 %29
  %30 = load i64, ptr %_4, align 8
  %31 = add i64 %30, 1
  store i64 %31, ptr %_4, align 8
; invoke core::ptr::drop_in_place<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>
  invoke void @"_ZN4core3ptr228drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h73c66f97244cf994E"(ptr %_5) #12
          to label %bb4 unwind label %abort
}

; core::ptr::drop_in_place<<alloc::collections::btree::map::IntoIter<K,V,A> as core::ops::drop::Drop>::drop::DropGuard<std::ffi::os_str::OsString,core::option::Option<std::ffi::os_str::OsString>,alloc::alloc::Global>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr250drop_in_place$LT$$LT$alloc..collections..btree..map..IntoIter$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$..drop..DropGuard$LT$std..ffi..os_str..OsString$C$core..option..Option$LT$std..ffi..os_str..OsString$GT$$C$alloc..alloc..Global$GT$$GT$17h5382c191cab528c6E"(ptr %_1) unnamed_addr #0 {
start:
; call <<alloc::collections::btree::map::IntoIter<K,V,A> as core::ops::drop::Drop>::drop::DropGuard<K,V,A> as core::ops::drop::Drop>::drop
  call void @"_ZN174_$LT$$LT$alloc..collections..btree..map..IntoIter$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$..drop..DropGuard$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h68759c0f5d27a4c2E"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<alloc::vec::Vec<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr251drop_in_place$LT$alloc..vec..Vec$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$$GT$17h4528b6b37b658bf5E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; invoke <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
  invoke void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h512452bd996d1bb9E"(ptr align 8 %_1)
          to label %bb4 unwind label %cleanup

bb3:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<alloc::raw_vec::RawVec<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>>
  invoke void @"_ZN4core3ptr258drop_in_place$LT$alloc..raw_vec..RawVec$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$$GT$17h291c309063d11be5E"(ptr %_1) #12
          to label %bb1 unwind label %abort

cleanup:                                          ; preds = %start
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb3

bb4:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::raw_vec::RawVec<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>>
  call void @"_ZN4core3ptr258drop_in_place$LT$alloc..raw_vec..RawVec$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$$GT$17h291c309063d11be5E"(ptr %_1)
  ret void

abort:                                            ; preds = %bb3
  %6 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb1:                                              ; preds = %bb3
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11
}

; core::ptr::drop_in_place<alloc::raw_vec::RawVec<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr258drop_in_place$LT$alloc..raw_vec..RawVec$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$$GT$17h291c309063d11be5E"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
  call void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h0b69a92fd6af8944E"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<std::process::Output>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr41drop_in_place$LT$std..process..Output$GT$17ha110aab2eef55a12E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; invoke core::ptr::drop_in_place<alloc::vec::Vec<u8>>
  invoke void @"_ZN4core3ptr46drop_in_place$LT$alloc..vec..Vec$LT$u8$GT$$GT$17h9db4adbdd1cb23ecE"(ptr %_1)
          to label %bb4 unwind label %cleanup

bb3:                                              ; preds = %cleanup
  %1 = getelementptr inbounds %"std::process::Output", ptr %_1, i32 0, i32 1
; invoke core::ptr::drop_in_place<alloc::vec::Vec<u8>>
  invoke void @"_ZN4core3ptr46drop_in_place$LT$alloc..vec..Vec$LT$u8$GT$$GT$17h9db4adbdd1cb23ecE"(ptr %1) #12
          to label %bb1 unwind label %abort

cleanup:                                          ; preds = %start
  %2 = landingpad { ptr, i32 }
          cleanup
  %3 = extractvalue { ptr, i32 } %2, 0
  %4 = extractvalue { ptr, i32 } %2, 1
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %3, ptr %5, align 8
  %6 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %4, ptr %6, align 8
  br label %bb3

bb4:                                              ; preds = %start
  %7 = getelementptr inbounds %"std::process::Output", ptr %_1, i32 0, i32 1
; call core::ptr::drop_in_place<alloc::vec::Vec<u8>>
  call void @"_ZN4core3ptr46drop_in_place$LT$alloc..vec..Vec$LT$u8$GT$$GT$17h9db4adbdd1cb23ecE"(ptr %7)
  ret void

abort:                                            ; preds = %bb3
  %8 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb1:                                              ; preds = %bb3
  %9 = load ptr, ptr %0, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %11 = load i32, ptr %10, align 8
  %12 = insertvalue { ptr, i32 } undef, ptr %9, 0
  %13 = insertvalue { ptr, i32 } %12, i32 %11, 1
  resume { ptr, i32 } %13
}

; core::ptr::drop_in_place<std::io::error::Error>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17he5cda979aa03a59eE"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<std::io::error::repr_bitpacked::Repr>
  call void @"_ZN4core3ptr57drop_in_place$LT$std..io..error..repr_bitpacked..Repr$GT$17h794e124675f39e31E"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<std::process::Command>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr42drop_in_place$LT$std..process..Command$GT$17hea50fcb0e002b880E"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<std::sys::unix::process::process_common::Command>
  call void @"_ZN4core3ptr69drop_in_place$LT$std..sys..unix..process..process_common..Command$GT$17h044d699c4f9ce521E"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<std::io::error::Custom>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr43drop_in_place$LT$std..io..error..Custom$GT$17h6c00cf5c2cf97964E"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<alloc::boxed::Box<dyn core::error::Error+core::marker::Send+core::marker::Sync>>
  call void @"_ZN4core3ptr118drop_in_place$LT$alloc..boxed..Box$LT$dyn$u20$core..error..Error$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$17h4a2ab39d8c76271fE"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<alloc::vec::Vec<u8>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr46drop_in_place$LT$alloc..vec..Vec$LT$u8$GT$$GT$17h9db4adbdd1cb23ecE"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; invoke <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
  invoke void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h33a13d81e8d11650E"(ptr align 8 %_1)
          to label %bb4 unwind label %cleanup

bb3:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<alloc::raw_vec::RawVec<u8>>
  invoke void @"_ZN4core3ptr53drop_in_place$LT$alloc..raw_vec..RawVec$LT$u8$GT$$GT$17hdb5e1176e86136a2E"(ptr %_1) #12
          to label %bb1 unwind label %abort

cleanup:                                          ; preds = %start
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb3

bb4:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::raw_vec::RawVec<u8>>
  call void @"_ZN4core3ptr53drop_in_place$LT$alloc..raw_vec..RawVec$LT$u8$GT$$GT$17hdb5e1176e86136a2E"(ptr %_1)
  ret void

abort:                                            ; preds = %bb3
  %6 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb1:                                              ; preds = %bb3
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11
}

; core::ptr::drop_in_place<alloc::ffi::c_str::CString>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; invoke <alloc::ffi::c_str::CString as core::ops::drop::Drop>::drop
  invoke void @"_ZN68_$LT$alloc..ffi..c_str..CString$u20$as$u20$core..ops..drop..Drop$GT$4drop17h7a429437c6d8e0a4E"(ptr align 8 %_1)
          to label %bb4 unwind label %cleanup

bb3:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<alloc::boxed::Box<[u8]>>
  invoke void @"_ZN4core3ptr58drop_in_place$LT$alloc..boxed..Box$LT$$u5b$u8$u5d$$GT$$GT$17h41de86df630c8dd0E"(ptr %_1) #12
          to label %bb1 unwind label %abort

cleanup:                                          ; preds = %start
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb3

bb4:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::boxed::Box<[u8]>>
  call void @"_ZN4core3ptr58drop_in_place$LT$alloc..boxed..Box$LT$$u5b$u8$u5d$$GT$$GT$17h41de86df630c8dd0E"(ptr %_1)
  ret void

abort:                                            ; preds = %bb3
  %6 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb1:                                              ; preds = %bb3
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11
}

; core::ptr::drop_in_place<std::ffi::os_str::OsString>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr47drop_in_place$LT$std..ffi..os_str..OsString$GT$17h3de6d589d944a230E"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<std::sys::unix::os_str::Buf>
  call void @"_ZN4core3ptr48drop_in_place$LT$std..sys..unix..os_str..Buf$GT$17h8f9f697b94f7833aE"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<std::os::fd::owned::OwnedFd>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr48drop_in_place$LT$std..os..fd..owned..OwnedFd$GT$17h91ef5b2edc5c27d6E"(ptr %_1) unnamed_addr #0 {
start:
; call <std::os::fd::owned::OwnedFd as core::ops::drop::Drop>::drop
  call void @"_ZN69_$LT$std..os..fd..owned..OwnedFd$u20$as$u20$core..ops..drop..Drop$GT$4drop17h727ef452e9e8af54E"(ptr align 4 %_1)
  ret void
}

; core::ptr::drop_in_place<std::sys::unix::os_str::Buf>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr48drop_in_place$LT$std..sys..unix..os_str..Buf$GT$17h8f9f697b94f7833aE"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<alloc::vec::Vec<u8>>
  call void @"_ZN4core3ptr46drop_in_place$LT$alloc..vec..Vec$LT$u8$GT$$GT$17h9db4adbdd1cb23ecE"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<std::sys::unix::fd::FileDesc>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr49drop_in_place$LT$std..sys..unix..fd..FileDesc$GT$17ha95ba1d5d18f6da9E"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<std::os::fd::owned::OwnedFd>
  call void @"_ZN4core3ptr48drop_in_place$LT$std..os..fd..owned..OwnedFd$GT$17h91ef5b2edc5c27d6E"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<alloc::raw_vec::RawVec<u8>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr53drop_in_place$LT$alloc..raw_vec..RawVec$LT$u8$GT$$GT$17hdb5e1176e86136a2E"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
  call void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17ha6e917ec4bc18abdE"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<[alloc::ffi::c_str::CString]>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr57drop_in_place$LT$$u5b$alloc..ffi..c_str..CString$u5d$$GT$17h66826af11cfec2b6E"(ptr %_1.0, i64 %_1.1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_9 = alloca ptr, align 8
  %_4 = alloca i64, align 8
  br i1 false, label %bb7, label %bb12

bb7:                                              ; preds = %start
  store i64 0, ptr %_4, align 8
  br label %bb6

bb12:                                             ; preds = %start
  store ptr %_1.0, ptr %_9, align 8
  %1 = load ptr, ptr %_9, align 8
  %_10 = getelementptr inbounds { ptr, i64 }, ptr %1, i64 %_1.1
  br label %bb11

bb11:                                             ; preds = %bb10, %bb12
  %2 = load ptr, ptr %_9, align 8
  %_14 = icmp eq ptr %2, %_10
  br i1 %_14, label %bb1, label %bb10

bb10:                                             ; preds = %bb11
  %_13 = load ptr, ptr %_9, align 8
  %3 = load ptr, ptr %_9, align 8
  %4 = getelementptr inbounds { ptr, i64 }, ptr %3, i64 1
  store ptr %4, ptr %_9, align 8
; invoke core::ptr::drop_in_place<alloc::ffi::c_str::CString>
  invoke void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %_13)
          to label %bb11 unwind label %cleanup

bb1:                                              ; preds = %bb6, %bb11
  ret void

bb9:                                              ; preds = %bb8, %cleanup
  %5 = load ptr, ptr %_9, align 8
  %_12 = icmp eq ptr %5, %_10
  br i1 %_12, label %bb2, label %bb8

cleanup:                                          ; preds = %bb10
  %6 = landingpad { ptr, i32 }
          cleanup
  %7 = extractvalue { ptr, i32 } %6, 0
  %8 = extractvalue { ptr, i32 } %6, 1
  %9 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %7, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %8, ptr %10, align 8
  br label %bb9

bb8:                                              ; preds = %bb9
  %_11 = load ptr, ptr %_9, align 8
  %11 = load ptr, ptr %_9, align 8
  %12 = getelementptr inbounds { ptr, i64 }, ptr %11, i64 1
  store ptr %12, ptr %_9, align 8
; invoke core::ptr::drop_in_place<alloc::ffi::c_str::CString>
  invoke void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %_11) #12
          to label %bb9 unwind label %abort

bb2:                                              ; preds = %bb4, %bb9
  %13 = load ptr, ptr %0, align 8
  %14 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %15 = load i32, ptr %14, align 8
  %16 = insertvalue { ptr, i32 } undef, ptr %13, 0
  %17 = insertvalue { ptr, i32 } %16, i32 %15, 1
  resume { ptr, i32 } %17

abort:                                            ; preds = %bb3, %bb8
  %18 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb6:                                              ; preds = %bb5, %bb7
  %19 = load i64, ptr %_4, align 8
  %_8 = icmp eq i64 %19, %_1.1
  br i1 %_8, label %bb1, label %bb5

bb5:                                              ; preds = %bb6
  %20 = load i64, ptr %_4, align 8
  %_7 = getelementptr inbounds [0 x { ptr, i64 }], ptr %_1.0, i64 0, i64 %20
  %21 = load i64, ptr %_4, align 8
  %22 = add i64 %21, 1
  store i64 %22, ptr %_4, align 8
; invoke core::ptr::drop_in_place<alloc::ffi::c_str::CString>
  invoke void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %_7)
          to label %bb6 unwind label %cleanup1

bb4:                                              ; preds = %bb3, %cleanup1
  %23 = load i64, ptr %_4, align 8
  %_6 = icmp eq i64 %23, %_1.1
  br i1 %_6, label %bb2, label %bb3

cleanup1:                                         ; preds = %bb5
  %24 = landingpad { ptr, i32 }
          cleanup
  %25 = extractvalue { ptr, i32 } %24, 0
  %26 = extractvalue { ptr, i32 } %24, 1
  %27 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %25, ptr %27, align 8
  %28 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %26, ptr %28, align 8
  br label %bb4

bb3:                                              ; preds = %bb4
  %29 = load i64, ptr %_4, align 8
  %_5 = getelementptr inbounds [0 x { ptr, i64 }], ptr %_1.0, i64 0, i64 %29
  %30 = load i64, ptr %_4, align 8
  %31 = add i64 %30, 1
  store i64 %31, ptr %_4, align 8
; invoke core::ptr::drop_in_place<alloc::ffi::c_str::CString>
  invoke void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %_5) #12
          to label %bb4 unwind label %abort
}

; core::ptr::drop_in_place<std::io::error::repr_bitpacked::Repr>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr57drop_in_place$LT$std..io..error..repr_bitpacked..Repr$GT$17h794e124675f39e31E"(ptr %_1) unnamed_addr #0 {
start:
; call <std::io::error::repr_bitpacked::Repr as core::ops::drop::Drop>::drop
  call void @"_ZN78_$LT$std..io..error..repr_bitpacked..Repr$u20$as$u20$core..ops..drop..Drop$GT$4drop17ha28e326fb8f179ebE"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<std::sys_common::process::CommandEnv>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr57drop_in_place$LT$std..sys_common..process..CommandEnv$GT$17h7242641d1397c356E"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString,core::option::Option<std::ffi::os_str::OsString>>>
  call void @"_ZN4core3ptr152drop_in_place$LT$alloc..collections..btree..map..BTreeMap$LT$std..ffi..os_str..OsString$C$core..option..Option$LT$std..ffi..os_str..OsString$GT$$GT$$GT$17h2e39b46051ec6fd7E"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<alloc::boxed::Box<[u8]>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr58drop_in_place$LT$alloc..boxed..Box$LT$$u5b$u8$u5d$$GT$$GT$17h41de86df630c8dd0E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %1 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 0
  %_4.0 = load ptr, ptr %1, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 1
  %_4.1 = load i64, ptr %2, align 8
  br label %bb3

bb3:                                              ; preds = %start
  %3 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 0
  %4 = load ptr, ptr %3, align 8, !nonnull !3, !noundef !3
  %5 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 1
  %6 = load i64, ptr %5, align 8
; call alloc::alloc::box_free
  call void @_ZN5alloc5alloc8box_free17ha6e557d1a719ef37E(ptr %4, i64 %6)
  ret void

bb4:                                              ; No predecessors!
  %7 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 0
  %8 = load ptr, ptr %7, align 8, !nonnull !3, !noundef !3
  %9 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 1
  %10 = load i64, ptr %9, align 8
; invoke alloc::alloc::box_free
  invoke void @_ZN5alloc5alloc8box_free17ha6e557d1a719ef37E(ptr %8, i64 %10) #12
          to label %bb2 unwind label %abort

abort:                                            ; preds = %bb4
  %11 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb2:                                              ; preds = %bb4
  %12 = load ptr, ptr %0, align 8
  %13 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %14 = load i32, ptr %13, align 8
  %15 = insertvalue { ptr, i32 } undef, ptr %12, 0
  %16 = insertvalue { ptr, i32 } %15, i32 %14, 1
  resume { ptr, i32 } %16
}

; core::ptr::drop_in_place<alloc::boxed::Box<[u32]>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr59drop_in_place$LT$alloc..boxed..Box$LT$$u5b$u32$u5d$$GT$$GT$17hb8118f4c6aa65dc3E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %1 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 0
  %_4.0 = load ptr, ptr %1, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 1
  %_4.1 = load i64, ptr %2, align 8
  br label %bb3

bb3:                                              ; preds = %start
  %3 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 0
  %4 = load ptr, ptr %3, align 8, !nonnull !3, !noundef !3
  %5 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 1
  %6 = load i64, ptr %5, align 8
; call alloc::alloc::box_free
  call void @_ZN5alloc5alloc8box_free17hdddc60ee21b60f9cE(ptr %4, i64 %6)
  ret void

bb4:                                              ; No predecessors!
  %7 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 0
  %8 = load ptr, ptr %7, align 8, !nonnull !3, !noundef !3
  %9 = getelementptr inbounds { ptr, i64 }, ptr %_1, i32 0, i32 1
  %10 = load i64, ptr %9, align 8
; invoke alloc::alloc::box_free
  invoke void @_ZN5alloc5alloc8box_free17hdddc60ee21b60f9cE(ptr %8, i64 %10) #12
          to label %bb2 unwind label %abort

abort:                                            ; preds = %bb4
  %11 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb2:                                              ; preds = %bb4
  %12 = load ptr, ptr %0, align 8
  %13 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %14 = load i32, ptr %13, align 8
  %15 = insertvalue { ptr, i32 } undef, ptr %12, 0
  %16 = insertvalue { ptr, i32 } %15, i32 %14, 1
  resume { ptr, i32 } %16
}

; core::ptr::drop_in_place<alloc::vec::Vec<*const i8>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr60drop_in_place$LT$alloc..vec..Vec$LT$$BP$const$u20$i8$GT$$GT$17h0e877dd013ad810bE"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; invoke <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
  invoke void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17hd517229758d55e44E"(ptr align 8 %_1)
          to label %bb4 unwind label %cleanup

bb3:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<alloc::raw_vec::RawVec<*const i8>>
  invoke void @"_ZN4core3ptr67drop_in_place$LT$alloc..raw_vec..RawVec$LT$$BP$const$u20$i8$GT$$GT$17h3cc88ada44f3e272E"(ptr %_1) #12
          to label %bb1 unwind label %abort

cleanup:                                          ; preds = %start
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb3

bb4:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::raw_vec::RawVec<*const i8>>
  call void @"_ZN4core3ptr67drop_in_place$LT$alloc..raw_vec..RawVec$LT$$BP$const$u20$i8$GT$$GT$17h3cc88ada44f3e272E"(ptr %_1)
  ret void

abort:                                            ; preds = %bb3
  %6 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb1:                                              ; preds = %bb3
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11
}

; core::ptr::drop_in_place<std::sys::unix::process::process_common::Argv>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr66drop_in_place$LT$std..sys..unix..process..process_common..Argv$GT$17h1e4ed23c51ea2fedE"(ptr %_1) unnamed_addr #0 {
start:
; call core::ptr::drop_in_place<alloc::vec::Vec<*const i8>>
  call void @"_ZN4core3ptr60drop_in_place$LT$alloc..vec..Vec$LT$$BP$const$u20$i8$GT$$GT$17h0e877dd013ad810bE"(ptr %_1)
  ret void
}

; core::ptr::drop_in_place<alloc::raw_vec::RawVec<*const i8>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr67drop_in_place$LT$alloc..raw_vec..RawVec$LT$$BP$const$u20$i8$GT$$GT$17h3cc88ada44f3e272E"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
  call void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h7870d79897905bbcE"(ptr align 8 %_1)
  ret void
}

; core::ptr::drop_in_place<std::sys::unix::process::process_common::Stdio>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr67drop_in_place$LT$std..sys..unix..process..process_common..Stdio$GT$17h99fc56b2bc90461fE"(ptr %_1) unnamed_addr #0 {
start:
  %0 = load i32, ptr %_1, align 4, !range !11, !noundef !3
  %_2 = zext i32 %0 to i64
  switch i64 %_2, label %bb2 [
    i64 0, label %bb1
    i64 1, label %bb1
    i64 2, label %bb1
  ]

bb2:                                              ; preds = %start
  %1 = getelementptr inbounds { i32, i32 }, ptr %_1, i32 0, i32 1
; call core::ptr::drop_in_place<std::sys::unix::fd::FileDesc>
  call void @"_ZN4core3ptr49drop_in_place$LT$std..sys..unix..fd..FileDesc$GT$17ha95ba1d5d18f6da9E"(ptr %1)
  br label %bb1

bb1:                                              ; preds = %bb2, %start, %start, %start
  ret void
}

; core::ptr::drop_in_place<alloc::boxed::Box<std::io::error::Custom>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr68drop_in_place$LT$alloc..boxed..Box$LT$std..io..error..Custom$GT$$GT$17h90c5cca08b22d518E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_4 = load ptr, ptr %_1, align 8
; invoke core::ptr::drop_in_place<std::io::error::Custom>
  invoke void @"_ZN4core3ptr43drop_in_place$LT$std..io..error..Custom$GT$17h6c00cf5c2cf97964E"(ptr %_4)
          to label %bb3 unwind label %cleanup

bb4:                                              ; preds = %cleanup
  %1 = load ptr, ptr %_1, align 8, !nonnull !3, !noundef !3
; invoke alloc::alloc::box_free
  invoke void @_ZN5alloc5alloc8box_free17h5622a6443d75e9faE(ptr %1) #12
          to label %bb2 unwind label %abort

cleanup:                                          ; preds = %start
  %2 = landingpad { ptr, i32 }
          cleanup
  %3 = extractvalue { ptr, i32 } %2, 0
  %4 = extractvalue { ptr, i32 } %2, 1
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %3, ptr %5, align 8
  %6 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %4, ptr %6, align 8
  br label %bb4

bb3:                                              ; preds = %start
  %7 = load ptr, ptr %_1, align 8, !nonnull !3, !noundef !3
; call alloc::alloc::box_free
  call void @_ZN5alloc5alloc8box_free17h5622a6443d75e9faE(ptr %7)
  ret void

abort:                                            ; preds = %bb4
  %8 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb2:                                              ; preds = %bb4
  %9 = load ptr, ptr %0, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %11 = load i32, ptr %10, align 8
  %12 = insertvalue { ptr, i32 } undef, ptr %9, 0
  %13 = insertvalue { ptr, i32 } %12, i32 %11, 1
  resume { ptr, i32 } %13
}

; core::ptr::drop_in_place<std::sys::unix::process::process_common::Command>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr69drop_in_place$LT$std..sys..unix..process..process_common..Command$GT$17h044d699c4f9ce521E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %1 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 3
; invoke core::ptr::drop_in_place<alloc::ffi::c_str::CString>
  invoke void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %1)
          to label %bb20 unwind label %cleanup

bb11:                                             ; preds = %cleanup
  %2 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 4
; invoke core::ptr::drop_in_place<alloc::vec::Vec<alloc::ffi::c_str::CString>>
  invoke void @"_ZN4core3ptr70drop_in_place$LT$alloc..vec..Vec$LT$alloc..ffi..c_str..CString$GT$$GT$17h33c9f589606e4a43E"(ptr %2) #12
          to label %bb10 unwind label %abort

cleanup:                                          ; preds = %start
  %3 = landingpad { ptr, i32 }
          cleanup
  %4 = extractvalue { ptr, i32 } %3, 0
  %5 = extractvalue { ptr, i32 } %3, 1
  %6 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %4, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %5, ptr %7, align 8
  br label %bb11

bb20:                                             ; preds = %start
  %8 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 4
; invoke core::ptr::drop_in_place<alloc::vec::Vec<alloc::ffi::c_str::CString>>
  invoke void @"_ZN4core3ptr70drop_in_place$LT$alloc..vec..Vec$LT$alloc..ffi..c_str..CString$GT$$GT$17h33c9f589606e4a43E"(ptr %8)
          to label %bb19 unwind label %cleanup1

abort:                                            ; preds = %bb3, %bb4, %bb5, %bb6, %bb7, %bb8, %bb9, %bb10, %bb11
  %9 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb10:                                             ; preds = %cleanup1, %bb11
  %10 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 5
; invoke core::ptr::drop_in_place<std::sys::unix::process::process_common::Argv>
  invoke void @"_ZN4core3ptr66drop_in_place$LT$std..sys..unix..process..process_common..Argv$GT$17h1e4ed23c51ea2fedE"(ptr %10) #12
          to label %bb9 unwind label %abort

cleanup1:                                         ; preds = %bb20
  %11 = landingpad { ptr, i32 }
          cleanup
  %12 = extractvalue { ptr, i32 } %11, 0
  %13 = extractvalue { ptr, i32 } %11, 1
  %14 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %12, ptr %14, align 8
  %15 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %13, ptr %15, align 8
  br label %bb10

bb19:                                             ; preds = %bb20
  %16 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 5
; invoke core::ptr::drop_in_place<std::sys::unix::process::process_common::Argv>
  invoke void @"_ZN4core3ptr66drop_in_place$LT$std..sys..unix..process..process_common..Argv$GT$17h1e4ed23c51ea2fedE"(ptr %16)
          to label %bb18 unwind label %cleanup2

bb9:                                              ; preds = %cleanup2, %bb10
; invoke core::ptr::drop_in_place<std::sys_common::process::CommandEnv>
  invoke void @"_ZN4core3ptr57drop_in_place$LT$std..sys_common..process..CommandEnv$GT$17h7242641d1397c356E"(ptr %_1) #12
          to label %bb8 unwind label %abort

cleanup2:                                         ; preds = %bb19
  %17 = landingpad { ptr, i32 }
          cleanup
  %18 = extractvalue { ptr, i32 } %17, 0
  %19 = extractvalue { ptr, i32 } %17, 1
  %20 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %18, ptr %20, align 8
  %21 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %19, ptr %21, align 8
  br label %bb9

bb18:                                             ; preds = %bb19
; invoke core::ptr::drop_in_place<std::sys_common::process::CommandEnv>
  invoke void @"_ZN4core3ptr57drop_in_place$LT$std..sys_common..process..CommandEnv$GT$17h7242641d1397c356E"(ptr %_1)
          to label %bb17 unwind label %cleanup3

bb8:                                              ; preds = %cleanup3, %bb9
  %22 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 1
; invoke core::ptr::drop_in_place<core::option::Option<alloc::ffi::c_str::CString>>
  invoke void @"_ZN4core3ptr75drop_in_place$LT$core..option..Option$LT$alloc..ffi..c_str..CString$GT$$GT$17ha56f49bf12659538E"(ptr %22) #12
          to label %bb7 unwind label %abort

cleanup3:                                         ; preds = %bb18
  %23 = landingpad { ptr, i32 }
          cleanup
  %24 = extractvalue { ptr, i32 } %23, 0
  %25 = extractvalue { ptr, i32 } %23, 1
  %26 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %24, ptr %26, align 8
  %27 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %25, ptr %27, align 8
  br label %bb8

bb17:                                             ; preds = %bb18
  %28 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 1
; invoke core::ptr::drop_in_place<core::option::Option<alloc::ffi::c_str::CString>>
  invoke void @"_ZN4core3ptr75drop_in_place$LT$core..option..Option$LT$alloc..ffi..c_str..CString$GT$$GT$17ha56f49bf12659538E"(ptr %28)
          to label %bb16 unwind label %cleanup4

bb7:                                              ; preds = %cleanup4, %bb8
  %29 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 6
; invoke core::ptr::drop_in_place<alloc::vec::Vec<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>>
  invoke void @"_ZN4core3ptr251drop_in_place$LT$alloc..vec..Vec$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$$GT$17h4528b6b37b658bf5E"(ptr %29) #12
          to label %bb6 unwind label %abort

cleanup4:                                         ; preds = %bb17
  %30 = landingpad { ptr, i32 }
          cleanup
  %31 = extractvalue { ptr, i32 } %30, 0
  %32 = extractvalue { ptr, i32 } %30, 1
  %33 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %31, ptr %33, align 8
  %34 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %32, ptr %34, align 8
  br label %bb7

bb16:                                             ; preds = %bb17
  %35 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 6
; invoke core::ptr::drop_in_place<alloc::vec::Vec<alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>>>
  invoke void @"_ZN4core3ptr251drop_in_place$LT$alloc..vec..Vec$LT$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$GT$$GT$17h4528b6b37b658bf5E"(ptr %35)
          to label %bb15 unwind label %cleanup5

bb6:                                              ; preds = %cleanup5, %bb7
  %36 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 2
; invoke core::ptr::drop_in_place<core::option::Option<alloc::boxed::Box<[u32]>>>
  invoke void @"_ZN4core3ptr87drop_in_place$LT$core..option..Option$LT$alloc..boxed..Box$LT$$u5b$u32$u5d$$GT$$GT$$GT$17h6c5029321880c967E"(ptr %36) #12
          to label %bb5 unwind label %abort

cleanup5:                                         ; preds = %bb16
  %37 = landingpad { ptr, i32 }
          cleanup
  %38 = extractvalue { ptr, i32 } %37, 0
  %39 = extractvalue { ptr, i32 } %37, 1
  %40 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %38, ptr %40, align 8
  %41 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %39, ptr %41, align 8
  br label %bb6

bb15:                                             ; preds = %bb16
  %42 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 2
; invoke core::ptr::drop_in_place<core::option::Option<alloc::boxed::Box<[u32]>>>
  invoke void @"_ZN4core3ptr87drop_in_place$LT$core..option..Option$LT$alloc..boxed..Box$LT$$u5b$u32$u5d$$GT$$GT$$GT$17h6c5029321880c967E"(ptr %42)
          to label %bb14 unwind label %cleanup6

bb5:                                              ; preds = %cleanup6, %bb6
  %43 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 7
; invoke core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
  invoke void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %43) #12
          to label %bb4 unwind label %abort

cleanup6:                                         ; preds = %bb15
  %44 = landingpad { ptr, i32 }
          cleanup
  %45 = extractvalue { ptr, i32 } %44, 0
  %46 = extractvalue { ptr, i32 } %44, 1
  %47 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %45, ptr %47, align 8
  %48 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %46, ptr %48, align 8
  br label %bb5

bb14:                                             ; preds = %bb15
  %49 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 7
; invoke core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
  invoke void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %49)
          to label %bb13 unwind label %cleanup7

bb4:                                              ; preds = %cleanup7, %bb5
  %50 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 8
; invoke core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
  invoke void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %50) #12
          to label %bb3 unwind label %abort

cleanup7:                                         ; preds = %bb14
  %51 = landingpad { ptr, i32 }
          cleanup
  %52 = extractvalue { ptr, i32 } %51, 0
  %53 = extractvalue { ptr, i32 } %51, 1
  %54 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %52, ptr %54, align 8
  %55 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %53, ptr %55, align 8
  br label %bb4

bb13:                                             ; preds = %bb14
  %56 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 8
; invoke core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
  invoke void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %56)
          to label %bb12 unwind label %cleanup8

bb3:                                              ; preds = %cleanup8, %bb4
  %57 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 9
; invoke core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
  invoke void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %57) #12
          to label %bb1 unwind label %abort

cleanup8:                                         ; preds = %bb13
  %58 = landingpad { ptr, i32 }
          cleanup
  %59 = extractvalue { ptr, i32 } %58, 0
  %60 = extractvalue { ptr, i32 } %58, 1
  %61 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %59, ptr %61, align 8
  %62 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %60, ptr %62, align 8
  br label %bb3

bb12:                                             ; preds = %bb13
  %63 = getelementptr inbounds %"std::sys::unix::process::process_common::Command", ptr %_1, i32 0, i32 9
; call core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
  call void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %63)
  ret void

bb1:                                              ; preds = %bb3
  %64 = load ptr, ptr %0, align 8
  %65 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %66 = load i32, ptr %65, align 8
  %67 = insertvalue { ptr, i32 } undef, ptr %64, 0
  %68 = insertvalue { ptr, i32 } %67, i32 %66, 1
  resume { ptr, i32 } %68
}

; core::ptr::drop_in_place<alloc::vec::Vec<alloc::ffi::c_str::CString>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr70drop_in_place$LT$alloc..vec..Vec$LT$alloc..ffi..c_str..CString$GT$$GT$17h33c9f589606e4a43E"(ptr %_1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
; invoke <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
  invoke void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h9edb5011854831adE"(ptr align 8 %_1)
          to label %bb4 unwind label %cleanup

bb3:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<alloc::raw_vec::RawVec<alloc::ffi::c_str::CString>>
  invoke void @"_ZN4core3ptr77drop_in_place$LT$alloc..raw_vec..RawVec$LT$alloc..ffi..c_str..CString$GT$$GT$17h3175be13b824576eE"(ptr %_1) #12
          to label %bb1 unwind label %abort

cleanup:                                          ; preds = %start
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb3

bb4:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::raw_vec::RawVec<alloc::ffi::c_str::CString>>
  call void @"_ZN4core3ptr77drop_in_place$LT$alloc..raw_vec..RawVec$LT$alloc..ffi..c_str..CString$GT$$GT$17h3175be13b824576eE"(ptr %_1)
  ret void

abort:                                            ; preds = %bb3
  %6 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb1:                                              ; preds = %bb3
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11
}

; core::ptr::drop_in_place<alloc::collections::btree::mem::replace::PanicGuard>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr72drop_in_place$LT$alloc..collections..btree..mem..replace..PanicGuard$GT$17ha0825560598d6990E"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::collections::btree::mem::replace::PanicGuard as core::ops::drop::Drop>::drop
  call void @"_ZN93_$LT$alloc..collections..btree..mem..replace..PanicGuard$u20$as$u20$core..ops..drop..Drop$GT$4drop17h1d57ee459902bc41E"(ptr align 1 %_1)
  ret void
}

; core::ptr::drop_in_place<core::option::Option<alloc::ffi::c_str::CString>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr75drop_in_place$LT$core..option..Option$LT$alloc..ffi..c_str..CString$GT$$GT$17ha56f49bf12659538E"(ptr %_1) unnamed_addr #0 {
start:
  %0 = load ptr, ptr %_1, align 8
  %1 = ptrtoint ptr %0 to i64
  %2 = icmp eq i64 %1, 0
  %_2 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_2, 0
  br i1 %3, label %bb1, label %bb2

bb1:                                              ; preds = %bb2, %start
  ret void

bb2:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::ffi::c_str::CString>
  call void @"_ZN4core3ptr47drop_in_place$LT$alloc..ffi..c_str..CString$GT$17h1da6804eef26dce4E"(ptr %_1)
  br label %bb1
}

; core::ptr::drop_in_place<core::option::Option<std::ffi::os_str::OsString>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr75drop_in_place$LT$core..option..Option$LT$std..ffi..os_str..OsString$GT$$GT$17h1f41830fc3f0a80bE"(ptr %_1) unnamed_addr #0 {
start:
  %0 = getelementptr inbounds %"core::option::Option<std::ffi::os_str::OsString>", ptr %_1, i32 0, i32 1
  %1 = load ptr, ptr %0, align 8
  %2 = ptrtoint ptr %1 to i64
  %3 = icmp eq i64 %2, 0
  %_2 = select i1 %3, i64 0, i64 1
  %4 = icmp eq i64 %_2, 0
  br i1 %4, label %bb1, label %bb2

bb1:                                              ; preds = %bb2, %start
  ret void

bb2:                                              ; preds = %start
; call core::ptr::drop_in_place<std::ffi::os_str::OsString>
  call void @"_ZN4core3ptr47drop_in_place$LT$std..ffi..os_str..OsString$GT$17h3de6d589d944a230E"(ptr %_1)
  br label %bb1
}

; core::ptr::drop_in_place<alloc::raw_vec::RawVec<alloc::ffi::c_str::CString>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr77drop_in_place$LT$alloc..raw_vec..RawVec$LT$alloc..ffi..c_str..CString$GT$$GT$17h3175be13b824576eE"(ptr %_1) unnamed_addr #0 {
start:
; call <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
  call void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h9170bbe0d5240382E"(ptr align 8 %_1)
  ret void
}

; core::ptr::mut_ptr::<impl *mut T>::guaranteed_eq
; Function Attrs: inlinehint nonlazybind uwtable
define internal i8 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$13guaranteed_eq17h52a0c5a0a529ee00E"(ptr %self, ptr %other) unnamed_addr #1 {
start:
  %0 = alloca i8, align 1
  %1 = alloca i8, align 1
  %2 = icmp eq ptr %self, %other
  %3 = zext i1 %2 to i8
  store i8 %3, ptr %0, align 1
  %_8 = load i8, ptr %0, align 1
  %4 = icmp eq i8 %_8, 2
  br i1 %4, label %bb3, label %bb2

bb3:                                              ; preds = %start
  store i8 2, ptr %1, align 1
  br label %bb4

bb2:                                              ; preds = %start
  %_14 = icmp eq i8 %_8, 1
  %5 = zext i1 %_14 to i8
  store i8 %5, ptr %1, align 1
  br label %bb4

bb4:                                              ; preds = %bb3, %bb2
  %6 = load i8, ptr %1, align 1, !range !12, !noundef !3
  ret i8 %6
}

; core::ptr::mut_ptr::<impl *mut T>::with_metadata_of
; Function Attrs: inlinehint nonlazybind uwtable
define internal ptr @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$16with_metadata_of17h7ccf771fd8534e89E"(ptr %self, ptr %meta) unnamed_addr #1 {
start:
  %_10 = alloca %"core::ptr::metadata::PtrComponents<()>", align 8
  %_9 = alloca %"core::ptr::metadata::PtrRepr<()>", align 8
  %_7 = alloca %"core::ptr::metadata::PtrRepr<()>", align 8
  store ptr %meta, ptr %_7, align 8
  store ptr %self, ptr %_10, align 8
  %0 = getelementptr inbounds %"core::ptr::metadata::PtrComponents<()>", ptr %_10, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_9, ptr align 8 %_10, i64 8, i1 false)
  %1 = load ptr, ptr %_9, align 8
  ret ptr %1
}

; core::ptr::mut_ptr::<impl *mut T>::is_null
; Function Attrs: inlinehint nonlazybind uwtable
define internal zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17h0ad6e4575a979414E"(ptr %self) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %_13 = alloca %"core::ptr::metadata::PtrComponents<u8>", align 8
  %_12 = alloca %"core::ptr::metadata::PtrRepr<u8>", align 8
  %_2 = alloca i8, align 1
  %1 = alloca i8, align 1
  store i64 0, ptr %0, align 8
  %data_address = load ptr, ptr %0, align 8
  store ptr %data_address, ptr %_13, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_12, ptr align 8 %_13, i64 8, i1 false)
  %_5 = load ptr, ptr %_12, align 8
; call core::ptr::mut_ptr::<impl *mut T>::guaranteed_eq
  %2 = call i8 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$13guaranteed_eq17h52a0c5a0a529ee00E"(ptr %self, ptr %_5), !range !12
  store i8 %2, ptr %_2, align 1
  %3 = load i8, ptr %_2, align 1, !range !12, !noundef !3
  %4 = icmp eq i8 %3, 2
  %_6 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_6, 0
  br i1 %5, label %bb4, label %bb2

bb4:                                              ; preds = %start
  store i8 0, ptr %1, align 1
  br label %bb5

bb2:                                              ; preds = %start
  %6 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %res = trunc i8 %6 to i1
  %7 = zext i1 %res to i8
  store i8 %7, ptr %1, align 1
  br label %bb5

bb3:                                              ; No predecessors!
  unreachable

bb5:                                              ; preds = %bb4, %bb2
  %8 = load i8, ptr %1, align 1, !range !7, !noundef !3
  %9 = trunc i8 %8 to i1
  ret i1 %9
}

; core::ptr::mut_ptr::<impl *mut T>::is_null
; Function Attrs: inlinehint nonlazybind uwtable
define internal zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17h1c7864747c3c4ee2E"(ptr %self) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %_13 = alloca %"core::ptr::metadata::PtrComponents<u8>", align 8
  %_12 = alloca %"core::ptr::metadata::PtrRepr<u8>", align 8
  %_2 = alloca i8, align 1
  %1 = alloca i8, align 1
  store i64 0, ptr %0, align 8
  %data_address = load ptr, ptr %0, align 8
  store ptr %data_address, ptr %_13, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_12, ptr align 8 %_13, i64 8, i1 false)
  %_5 = load ptr, ptr %_12, align 8
; call core::ptr::mut_ptr::<impl *mut T>::guaranteed_eq
  %2 = call i8 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$13guaranteed_eq17h52a0c5a0a529ee00E"(ptr %self, ptr %_5), !range !12
  store i8 %2, ptr %_2, align 1
  %3 = load i8, ptr %_2, align 1, !range !12, !noundef !3
  %4 = icmp eq i8 %3, 2
  %_6 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_6, 0
  br i1 %5, label %bb4, label %bb2

bb4:                                              ; preds = %start
  store i8 0, ptr %1, align 1
  br label %bb5

bb2:                                              ; preds = %start
  %6 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %res = trunc i8 %6 to i1
  %7 = zext i1 %res to i8
  store i8 %7, ptr %1, align 1
  br label %bb5

bb3:                                              ; No predecessors!
  unreachable

bb5:                                              ; preds = %bb4, %bb2
  %8 = load i8, ptr %1, align 1, !range !7, !noundef !3
  %9 = trunc i8 %8 to i1
  ret i1 %9
}

; core::ptr::mut_ptr::<impl *mut T>::is_null
; Function Attrs: inlinehint nonlazybind uwtable
define internal zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17h1f4110d3e6493e0eE"(ptr %self) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %_13 = alloca %"core::ptr::metadata::PtrComponents<u8>", align 8
  %_12 = alloca %"core::ptr::metadata::PtrRepr<u8>", align 8
  %_2 = alloca i8, align 1
  %1 = alloca i8, align 1
  store i64 0, ptr %0, align 8
  %data_address = load ptr, ptr %0, align 8
  store ptr %data_address, ptr %_13, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_12, ptr align 8 %_13, i64 8, i1 false)
  %_5 = load ptr, ptr %_12, align 8
; call core::ptr::mut_ptr::<impl *mut T>::guaranteed_eq
  %2 = call i8 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$13guaranteed_eq17h52a0c5a0a529ee00E"(ptr %self, ptr %_5), !range !12
  store i8 %2, ptr %_2, align 1
  %3 = load i8, ptr %_2, align 1, !range !12, !noundef !3
  %4 = icmp eq i8 %3, 2
  %_6 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_6, 0
  br i1 %5, label %bb4, label %bb2

bb4:                                              ; preds = %start
  store i8 0, ptr %1, align 1
  br label %bb5

bb2:                                              ; preds = %start
  %6 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %res = trunc i8 %6 to i1
  %7 = zext i1 %res to i8
  store i8 %7, ptr %1, align 1
  br label %bb5

bb3:                                              ; No predecessors!
  unreachable

bb5:                                              ; preds = %bb4, %bb2
  %8 = load i8, ptr %1, align 1, !range !7, !noundef !3
  %9 = trunc i8 %8 to i1
  ret i1 %9
}

; core::ptr::mut_ptr::<impl *mut T>::is_null
; Function Attrs: inlinehint nonlazybind uwtable
define internal zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17hc8c85d3458796f4dE"(ptr %self) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %_13 = alloca %"core::ptr::metadata::PtrComponents<u8>", align 8
  %_12 = alloca %"core::ptr::metadata::PtrRepr<u8>", align 8
  %_2 = alloca i8, align 1
  %1 = alloca i8, align 1
  store i64 0, ptr %0, align 8
  %data_address = load ptr, ptr %0, align 8
  store ptr %data_address, ptr %_13, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_12, ptr align 8 %_13, i64 8, i1 false)
  %_5 = load ptr, ptr %_12, align 8
; call core::ptr::mut_ptr::<impl *mut T>::guaranteed_eq
  %2 = call i8 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$13guaranteed_eq17h52a0c5a0a529ee00E"(ptr %self, ptr %_5), !range !12
  store i8 %2, ptr %_2, align 1
  %3 = load i8, ptr %_2, align 1, !range !12, !noundef !3
  %4 = icmp eq i8 %3, 2
  %_6 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_6, 0
  br i1 %5, label %bb4, label %bb2

bb4:                                              ; preds = %start
  store i8 0, ptr %1, align 1
  br label %bb5

bb2:                                              ; preds = %start
  %6 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %res = trunc i8 %6 to i1
  %7 = zext i1 %res to i8
  store i8 %7, ptr %1, align 1
  br label %bb5

bb3:                                              ; No predecessors!
  unreachable

bb5:                                              ; preds = %bb4, %bb2
  %8 = load i8, ptr %1, align 1, !range !7, !noundef !3
  %9 = trunc i8 %8 to i1
  ret i1 %9
}

; core::ptr::drop_in_place<std::rt::lang_start<()>::{{closure}}>
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN4core3ptr85drop_in_place$LT$std..rt..lang_start$LT$$LP$$RP$$GT$..$u7b$$u7b$closure$u7d$$u7d$$GT$17h684eecfe36f26fb0E"(ptr %_1) unnamed_addr #1 {
start:
  ret void
}

; core::ptr::drop_in_place<core::option::Option<alloc::boxed::Box<[u32]>>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr87drop_in_place$LT$core..option..Option$LT$alloc..boxed..Box$LT$$u5b$u32$u5d$$GT$$GT$$GT$17h6c5029321880c967E"(ptr %_1) unnamed_addr #0 {
start:
  %0 = load ptr, ptr %_1, align 8
  %1 = ptrtoint ptr %0 to i64
  %2 = icmp eq i64 %1, 0
  %_2 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_2, 0
  br i1 %3, label %bb1, label %bb2

bb1:                                              ; preds = %bb2, %start
  ret void

bb2:                                              ; preds = %start
; call core::ptr::drop_in_place<alloc::boxed::Box<[u32]>>
  call void @"_ZN4core3ptr59drop_in_place$LT$alloc..boxed..Box$LT$$u5b$u32$u5d$$GT$$GT$17hb8118f4c6aa65dc3E"(ptr %_1)
  br label %bb1
}

; core::ptr::drop_in_place<dyn core::error::Error+core::marker::Send+core::marker::Sync>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr93drop_in_place$LT$dyn$u20$core..error..Error$u2b$core..marker..Send$u2b$core..marker..Sync$GT$17hd99039e05ee8d0f5E"(ptr %_1.0, ptr align 8 %_1.1) unnamed_addr #0 {
start:
  %0 = getelementptr inbounds ptr, ptr %_1.1, i64 0
  %1 = load ptr, ptr %0, align 8, !invariant.load !3, !nonnull !3
  call void %1(ptr %_1.0)
  ret void
}

; core::ptr::drop_in_place<core::option::Option<std::sys::unix::process::process_common::Stdio>>
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN4core3ptr95drop_in_place$LT$core..option..Option$LT$std..sys..unix..process..process_common..Stdio$GT$$GT$17h5ee51c6fc3f8faa0E"(ptr %_1) unnamed_addr #0 {
start:
  %0 = load i32, ptr %_1, align 4, !range !13, !noundef !3
  %1 = icmp eq i32 %0, 4
  %_2 = select i1 %1, i64 0, i64 1
  %2 = icmp eq i64 %_2, 0
  br i1 %2, label %bb1, label %bb2

bb1:                                              ; preds = %bb2, %start
  ret void

bb2:                                              ; preds = %start
; call core::ptr::drop_in_place<std::sys::unix::process::process_common::Stdio>
  call void @"_ZN4core3ptr67drop_in_place$LT$std..sys..unix..process..process_common..Stdio$GT$17h99fc56b2bc90461fE"(ptr %_1)
  br label %bb1
}

; core::hint::unreachable_unchecked
; Function Attrs: inlinehint noreturn nonlazybind uwtable
define internal void @_ZN4core4hint21unreachable_unchecked17hf62cde02ee11bb42E() unnamed_addr #3 {
start:
  unreachable
}

; core::alloc::layout::size_align
; Function Attrs: nonlazybind uwtable
define internal { i64, i64 } @_ZN4core5alloc6layout10size_align17h076a569e907c7318E() unnamed_addr #0 {
start:
  %0 = alloca { i64, i64 }, align 8
  store i64 640, ptr %0, align 8
  %1 = getelementptr inbounds { i64, i64 }, ptr %0, i32 0, i32 1
  store i64 8, ptr %1, align 8
  %2 = getelementptr inbounds { i64, i64 }, ptr %0, i32 0, i32 0
  %3 = load i64, ptr %2, align 8
  %4 = getelementptr inbounds { i64, i64 }, ptr %0, i32 0, i32 1
  %5 = load i64, ptr %4, align 8
  %6 = insertvalue { i64, i64 } undef, i64 %3, 0
  %7 = insertvalue { i64, i64 } %6, i64 %5, 1
  ret { i64, i64 } %7
}

; core::alloc::layout::size_align
; Function Attrs: nonlazybind uwtable
define internal { i64, i64 } @_ZN4core5alloc6layout10size_align17ha6a6fb2e74031ff7E() unnamed_addr #0 {
start:
  %0 = alloca { i64, i64 }, align 8
  store i64 544, ptr %0, align 8
  %1 = getelementptr inbounds { i64, i64 }, ptr %0, i32 0, i32 1
  store i64 8, ptr %1, align 8
  %2 = getelementptr inbounds { i64, i64 }, ptr %0, i32 0, i32 0
  %3 = load i64, ptr %2, align 8
  %4 = getelementptr inbounds { i64, i64 }, ptr %0, i32 0, i32 1
  %5 = load i64, ptr %4, align 8
  %6 = insertvalue { i64, i64 } undef, i64 %3, 0
  %7 = insertvalue { i64, i64 } %6, i64 %5, 1
  ret { i64, i64 } %7
}

; core::alloc::layout::Layout::array::inner
; Function Attrs: inlinehint nonlazybind uwtable
define internal { i64, i64 } @_ZN4core5alloc6layout6Layout5array5inner17h867f7bb670fb4284E(i64 %element_size, i64 %align, i64 %n) unnamed_addr #1 {
start:
  %0 = alloca i64, align 8
  %_28 = alloca i64, align 8
  %_24 = alloca i64, align 8
  %_16 = alloca { i64, i64 }, align 8
  %_4 = alloca i8, align 1
  %1 = alloca { i64, i64 }, align 8
  %2 = icmp eq i64 %element_size, 0
  br i1 %2, label %bb1, label %bb2

bb1:                                              ; preds = %start
  store i8 0, ptr %_4, align 1
  br label %bb3

bb2:                                              ; preds = %start
  store i64 %align, ptr %_24, align 8
  %_25 = load i64, ptr %_24, align 8, !range !14, !noundef !3
  %_26 = icmp uge i64 -9223372036854775808, %_25
  call void @llvm.assume(i1 %_26)
  %_27 = icmp ule i64 1, %_25
  call void @llvm.assume(i1 %_27)
  %_21 = sub i64 %_25, 1
  %_9 = sub i64 9223372036854775807, %_21
  %_12 = icmp eq i64 %element_size, 0
  %3 = call i1 @llvm.expect.i1(i1 %_12, i1 false)
  br i1 %3, label %panic, label %bb4

bb4:                                              ; preds = %bb2
  %_8 = udiv i64 %_9, %element_size
  %_6 = icmp ugt i64 %n, %_8
  %4 = zext i1 %_6 to i8
  store i8 %4, ptr %_4, align 1
  br label %bb3

panic:                                            ; preds = %bb2
; call core::panicking::panic
  call void @_ZN4core9panicking5panic17h545818946343732bE(ptr align 1 @str.1, i64 25, ptr align 8 @alloc139) #11
  unreachable

bb3:                                              ; preds = %bb1, %bb4
  %5 = load i8, ptr %_4, align 1, !range !7, !noundef !3
  %6 = trunc i8 %5 to i1
  br i1 %6, label %bb5, label %bb6

bb6:                                              ; preds = %bb3
  %array_size = mul i64 %element_size, %n
  store i64 %align, ptr %_28, align 8
  %_29 = load i64, ptr %_28, align 8, !range !14, !noundef !3
  %_30 = icmp uge i64 -9223372036854775808, %_29
  call void @llvm.assume(i1 %_30)
  %_31 = icmp ule i64 1, %_29
  call void @llvm.assume(i1 %_31)
  store i64 %_29, ptr %0, align 8
  %_33 = load i64, ptr %0, align 8, !range !14, !noundef !3
  store i64 %array_size, ptr %_16, align 8
  %7 = getelementptr inbounds { i64, i64 }, ptr %_16, i32 0, i32 1
  store i64 %_33, ptr %7, align 8
  %8 = getelementptr inbounds { i64, i64 }, ptr %_16, i32 0, i32 0
  %9 = load i64, ptr %8, align 8
  %10 = getelementptr inbounds { i64, i64 }, ptr %_16, i32 0, i32 1
  %11 = load i64, ptr %10, align 8, !range !14, !noundef !3
  %12 = getelementptr inbounds { i64, i64 }, ptr %1, i32 0, i32 0
  store i64 %9, ptr %12, align 8
  %13 = getelementptr inbounds { i64, i64 }, ptr %1, i32 0, i32 1
  store i64 %11, ptr %13, align 8
  br label %bb7

bb5:                                              ; preds = %bb3
  %14 = getelementptr inbounds { i64, i64 }, ptr %1, i32 0, i32 1
  store i64 0, ptr %14, align 8
  br label %bb7

bb7:                                              ; preds = %bb6, %bb5
  %15 = getelementptr inbounds { i64, i64 }, ptr %1, i32 0, i32 0
  %16 = load i64, ptr %15, align 8
  %17 = getelementptr inbounds { i64, i64 }, ptr %1, i32 0, i32 1
  %18 = load i64, ptr %17, align 8, !range !15, !noundef !3
  %19 = insertvalue { i64, i64 } undef, i64 %16, 0
  %20 = insertvalue { i64, i64 } %19, i64 %18, 1
  ret { i64, i64 } %20
}

; core::option::Option<T>::map
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN4core6option15Option$LT$T$GT$3map17hcf555d35e40c2defE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>") %0, ptr align 8 %1, ptr align 8 %f.0, ptr align 8 %f.1) unnamed_addr #1 {
start:
  %_9 = alloca i8, align 1
  %_7 = alloca ptr, align 8
  %_5 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %self = alloca ptr, align 8
  store ptr %1, ptr %self, align 8
  store i8 1, ptr %_9, align 1
  %2 = load ptr, ptr %self, align 8
  %3 = ptrtoint ptr %2 to i64
  %4 = icmp eq i64 %3, 0
  %_3 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_3, 0
  br i1 %5, label %bb1, label %bb3

bb1:                                              ; preds = %start
  %6 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", ptr %0, i32 0, i32 1
  store ptr null, ptr %6, align 8
  br label %bb7

bb3:                                              ; preds = %start
  %x = load ptr, ptr %self, align 8, !nonnull !3, !align !4, !noundef !3
  store i8 0, ptr %_9, align 1
  store ptr %x, ptr %_7, align 8
  %7 = load ptr, ptr %_7, align 8, !nonnull !3, !align !4, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::ascend::{{closure}}
  call void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$6ascend28_$u7b$$u7b$closure$u7d$$u7d$17hbbb3c0a2214fbee0E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %_5, ptr align 8 %f.0, ptr align 8 %f.1, ptr align 8 %7)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_5, i64 24, i1 false)
  br label %bb7

bb2:                                              ; No predecessors!
  unreachable

bb7:                                              ; preds = %bb1, %bb3
  %8 = load i8, ptr %_9, align 1, !range !7, !noundef !3
  %9 = trunc i8 %8 to i1
  br i1 %9, label %bb6, label %bb5

bb5:                                              ; preds = %bb6, %bb7
  ret void

bb6:                                              ; preds = %bb7
  br label %bb5
}

; core::result::Result<T,E>::unwrap_unchecked
; Function Attrs: inlinehint nonlazybind uwtable
define internal { i64, i64 } @"_ZN4core6result19Result$LT$T$C$E$GT$16unwrap_unchecked17h2b9eb42ba2a15764E"(i64 %0, i64 %1, ptr align 8 %2) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %3 = alloca { ptr, i32 }, align 8
  %self = alloca { i64, i64 }, align 8
  %4 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 0
  store i64 %0, ptr %4, align 8
  %5 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 1
  store i64 %1, ptr %5, align 8
  %6 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 1
  %7 = load i64, ptr %6, align 8, !range !15, !noundef !3
  %8 = icmp eq i64 %7, 0
  %_3 = select i1 %8, i64 1, i64 0
  %9 = icmp eq i64 %_3, 0
  br i1 %9, label %bb3, label %bb1

bb3:                                              ; preds = %start
  %10 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 0
  %t.0 = load i64, ptr %10, align 8
  %11 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 1
  %t.1 = load i64, ptr %11, align 8, !range !14, !noundef !3
  %12 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 1
  %13 = load i64, ptr %12, align 8, !range !15, !noundef !3
  %14 = icmp eq i64 %13, 0
  %_7 = select i1 %14, i64 1, i64 0
  %15 = icmp eq i64 %_7, 0
  br i1 %15, label %bb5, label %bb6

bb1:                                              ; preds = %start
; invoke core::hint::unreachable_unchecked
  invoke void @_ZN4core4hint21unreachable_unchecked17hf62cde02ee11bb42E() #11
          to label %unreachable unwind label %cleanup

bb2:                                              ; No predecessors!
  unreachable

bb10:                                             ; preds = %cleanup
  %16 = getelementptr inbounds { i64, i64 }, ptr %self, i32 0, i32 1
  %17 = load i64, ptr %16, align 8, !range !15, !noundef !3
  %18 = icmp eq i64 %17, 0
  %_8 = select i1 %18, i64 1, i64 0
  %19 = icmp eq i64 %_8, 0
  br i1 %19, label %bb7, label %bb9

cleanup:                                          ; preds = %bb1
  %20 = landingpad { ptr, i32 }
          cleanup
  %21 = extractvalue { ptr, i32 } %20, 0
  %22 = extractvalue { ptr, i32 } %20, 1
  %23 = getelementptr inbounds { ptr, i32 }, ptr %3, i32 0, i32 0
  store ptr %21, ptr %23, align 8
  %24 = getelementptr inbounds { ptr, i32 }, ptr %3, i32 0, i32 1
  store i32 %22, ptr %24, align 8
  br label %bb10

unreachable:                                      ; preds = %bb1
  unreachable

bb7:                                              ; preds = %bb10
  br i1 true, label %bb8, label %bb4

bb9:                                              ; preds = %bb10
  br label %bb4

bb4:                                              ; preds = %bb8, %bb7, %bb9
  %25 = load ptr, ptr %3, align 8
  %26 = getelementptr inbounds { ptr, i32 }, ptr %3, i32 0, i32 1
  %27 = load i32, ptr %26, align 8
  %28 = insertvalue { ptr, i32 } undef, ptr %25, 0
  %29 = insertvalue { ptr, i32 } %28, i32 %27, 1
  resume { ptr, i32 } %29

bb8:                                              ; preds = %bb7
  br label %bb4

bb5:                                              ; preds = %bb6, %bb3
  %30 = insertvalue { i64, i64 } undef, i64 %t.0, 0
  %31 = insertvalue { i64, i64 } %30, i64 %t.1, 1
  ret { i64, i64 } %31

bb6:                                              ; preds = %bb3
  br label %bb5
}

; core::result::Result<T,E>::expect
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN4core6result19Result$LT$T$C$E$GT$6expect17h97ef44f93ad4a754E"(ptr sret(%"std::process::Output") %t, ptr %self, ptr align 1 %msg.0, i64 %msg.1, ptr align 8 %0) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %1 = alloca { ptr, i32 }, align 8
  %e = alloca ptr, align 8
  %2 = getelementptr inbounds %"core::result::Result<std::process::Output, std::io::error::Error>", ptr %self, i32 0, i32 1
  %3 = load ptr, ptr %2, align 8
  %4 = ptrtoint ptr %3 to i64
  %5 = icmp eq i64 %4, 0
  %_3 = select i1 %5, i64 1, i64 0
  %6 = icmp eq i64 %_3, 0
  br i1 %6, label %bb3, label %bb1

bb3:                                              ; preds = %start
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %t, ptr align 8 %self, i64 56, i1 false)
  ret void

bb1:                                              ; preds = %start
  %7 = load ptr, ptr %self, align 8, !nonnull !3, !noundef !3
  store ptr %7, ptr %e, align 8
; invoke core::result::unwrap_failed
  invoke void @_ZN4core6result13unwrap_failed17h62abf61e411aaa08E(ptr align 1 %msg.0, i64 %msg.1, ptr align 1 %e, ptr align 8 @vtable.2, ptr align 8 %0) #11
          to label %unreachable unwind label %cleanup

bb2:                                              ; No predecessors!
  unreachable

bb4:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<std::io::error::Error>
  invoke void @"_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17he5cda979aa03a59eE"(ptr %e) #12
          to label %bb5 unwind label %abort

cleanup:                                          ; preds = %bb1
  %8 = landingpad { ptr, i32 }
          cleanup
  %9 = extractvalue { ptr, i32 } %8, 0
  %10 = extractvalue { ptr, i32 } %8, 1
  %11 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 0
  store ptr %9, ptr %11, align 8
  %12 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  store i32 %10, ptr %12, align 8
  br label %bb4

unreachable:                                      ; preds = %bb1
  unreachable

abort:                                            ; preds = %bb4
  %13 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb5:                                              ; preds = %bb4
  %14 = load ptr, ptr %1, align 8
  %15 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  %16 = load i32, ptr %15, align 8
  %17 = insertvalue { ptr, i32 } undef, ptr %14, 0
  %18 = insertvalue { ptr, i32 } %17, i32 %16, 1
  resume { ptr, i32 } %18
}

; <T as core::convert::Into<U>>::into
; Function Attrs: nonlazybind uwtable
define internal ptr @"_ZN50_$LT$T$u20$as$u20$core..convert..Into$LT$U$GT$$GT$4into17hbd8c27f68e5b0f88E"(ptr %self) unnamed_addr #0 {
start:
; call <core::ptr::non_null::NonNull<T> as core::convert::From<core::ptr::unique::Unique<T>>>::from
  %0 = call ptr @"_ZN119_$LT$core..ptr..non_null..NonNull$LT$T$GT$$u20$as$u20$core..convert..From$LT$core..ptr..unique..Unique$LT$T$GT$$GT$$GT$4from17h40bfc4ab431dc539E"(ptr %self)
  ret ptr %0
}

; <() as std::process::Termination>::report
; Function Attrs: inlinehint nonlazybind uwtable
define internal i8 @"_ZN54_$LT$$LP$$RP$$u20$as$u20$std..process..Termination$GT$6report17hb90ba8952d02e25eE"() unnamed_addr #1 {
start:
  ret i8 0
}

; <&T as core::convert::AsRef<U>>::as_ref
; Function Attrs: inlinehint nonlazybind uwtable
define internal { ptr, i64 } @"_ZN55_$LT$$RF$T$u20$as$u20$core..convert..AsRef$LT$U$GT$$GT$6as_ref17h37fb207455acf265E"(ptr align 8 %self) unnamed_addr #1 {
start:
  %0 = getelementptr inbounds { ptr, i64 }, ptr %self, i32 0, i32 0
  %_3.0 = load ptr, ptr %0, align 8, !nonnull !3, !align !9, !noundef !3
  %1 = getelementptr inbounds { ptr, i64 }, ptr %self, i32 0, i32 1
  %_3.1 = load i64, ptr %1, align 8
; call std::ffi::os_str::<impl core::convert::AsRef<std::ffi::os_str::OsStr> for str>::as_ref
  %2 = call { ptr, i64 } @"_ZN3std3ffi6os_str85_$LT$impl$u20$core..convert..AsRef$LT$std..ffi..os_str..OsStr$GT$$u20$for$u20$str$GT$6as_ref17h6edd23f2e71f6e8aE"(ptr align 1 %_3.0, i64 %_3.1)
  %3 = extractvalue { ptr, i64 } %2, 0
  %4 = extractvalue { ptr, i64 } %2, 1
  %5 = insertvalue { ptr, i64 } undef, ptr %3, 0
  %6 = insertvalue { ptr, i64 } %5, i64 %4, 1
  ret { ptr, i64 } %6
}

; <alloc::alloc::Global as core::clone::Clone>::clone
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN59_$LT$alloc..alloc..Global$u20$as$u20$core..clone..Clone$GT$5clone17hc7b86e212f6c4442E"(ptr align 1 %self) unnamed_addr #1 {
start:
  ret void
}

; alloc::collections::btree::map::IntoIter<K,V,A>::dying_next
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree3map25IntoIter$LT$K$C$V$C$A$GT$10dying_next17h53e185146a41908aE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %_7 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %1 = getelementptr inbounds %"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %self, i32 0, i32 2
  %_2 = load i64, ptr %1, align 8
  %2 = icmp eq i64 %_2, 0
  br i1 %2, label %bb1, label %bb4

bb1:                                              ; preds = %start
; call <alloc::alloc::Global as core::clone::Clone>::clone
  call void @"_ZN59_$LT$alloc..alloc..Global$u20$as$u20$core..clone..Clone$GT$5clone17hc7b86e212f6c4442E"(ptr align 1 %self)
; call alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying,K,V>::deallocating_end
  call void @"_ZN5alloc11collections5btree8navigate75LazyLeafRange$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$GT$16deallocating_end17hec7fe9a69531f670E"(ptr align 8 %self)
  %3 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", ptr %0, i32 0, i32 1
  store ptr null, ptr %3, align 8
  br label %bb7

bb4:                                              ; preds = %start
  %4 = getelementptr inbounds %"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %self, i32 0, i32 2
  %5 = getelementptr inbounds %"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %self, i32 0, i32 2
  %6 = load i64, ptr %5, align 8
  %7 = sub i64 %6, 1
  store i64 %7, ptr %4, align 8
; call <alloc::alloc::Global as core::clone::Clone>::clone
  call void @"_ZN59_$LT$alloc..alloc..Global$u20$as$u20$core..clone..Clone$GT$5clone17hc7b86e212f6c4442E"(ptr align 1 %self)
; call alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying,K,V>::deallocating_next_unchecked
  call void @"_ZN5alloc11collections5btree8navigate75LazyLeafRange$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$GT$27deallocating_next_unchecked17h31e3a6e9582f3278E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %_7, ptr align 8 %self)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_7, i64 24, i1 false)
  br label %bb7

bb7:                                              ; preds = %bb1, %bb4
  ret void
}

; alloc::collections::btree::mem::replace
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN5alloc11collections5btree3mem7replace17h27db3d96b45b3f37E(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %ret, ptr align 8 %v) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %slot = alloca %"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>", align 8
  %self = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>", align 8
  %tmp = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>", align 8
  %_15 = alloca i8, align 1
  %src = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %_10 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %_9 = alloca { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>" }, align 8
  %_7 = alloca { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }, align 8
  %new_value = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %value = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %guard = alloca %"alloc::collections::btree::mem::replace::PanicGuard", align 1
  store i8 1, ptr %_15, align 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %v, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %self, ptr align 8 %tmp, i64 24, i1 false)
  br label %bb7

bb7:                                              ; preds = %start
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %slot, ptr align 8 %self, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %value, ptr align 8 %slot, i64 24, i1 false)
  store i8 0, ptr %_15, align 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_10, ptr align 8 %value, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_9, ptr align 8 %_10, i64 24, i1 false)
; invoke alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_next_unchecked::{{closure}}
  invoke void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$27deallocating_next_unchecked28_$u7b$$u7b$closure$u7d$$u7d$17hfa9fcfd18cbdac44E"(ptr sret({ %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }) %_7, ptr %_9)
          to label %bb1 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  br i1 true, label %bb4, label %bb2

cleanup:                                          ; preds = %bb7
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb5

bb1:                                              ; preds = %bb7
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %new_value, ptr align 8 %_7, i64 24, i1 false)
  %6 = getelementptr inbounds { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }, ptr %_7, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %ret, ptr align 8 %6, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %src, ptr align 8 %new_value, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %v, ptr align 8 %src, i64 24, i1 false)
  ret void

bb2:                                              ; preds = %bb4, %bb5
  %7 = load i8, ptr %_15, align 1, !range !7, !noundef !3
  %8 = trunc i8 %7 to i1
  br i1 %8, label %bb6, label %bb3

bb4:                                              ; preds = %bb5
; invoke core::ptr::drop_in_place<alloc::collections::btree::mem::replace::PanicGuard>
  invoke void @"_ZN4core3ptr72drop_in_place$LT$alloc..collections..btree..mem..replace..PanicGuard$GT$17ha0825560598d6990E"(ptr %guard) #12
          to label %bb2 unwind label %abort

abort:                                            ; preds = %bb4
  %9 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb3:                                              ; preds = %bb6, %bb2
  %10 = load ptr, ptr %0, align 8
  %11 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %12 = load i32, ptr %11, align 8
  %13 = insertvalue { ptr, i32 } undef, ptr %10, 0
  %14 = insertvalue { ptr, i32 } %13, i32 %12, 1
  resume { ptr, i32 } %14

bb6:                                              ; preds = %bb2
  br label %bb3
}

; alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::LeafOrInternal>::deallocate_and_ascend
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node127NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$21deallocate_and_ascend17h48bae09deb1280ebE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>") %ret, i64 %self.0, ptr %self.1) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca i64, align 8
  %1 = alloca i64, align 8
  %2 = alloca { ptr, i32 }, align 8
  %_16 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %x = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_11 = alloca { i64, i64 }, align 8
  %_9 = alloca ptr, align 8
  %self = alloca %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>", align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
; invoke alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::ascend
  invoke void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$6ascend17h5415cfa799f99541E"(ptr sret(%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>") %self, i64 %self.0, ptr %self.1)
          to label %bb1 unwind label %cleanup

bb8:                                              ; preds = %cleanup
  %3 = load ptr, ptr %2, align 8
  %4 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %5 = load i32, ptr %4, align 8
  %6 = insertvalue { ptr, i32 } undef, ptr %3, 0
  %7 = insertvalue { ptr, i32 } %6, i32 %5, 1
  resume { ptr, i32 } %7

cleanup:                                          ; preds = %bb5, %bb4, %bb3, %start
  %8 = landingpad { ptr, i32 }
          cleanup
  %9 = extractvalue { ptr, i32 } %8, 0
  %10 = extractvalue { ptr, i32 } %8, 1
  %11 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %9, ptr %11, align 8
  %12 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %10, ptr %12, align 8
  br label %bb8

bb1:                                              ; preds = %start
  %_14 = load i64, ptr %self, align 8, !range !16, !noundef !3
  %13 = icmp eq i64 %_14, 0
  br i1 %13, label %bb12, label %bb10

bb12:                                             ; preds = %bb1
  %14 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>::Ok", ptr %self, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %x, ptr align 8 %14, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_16, ptr align 8 %x, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %ret, ptr align 8 %_16, i64 24, i1 false)
  br label %bb2

bb10:                                             ; preds = %bb1
  %15 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", ptr %ret, i32 0, i32 1
  store ptr null, ptr %15, align 8
  br label %bb2

bb11:                                             ; No predecessors!
  unreachable

bb2:                                              ; preds = %bb12, %bb10
  store ptr %self.1, ptr %_9, align 8
  %_12 = icmp ugt i64 %self.0, 0
  br i1 %_12, label %bb3, label %bb4

bb4:                                              ; preds = %bb2
; invoke core::alloc::layout::size_align
  %16 = invoke { i64, i64 } @_ZN4core5alloc6layout10size_align17ha6a6fb2e74031ff7E()
          to label %bb15 unwind label %cleanup

bb3:                                              ; preds = %bb2
; invoke core::alloc::layout::size_align
  %17 = invoke { i64, i64 } @_ZN4core5alloc6layout10size_align17h076a569e907c7318E()
          to label %bb13 unwind label %cleanup

bb13:                                             ; preds = %bb3
  %_28.0 = extractvalue { i64, i64 } %17, 0
  %_28.1 = extractvalue { i64, i64 } %17, 1
  store i64 %_28.1, ptr %1, align 8
  %_32 = load i64, ptr %1, align 8, !range !14, !noundef !3
  br label %bb14

bb14:                                             ; preds = %bb13
  store i64 %_28.0, ptr %_11, align 8
  %18 = getelementptr inbounds { i64, i64 }, ptr %_11, i32 0, i32 1
  store i64 %_32, ptr %18, align 8
  br label %bb5

bb5:                                              ; preds = %bb16, %bb14
  %19 = load ptr, ptr %_9, align 8, !nonnull !3, !noundef !3
  %20 = getelementptr inbounds { i64, i64 }, ptr %_11, i32 0, i32 0
  %21 = load i64, ptr %20, align 8
  %22 = getelementptr inbounds { i64, i64 }, ptr %_11, i32 0, i32 1
  %23 = load i64, ptr %22, align 8, !range !14, !noundef !3
; invoke <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  invoke void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %alloc, ptr %19, i64 %21, i64 %23)
          to label %bb6 unwind label %cleanup

bb15:                                             ; preds = %bb4
  %_40.0 = extractvalue { i64, i64 } %16, 0
  %_40.1 = extractvalue { i64, i64 } %16, 1
  store i64 %_40.1, ptr %0, align 8
  %_44 = load i64, ptr %0, align 8, !range !14, !noundef !3
  br label %bb16

bb16:                                             ; preds = %bb15
  store i64 %_40.0, ptr %_11, align 8
  %24 = getelementptr inbounds { i64, i64 }, ptr %_11, i32 0, i32 1
  store i64 %_44, ptr %24, align 8
  br label %bb5

bb6:                                              ; preds = %bb5
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::right_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node137Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$10right_edge17h9ee33dddef761fbaE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %0, ptr %self) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_2.0 = load i64, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_2.1 = load ptr, ptr %2, align 8, !nonnull !3, !noundef !3
  %3 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>", ptr %self, i32 0, i32 2
  %_4 = load i64, ptr %3, align 8
  %_3 = add i64 %_4, 1
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
  call void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17hdfb8ac633b2070cfE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %_2.0, ptr %_2.1, i64 %_3)
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::right_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node137Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$10right_edge17hc0e7fead19cf7107E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, ptr %self) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_2.0 = load i64, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_2.1 = load ptr, ptr %2, align 8, !nonnull !3, !noundef !3
  %3 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>", ptr %self, i32 0, i32 2
  %_4 = load i64, ptr %3, align 8
  %_3 = add i64 %_4, 1
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
  call void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17hf21d201b6756c90bE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %_2.0, ptr %_2.1, i64 %_3)
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::new_kv
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node137Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$6new_kv17h5003f1e525993e8dE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %0, i64 %node.0, ptr %node.1, i64 %idx) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  store i64 %node.0, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %node.1, ptr %2, align 8
  %3 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", ptr %0, i32 0, i32 2
  store i64 %idx, ptr %3, align 8
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17h326cb12529b8304cE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %node.0, ptr %node.1, i64 %idx) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  store i64 %node.0, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %node.1, ptr %2, align 8
  %3 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", ptr %0, i32 0, i32 2
  store i64 %idx, ptr %3, align 8
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17hdfb8ac633b2070cfE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %node.0, ptr %node.1, i64 %idx) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  store i64 %node.0, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %node.1, ptr %2, align 8
  %3 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", ptr %0, i32 0, i32 2
  store i64 %idx, ptr %3, align 8
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17hf21d201b6756c90bE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %node.0, ptr %node.1, i64 %idx) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  store i64 %node.0, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %node.1, ptr %2, align 8
  %3 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", ptr %0, i32 0, i32 2
  store i64 %idx, ptr %3, align 8
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::right_kv
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8right_kv17h0321542d8560f8f4E"(ptr sret(%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>") %0, ptr %self) unnamed_addr #0 {
start:
  %_9 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_6 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %1 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", ptr %self, i32 0, i32 2
  %_3 = load i64, ptr %1, align 8
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::len
  %_4 = call i64 @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$3len17h268424539c6b2304E"(ptr align 8 %self)
  %_2 = icmp ult i64 %_3, %_4
  br i1 %_2, label %bb2, label %bb4

bb4:                                              ; preds = %start
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_9, ptr align 8 %self, i64 24, i1 false)
  %2 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>::Err", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %2, ptr align 8 %_9, i64 24, i1 false)
  store i64 1, ptr %0, align 8
  br label %bb5

bb2:                                              ; preds = %start
  %3 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_7.0 = load i64, ptr %3, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_7.1 = load ptr, ptr %4, align 8, !nonnull !3, !noundef !3
  %5 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", ptr %self, i32 0, i32 2
  %_8 = load i64, ptr %5, align 8
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::new_kv
  call void @"_ZN5alloc11collections5btree4node137Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$6new_kv17h5003f1e525993e8dE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %_6, i64 %_7.0, ptr %_7.1, i64 %_8)
  %6 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>::Ok", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %6, ptr align 8 %_6, i64 24, i1 false)
  store i64 0, ptr %0, align 8
  br label %bb5

bb5:                                              ; preds = %bb4, %bb2
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>,Type>::force
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node145Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$C$Type$GT$5force17hb737b2b06dcdc4bfE"(ptr sret(%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>") %0, ptr %self) unnamed_addr #0 {
start:
  %_10 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>", align 8
  %_6 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>", align 8
  %_2 = alloca %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>", align 8
  %1 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_3.0 = load i64, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_3.1 = load ptr, ptr %2, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>::force
  call void @"_ZN5alloc11collections5btree4node91NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$5force17h9a4b0e4d793399a5E"(ptr sret(%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>") %_2, i64 %_3.0, ptr %_3.1)
  %_4 = load i64, ptr %_2, align 8, !range !16, !noundef !3
  %3 = icmp eq i64 %_4, 0
  br i1 %3, label %bb4, label %bb2

bb4:                                              ; preds = %start
  %4 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Leaf", ptr %_2, i32 0, i32 1
  %5 = getelementptr inbounds { i64, ptr }, ptr %4, i32 0, i32 0
  %node.01 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { i64, ptr }, ptr %4, i32 0, i32 1
  %node.12 = load ptr, ptr %6, align 8, !nonnull !3, !noundef !3
  %7 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", ptr %self, i32 0, i32 2
  %_8 = load i64, ptr %7, align 8
  %8 = getelementptr inbounds { i64, ptr }, ptr %_6, i32 0, i32 0
  store i64 %node.01, ptr %8, align 8
  %9 = getelementptr inbounds { i64, ptr }, ptr %_6, i32 0, i32 1
  store ptr %node.12, ptr %9, align 8
  %10 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>", ptr %_6, i32 0, i32 2
  store i64 %_8, ptr %10, align 8
  %11 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>::Leaf", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %11, ptr align 8 %_6, i64 24, i1 false)
  store i64 0, ptr %0, align 8
  br label %bb5

bb2:                                              ; preds = %start
  %12 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Internal", ptr %_2, i32 0, i32 1
  %13 = getelementptr inbounds { i64, ptr }, ptr %12, i32 0, i32 0
  %node.0 = load i64, ptr %13, align 8
  %14 = getelementptr inbounds { i64, ptr }, ptr %12, i32 0, i32 1
  %node.1 = load ptr, ptr %14, align 8, !nonnull !3, !noundef !3
  %15 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", ptr %self, i32 0, i32 2
  %_12 = load i64, ptr %15, align 8
  %16 = getelementptr inbounds { i64, ptr }, ptr %_10, i32 0, i32 0
  store i64 %node.0, ptr %16, align 8
  %17 = getelementptr inbounds { i64, ptr }, ptr %_10, i32 0, i32 1
  store ptr %node.1, ptr %17, align 8
  %18 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>", ptr %_10, i32 0, i32 2
  store i64 %_12, ptr %18, align 8
  %19 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>::Internal", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %19, ptr align 8 %_10, i64 24, i1 false)
  store i64 1, ptr %0, align 8
  br label %bb5

bb3:                                              ; No predecessors!
  unreachable

bb5:                                              ; preds = %bb4, %bb2
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::drop_key_val
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node173Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$12drop_key_val17h20aa62e5ca1dbd04E"(ptr %self) unnamed_addr #1 {
start:
; call alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,Type>::as_leaf_dying
  %leaf = call align 8 ptr @"_ZN5alloc11collections5btree4node76NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$Type$GT$13as_leaf_dying17ha120f77962b77b86E"(ptr align 8 %self)
  %_8 = getelementptr inbounds %"alloc::collections::btree::node::LeafNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %leaf, i32 0, i32 1
  %0 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", ptr %self, i32 0, i32 2
  %index = load i64, ptr %0, align 8
; call <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked_mut
  %_19 = call ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$17get_unchecked_mut17h8fede40c3f413513E"(i64 %index, ptr %_8, i64 11)
; call core::mem::maybe_uninit::MaybeUninit<T>::assume_init_drop
  call void @"_ZN4core3mem12maybe_uninit20MaybeUninit$LT$T$GT$16assume_init_drop17hde1e8179dd2c8220E"(ptr align 8 %_19)
  %_14 = getelementptr inbounds %"alloc::collections::btree::node::LeafNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %leaf, i32 0, i32 2
  %1 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", ptr %self, i32 0, i32 2
  %index1 = load i64, ptr %1, align 8
; call <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked_mut
  %_25 = call ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$17get_unchecked_mut17hfaa45761f33dde87E"(i64 %index1, ptr %_14, i64 11)
; call core::mem::maybe_uninit::MaybeUninit<T>::assume_init_drop
  call void @"_ZN4core3mem12maybe_uninit20MaybeUninit$LT$T$GT$16assume_init_drop17h1c4eaa00bb087e26E"(ptr align 8 %_25)
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>::forget_node_type
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node176Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$16forget_node_type17hb5ae9013da0ef86aE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %0, ptr %self) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_3.0 = load i64, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_3.1 = load ptr, ptr %2, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Leaf>::forget_type
  %3 = call { i64, ptr } @"_ZN5alloc11collections5btree4node81NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$11forget_type17h3c21090ce367f209E"(i64 %_3.0, ptr %_3.1)
  %_2.0 = extractvalue { i64, ptr } %3, 0
  %_2.1 = extractvalue { i64, ptr } %3, 1
  %4 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", ptr %self, i32 0, i32 2
  %_4 = load i64, ptr %4, align 8
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
  call void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17h326cb12529b8304cE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %_2.0, ptr %_2.1, i64 %_4)
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>,alloc::collections::btree::node::marker::Edge>::forget_node_type
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node180Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$$C$alloc..collections..btree..node..marker..Edge$GT$16forget_node_type17hc4f3ad6111d770f2E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %0, ptr %self) unnamed_addr #0 {
start:
  %1 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_3.0 = load i64, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_3.1 = load ptr, ptr %2, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>::forget_type
  %3 = call { i64, ptr } @"_ZN5alloc11collections5btree4node85NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$11forget_type17ha660d0efea020f14E"(i64 %_3.0, ptr %_3.1)
  %_2.0 = extractvalue { i64, ptr } %3, 0
  %_2.1 = extractvalue { i64, ptr } %3, 1
  %4 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", ptr %self, i32 0, i32 2
  %_4 = load i64, ptr %4, align 8
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
  call void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17h326cb12529b8304cE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %_2.0, ptr %_2.1, i64 %_4)
  ret void
}

; alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>,alloc::collections::btree::node::marker::Edge>::descend
; Function Attrs: nonlazybind uwtable
define internal { i64, ptr } @"_ZN5alloc11collections5btree4node180Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$$C$alloc..collections..btree..node..marker..Edge$GT$7descend17hac3c1826c4ff045dE"(ptr %self) unnamed_addr #0 {
start:
  %tmp = alloca ptr, align 8
  %0 = alloca { i64, ptr }, align 8
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>::as_internal_ptr
  %parent_ptr = call ptr @"_ZN5alloc11collections5btree4node85NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$15as_internal_ptr17h1b5d3b577d4baac6E"(ptr align 8 %self)
  %_9 = getelementptr inbounds %"alloc::collections::btree::node::InternalNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %parent_ptr, i32 0, i32 1
  %1 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", ptr %self, i32 0, i32 2
  %index = load i64, ptr %1, align 8
; call <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked
  %_14 = call ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$13get_unchecked17hded7e697effab513E"(i64 %index, ptr %_9, i64 12)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %_14, i64 8, i1 false)
  %self1 = load ptr, ptr %tmp, align 8
  %_13 = load i64, ptr %self, align 8
  %_12 = sub i64 %_13, 1
  store i64 %_12, ptr %0, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %self1, ptr %2, align 8
  %3 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  %4 = load i64, ptr %3, align 8
  %5 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  %6 = load ptr, ptr %5, align 8, !nonnull !3, !noundef !3
  %7 = insertvalue { i64, ptr } undef, i64 %4, 0
  %8 = insertvalue { i64, ptr } %7, ptr %6, 1
  ret { i64, ptr } %8
}

; alloc::collections::btree::node::Handle<Node,Type>::into_node
; Function Attrs: nonlazybind uwtable
define internal { i64, ptr } @"_ZN5alloc11collections5btree4node25Handle$LT$Node$C$Type$GT$9into_node17h5f3e44ec526c8f25E"(ptr %self) unnamed_addr #0 {
start:
  %0 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %1 = load i64, ptr %0, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %3 = load ptr, ptr %2, align 8, !nonnull !3, !noundef !3
  %4 = insertvalue { i64, ptr } undef, i64 %1, 0
  %5 = insertvalue { i64, ptr } %4, ptr %3, 1
  ret { i64, ptr } %5
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::first_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$10first_edge17h3066ac9c79cdf975E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %self.0, ptr %self.1) unnamed_addr #0 {
start:
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
  call void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17hdfb8ac633b2070cfE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %0, i64 %self.0, ptr %self.1, i64 0)
  ret void
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::first_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$10first_edge17hf9a853191f961f10E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %self.0, ptr %self.1) unnamed_addr #0 {
start:
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::new_edge
  call void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8new_edge17hf21d201b6756c90bE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %self.0, ptr %self.1, i64 0)
  ret void
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::as_leaf_ptr
; Function Attrs: nonlazybind uwtable
define internal ptr @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$11as_leaf_ptr17hddafa28746884847E"(ptr align 8 %this) unnamed_addr #0 {
start:
  %0 = getelementptr inbounds { i64, ptr }, ptr %this, i32 0, i32 1
  %self = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
  ret ptr %self
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::len
; Function Attrs: nonlazybind uwtable
define internal i64 @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$3len17h268424539c6b2304E"(ptr align 8 %self) unnamed_addr #0 {
start:
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::as_leaf_ptr
  %_3 = call ptr @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$11as_leaf_ptr17hddafa28746884847E"(ptr align 8 %self)
  %0 = getelementptr inbounds %"alloc::collections::btree::node::LeafNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %_3, i32 0, i32 4
  %small = load i16, ptr %0, align 2
  %1 = zext i16 %small to i64
  ret i64 %1
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::ascend
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$6ascend17h5415cfa799f99541E"(ptr sret(%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>") %0, i64 %1, ptr %2) unnamed_addr #0 {
start:
  %_19 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %v = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_10 = alloca { ptr, ptr }, align 8
  %_9 = alloca ptr, align 8
  %_7 = alloca ptr, align 8
  %self1 = alloca %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", align 8
  %leaf_ptr = alloca ptr, align 8
  %self = alloca { i64, ptr }, align 8
  %3 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  store i64 %1, ptr %3, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  store ptr %2, ptr %4, align 8
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::as_leaf_ptr
  %_3 = call ptr @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$11as_leaf_ptr17hddafa28746884847E"(ptr align 8 %self)
  store ptr %_3, ptr %leaf_ptr, align 8
  %5 = load ptr, ptr %leaf_ptr, align 8
  %6 = load ptr, ptr %5, align 8
  store ptr %6, ptr %_9, align 8
  %7 = load ptr, ptr %_9, align 8
  %8 = ptrtoint ptr %7 to i64
  %9 = icmp eq i64 %8, 0
  %_14 = select i1 %9, i64 0, i64 1
  %10 = icmp eq i64 %_14, 0
  br i1 %10, label %bb4, label %bb6

bb4:                                              ; preds = %start
  store ptr null, ptr %_7, align 8
  br label %bb2

bb6:                                              ; preds = %start
  store ptr %_9, ptr %_7, align 8
  br label %bb2

bb5:                                              ; No predecessors!
  unreachable

bb2:                                              ; preds = %bb4, %bb6
  store ptr %self, ptr %_10, align 8
  %11 = getelementptr inbounds { ptr, ptr }, ptr %_10, i32 0, i32 1
  store ptr %leaf_ptr, ptr %11, align 8
  %12 = load ptr, ptr %_7, align 8, !align !4
  %13 = getelementptr inbounds { ptr, ptr }, ptr %_10, i32 0, i32 0
  %14 = load ptr, ptr %13, align 8, !nonnull !3, !align !4, !noundef !3
  %15 = getelementptr inbounds { ptr, ptr }, ptr %_10, i32 0, i32 1
  %16 = load ptr, ptr %15, align 8, !nonnull !3, !align !4, !noundef !3
; call core::option::Option<T>::map
  call void @"_ZN4core6option15Option$LT$T$GT$3map17hcf555d35e40c2defE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>") %self1, ptr align 8 %12, ptr align 8 %14, ptr align 8 %16)
  %17 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %err.0 = load i64, ptr %17, align 8
  %18 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %err.1 = load ptr, ptr %18, align 8, !nonnull !3, !noundef !3
  %19 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", ptr %self1, i32 0, i32 1
  %20 = load ptr, ptr %19, align 8
  %21 = ptrtoint ptr %20 to i64
  %22 = icmp eq i64 %21, 0
  %_17 = select i1 %22, i64 0, i64 1
  %23 = icmp eq i64 %_17, 0
  br i1 %23, label %bb7, label %bb9

bb7:                                              ; preds = %bb2
  %24 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>::Err", ptr %0, i32 0, i32 1
  %25 = getelementptr inbounds { i64, ptr }, ptr %24, i32 0, i32 0
  store i64 %err.0, ptr %25, align 8
  %26 = getelementptr inbounds { i64, ptr }, ptr %24, i32 0, i32 1
  store ptr %err.1, ptr %26, align 8
  store i64 1, ptr %0, align 8
  br label %bb10

bb9:                                              ; preds = %bb2
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %v, ptr align 8 %self1, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_19, ptr align 8 %v, i64 24, i1 false)
  %27 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>>::Ok", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %27, ptr align 8 %_19, i64 24, i1 false)
  store i64 0, ptr %0, align 8
  br label %bb10

bb8:                                              ; No predecessors!
  unreachable

bb10:                                             ; preds = %bb7, %bb9
  ret void
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::ascend::{{closure}}
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$6ascend28_$u7b$$u7b$closure$u7d$$u7d$17hbbb3c0a2214fbee0E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %0, ptr align 8 %1, ptr align 8 %2, ptr align 8 %parent) unnamed_addr #1 {
start:
  %_1 = alloca { ptr, ptr }, align 8
  %3 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 0
  store ptr %1, ptr %3, align 8
  %4 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  store ptr %2, ptr %4, align 8
  %_4 = load ptr, ptr %parent, align 8, !nonnull !3, !noundef !3
  %_10 = load ptr, ptr %_1, align 8, !nonnull !3, !align !4, !noundef !3
  %_6 = load i64, ptr %_10, align 8
  %_5 = add i64 %_6, 1
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>::from_internal
  %5 = call { i64, ptr } @"_ZN5alloc11collections5btree4node85NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$13from_internal17hefa97bceefd23954E"(ptr %_4, i64 %_5)
  %_3.0 = extractvalue { i64, ptr } %5, 0
  %_3.1 = extractvalue { i64, ptr } %5, 1
  %6 = getelementptr inbounds { ptr, ptr }, ptr %_1, i32 0, i32 1
  %_11 = load ptr, ptr %6, align 8, !nonnull !3, !align !4, !noundef !3
  %_12 = load ptr, ptr %_11, align 8
  %7 = getelementptr inbounds %"alloc::collections::btree::node::LeafNode<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %_12, i32 0, i32 3
  %self = load i16, ptr %7, align 8
  %_7 = zext i16 %self to i64
  %8 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  store i64 %_3.0, ptr %8, align 8
  %9 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %_3.1, ptr %9, align 8
  %10 = getelementptr inbounds %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", ptr %0, i32 0, i32 2
  store i64 %_7, ptr %10, align 8
  ret void
}

; alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,Type>::as_leaf_dying
; Function Attrs: nonlazybind uwtable
define internal align 8 ptr @"_ZN5alloc11collections5btree4node76NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$Type$GT$13as_leaf_dying17ha120f77962b77b86E"(ptr align 8 %self) unnamed_addr #0 {
start:
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::as_leaf_ptr
  %ptr = call ptr @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$11as_leaf_ptr17hddafa28746884847E"(ptr align 8 %self)
  ret ptr %ptr
}

; alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Owned,K,V,Type>::into_dying
; Function Attrs: nonlazybind uwtable
define internal { i64, ptr } @"_ZN5alloc11collections5btree4node76NodeRef$LT$alloc..collections..btree..node..marker..Owned$C$K$C$V$C$Type$GT$10into_dying17h77214fe9655b199fE"(i64 %self.0, ptr %self.1) unnamed_addr #0 {
start:
  %0 = alloca { i64, ptr }, align 8
  store i64 %self.0, ptr %0, align 8
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %self.1, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  %3 = load i64, ptr %2, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  %5 = load ptr, ptr %4, align 8, !nonnull !3, !noundef !3
  %6 = insertvalue { i64, ptr } undef, i64 %3, 0
  %7 = insertvalue { i64, ptr } %6, ptr %5, 1
  ret { i64, ptr } %7
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Leaf>::forget_type
; Function Attrs: nonlazybind uwtable
define internal { i64, ptr } @"_ZN5alloc11collections5btree4node81NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$11forget_type17h3c21090ce367f209E"(i64 %self.0, ptr %self.1) unnamed_addr #0 {
start:
  %0 = alloca { i64, ptr }, align 8
  store i64 %self.0, ptr %0, align 8
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %self.1, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  %3 = load i64, ptr %2, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  %5 = load ptr, ptr %4, align 8, !nonnull !3, !noundef !3
  %6 = insertvalue { i64, ptr } undef, i64 %3, 0
  %7 = insertvalue { i64, ptr } %6, ptr %5, 1
  ret { i64, ptr } %7
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>::forget_type
; Function Attrs: nonlazybind uwtable
define internal { i64, ptr } @"_ZN5alloc11collections5btree4node85NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$11forget_type17ha660d0efea020f14E"(i64 %self.0, ptr %self.1) unnamed_addr #0 {
start:
  %0 = alloca { i64, ptr }, align 8
  store i64 %self.0, ptr %0, align 8
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  store ptr %self.1, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  %3 = load i64, ptr %2, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  %5 = load ptr, ptr %4, align 8, !nonnull !3, !noundef !3
  %6 = insertvalue { i64, ptr } undef, i64 %3, 0
  %7 = insertvalue { i64, ptr } %6, ptr %5, 1
  ret { i64, ptr } %7
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>::from_internal
; Function Attrs: nonlazybind uwtable
define internal { i64, ptr } @"_ZN5alloc11collections5btree4node85NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$13from_internal17hefa97bceefd23954E"(ptr %node, i64 %height) unnamed_addr #0 {
start:
  %_4 = alloca ptr, align 8
  %0 = alloca { i64, ptr }, align 8
  store ptr %node, ptr %_4, align 8
  store i64 %height, ptr %0, align 8
  %1 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  %2 = load ptr, ptr %_4, align 8, !nonnull !3, !noundef !3
  store ptr %2, ptr %1, align 8
  %3 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 0
  %4 = load i64, ptr %3, align 8
  %5 = getelementptr inbounds { i64, ptr }, ptr %0, i32 0, i32 1
  %6 = load ptr, ptr %5, align 8, !nonnull !3, !noundef !3
  %7 = insertvalue { i64, ptr } undef, i64 %4, 0
  %8 = insertvalue { i64, ptr } %7, ptr %6, 1
  ret { i64, ptr } %8
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>::as_internal_ptr
; Function Attrs: nonlazybind uwtable
define internal ptr @"_ZN5alloc11collections5btree4node85NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$15as_internal_ptr17h1b5d3b577d4baac6E"(ptr align 8 %this) unnamed_addr #0 {
start:
  %0 = getelementptr inbounds { i64, ptr }, ptr %this, i32 0, i32 1
  %self = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
  ret ptr %self
}

; alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>::force
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree4node91NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$5force17h9a4b0e4d793399a5E"(ptr sret(%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>") %0, i64 %self.0, ptr %self.1) unnamed_addr #0 {
start:
  %_6 = alloca { i64, ptr }, align 8
  %_3 = alloca { i64, ptr }, align 8
  %1 = icmp eq i64 %self.0, 0
  br i1 %1, label %bb1, label %bb2

bb1:                                              ; preds = %start
  store i64 %self.0, ptr %_3, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %_3, i32 0, i32 1
  store ptr %self.1, ptr %2, align 8
  %3 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Leaf", ptr %0, i32 0, i32 1
  %4 = getelementptr inbounds { i64, ptr }, ptr %_3, i32 0, i32 0
  %5 = load i64, ptr %4, align 8
  %6 = getelementptr inbounds { i64, ptr }, ptr %_3, i32 0, i32 1
  %7 = load ptr, ptr %6, align 8, !nonnull !3, !noundef !3
  %8 = getelementptr inbounds { i64, ptr }, ptr %3, i32 0, i32 0
  store i64 %5, ptr %8, align 8
  %9 = getelementptr inbounds { i64, ptr }, ptr %3, i32 0, i32 1
  store ptr %7, ptr %9, align 8
  store i64 0, ptr %0, align 8
  br label %bb3

bb2:                                              ; preds = %start
  store i64 %self.0, ptr %_6, align 8
  %10 = getelementptr inbounds { i64, ptr }, ptr %_6, i32 0, i32 1
  store ptr %self.1, ptr %10, align 8
  %11 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Internal", ptr %0, i32 0, i32 1
  %12 = getelementptr inbounds { i64, ptr }, ptr %_6, i32 0, i32 0
  %13 = load i64, ptr %12, align 8
  %14 = getelementptr inbounds { i64, ptr }, ptr %_6, i32 0, i32 1
  %15 = load ptr, ptr %14, align 8, !nonnull !3, !noundef !3
  %16 = getelementptr inbounds { i64, ptr }, ptr %11, i32 0, i32 0
  store i64 %13, ptr %16, align 8
  %17 = getelementptr inbounds { i64, ptr }, ptr %11, i32 0, i32 1
  store ptr %15, ptr %17, align 8
  store i64 1, ptr %0, align 8
  br label %bb3

bb3:                                              ; preds = %bb1, %bb2
  ret void
}

; alloc::collections::btree::navigate::full_range
; Function Attrs: nonlazybind uwtable
define internal void @_ZN5alloc11collections5btree8navigate10full_range17hd56aac8392639781E(ptr sret(%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %0, i64 %root1.0, ptr %root1.1, i64 %root2.0, ptr %root2.1) unnamed_addr #0 {
start:
  %_7 = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_6 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %_4 = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_3 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %1 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Root", ptr %_4, i32 0, i32 1
  %2 = getelementptr inbounds { i64, ptr }, ptr %1, i32 0, i32 0
  store i64 %root1.0, ptr %2, align 8
  %3 = getelementptr inbounds { i64, ptr }, ptr %1, i32 0, i32 1
  store ptr %root1.1, ptr %3, align 8
  store i64 0, ptr %_4, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_3, ptr align 8 %_4, i64 32, i1 false)
  %4 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Root", ptr %_7, i32 0, i32 1
  %5 = getelementptr inbounds { i64, ptr }, ptr %4, i32 0, i32 0
  store i64 %root2.0, ptr %5, align 8
  %6 = getelementptr inbounds { i64, ptr }, ptr %4, i32 0, i32 1
  store ptr %root2.1, ptr %6, align 8
  store i64 0, ptr %_7, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_6, ptr align 8 %_7, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_3, i64 32, i1 false)
  %7 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %7, ptr align 8 %_6, i64 32, i1 false)
  ret void
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>>::first_leaf_edge
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate142_$LT$impl$u20$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$GT$15first_leaf_edge17h8db01edec0751c55E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %self.0, ptr %self.1) unnamed_addr #1 {
start:
  %_10 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_3 = alloca %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>", align 8
  %node = alloca { i64, ptr }, align 8
  %1 = getelementptr inbounds { i64, ptr }, ptr %node, i32 0, i32 0
  store i64 %self.0, ptr %1, align 8
  %2 = getelementptr inbounds { i64, ptr }, ptr %node, i32 0, i32 1
  store ptr %self.1, ptr %2, align 8
  br label %bb1

bb1:                                              ; preds = %bb3, %start
  %3 = getelementptr inbounds { i64, ptr }, ptr %node, i32 0, i32 0
  %_4.0 = load i64, ptr %3, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %node, i32 0, i32 1
  %_4.1 = load ptr, ptr %4, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>::force
  call void @"_ZN5alloc11collections5btree4node91NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$5force17h9a4b0e4d793399a5E"(ptr sret(%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>") %_3, i64 %_4.0, ptr %_4.1)
  %_5 = load i64, ptr %_3, align 8, !range !16, !noundef !3
  %5 = icmp eq i64 %_5, 0
  br i1 %5, label %bb5, label %bb3

bb5:                                              ; preds = %bb1
  %6 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Leaf", ptr %_3, i32 0, i32 1
  %7 = getelementptr inbounds { i64, ptr }, ptr %6, i32 0, i32 0
  %leaf.0 = load i64, ptr %7, align 8
  %8 = getelementptr inbounds { i64, ptr }, ptr %6, i32 0, i32 1
  %leaf.1 = load ptr, ptr %8, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::first_edge
  call void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$10first_edge17hf9a853191f961f10E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %leaf.0, ptr %leaf.1)
  ret void

bb3:                                              ; preds = %bb1
  %9 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>>::Internal", ptr %_3, i32 0, i32 1
  %10 = getelementptr inbounds { i64, ptr }, ptr %9, i32 0, i32 0
  %internal.0 = load i64, ptr %10, align 8
  %11 = getelementptr inbounds { i64, ptr }, ptr %9, i32 0, i32 1
  %internal.1 = load ptr, ptr %11, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::node::NodeRef<BorrowType,K,V,Type>::first_edge
  call void @"_ZN5alloc11collections5btree4node40NodeRef$LT$BorrowType$C$K$C$V$C$Type$GT$10first_edge17h3066ac9c79cdf975E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %_10, i64 %internal.0, ptr %internal.1)
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>,alloc::collections::btree::node::marker::Edge>::descend
  %12 = call { i64, ptr } @"_ZN5alloc11collections5btree4node180Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$$C$alloc..collections..btree..node..marker..Edge$GT$7descend17hac3c1826c4ff045dE"(ptr %_10)
  %_9.0 = extractvalue { i64, ptr } %12, 0
  %_9.1 = extractvalue { i64, ptr } %12, 1
  %13 = getelementptr inbounds { i64, ptr }, ptr %node, i32 0, i32 0
  store i64 %_9.0, ptr %13, align 8
  %14 = getelementptr inbounds { i64, ptr }, ptr %node, i32 0, i32 1
  store ptr %_9.1, ptr %14, align 8
  br label %bb1

bb4:                                              ; No predecessors!
  unreachable
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::LeafOrInternal>>::full_range
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate178_$LT$impl$u20$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$GT$10full_range17h0cee78f197f2fc4cE"(ptr sret(%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %0, i64 %1, ptr %2) unnamed_addr #0 {
start:
  %tmp = alloca { i64, ptr }, align 8
  %self = alloca { i64, ptr }, align 8
  %3 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  store i64 %1, ptr %3, align 8
  %4 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  store ptr %2, ptr %4, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %self, i64 16, i1 false)
  %5 = getelementptr inbounds { i64, ptr }, ptr %tmp, i32 0, i32 0
  %self.0 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { i64, ptr }, ptr %tmp, i32 0, i32 1
  %self.1 = load ptr, ptr %6, align 8
  %7 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 0
  %_5.0 = load i64, ptr %7, align 8
  %8 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %_5.1 = load ptr, ptr %8, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::navigate::full_range
  call void @_ZN5alloc11collections5btree8navigate10full_range17hd56aac8392639781E(ptr sret(%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %0, i64 %_5.0, ptr %_5.1, i64 %self.0, ptr %self.1)
  ret void
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>,alloc::collections::btree::node::marker::KV>>::next_leaf_edge
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate235_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$C$alloc..collections..btree..node..marker..KV$GT$$GT$14next_leaf_edge17hd342fc45b3b9ddd0E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, ptr %self) unnamed_addr #0 {
start:
  %_11 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_9 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>", align 8
  %next_internal_edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %internal_kv = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>", align 8
  %_6 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>", align 8
  %leaf_kv = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>", align 8
  %_3 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %_2 = alloca %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>", align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_3, ptr align 8 %self, i64 24, i1 false)
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>,Type>::force
  call void @"_ZN5alloc11collections5btree4node145Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$C$Type$GT$5force17hb737b2b06dcdc4bfE"(ptr sret(%"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>") %_2, ptr %_3)
  %_4 = load i64, ptr %_2, align 8, !range !16, !noundef !3
  %1 = icmp eq i64 %_4, 0
  br i1 %1, label %bb4, label %bb2

bb4:                                              ; preds = %start
  %2 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>::Leaf", ptr %_2, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %leaf_kv, ptr align 8 %2, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_6, ptr align 8 %leaf_kv, i64 24, i1 false)
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::right_edge
  call void @"_ZN5alloc11collections5btree4node137Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$10right_edge17hc0e7fead19cf7107E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, ptr %_6)
  br label %bb9

bb2:                                              ; preds = %start
  %3 = getelementptr inbounds %"alloc::collections::btree::node::ForceResult<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::KV>>::Internal", ptr %_2, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %internal_kv, ptr align 8 %3, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_9, ptr align 8 %internal_kv, i64 24, i1 false)
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::right_edge
  call void @"_ZN5alloc11collections5btree4node137Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$10right_edge17h9ee33dddef761fbaE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>") %next_internal_edge, ptr %_9)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_11, ptr align 8 %next_internal_edge, i64 24, i1 false)
; call alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>,alloc::collections::btree::node::marker::Edge>::descend
  %4 = call { i64, ptr } @"_ZN5alloc11collections5btree4node180Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$$C$alloc..collections..btree..node..marker..Edge$GT$7descend17hac3c1826c4ff045dE"(ptr %_11)
  %_10.0 = extractvalue { i64, ptr } %4, 0
  %_10.1 = extractvalue { i64, ptr } %4, 1
; call alloc::collections::btree::navigate::<impl alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>>::first_leaf_edge
  call void @"_ZN5alloc11collections5btree8navigate142_$LT$impl$u20$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$GT$15first_leaf_edge17h8db01edec0751c55E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %0, i64 %_10.0, ptr %_10.1)
  br label %bb9

bb3:                                              ; No predecessors!
  unreachable

bb9:                                              ; preds = %bb4, %bb2
  ret void
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_end
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$16deallocating_end17h36556c86a06bf3fbE"(ptr %self) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_13 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_12 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %parent_edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_7 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_5 = alloca %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", align 8
  %_4 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_4, ptr align 8 %self, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>::forget_node_type
  invoke void @"_ZN5alloc11collections5btree4node176Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$16forget_node_type17hb5ae9013da0ef86aE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %edge, ptr %_4)
          to label %bb1 unwind label %cleanup

bb10:                                             ; preds = %cleanup
  %1 = load ptr, ptr %0, align 8
  %2 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %3 = load i32, ptr %2, align 8
  %4 = insertvalue { ptr, i32 } undef, ptr %1, 0
  %5 = insertvalue { ptr, i32 } %4, i32 %3, 1
  resume { ptr, i32 } %5

cleanup:                                          ; preds = %bb6, %bb4, %bb3, %bb2, %start
  %6 = landingpad { ptr, i32 }
          cleanup
  %7 = extractvalue { ptr, i32 } %6, 0
  %8 = extractvalue { ptr, i32 } %6, 1
  %9 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %7, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %8, ptr %10, align 8
  br label %bb10

bb1:                                              ; preds = %start
  br label %bb2

bb2:                                              ; preds = %bb7, %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_7, ptr align 8 %edge, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<Node,Type>::into_node
  %11 = invoke { i64, ptr } @"_ZN5alloc11collections5btree4node25Handle$LT$Node$C$Type$GT$9into_node17h5f3e44ec526c8f25E"(ptr %_7)
          to label %bb3 unwind label %cleanup

bb3:                                              ; preds = %bb2
  %_6.0 = extractvalue { i64, ptr } %11, 0
  %_6.1 = extractvalue { i64, ptr } %11, 1
; invoke <alloc::alloc::Global as core::clone::Clone>::clone
  invoke void @"_ZN59_$LT$alloc..alloc..Global$u20$as$u20$core..clone..Clone$GT$5clone17hc7b86e212f6c4442E"(ptr align 1 %alloc)
          to label %bb4 unwind label %cleanup

bb4:                                              ; preds = %bb3
; invoke alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::LeafOrInternal>::deallocate_and_ascend
  invoke void @"_ZN5alloc11collections5btree4node127NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$21deallocate_and_ascend17h48bae09deb1280ebE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>") %_5, i64 %_6.0, ptr %_6.1)
          to label %bb5 unwind label %cleanup

bb5:                                              ; preds = %bb4
  %12 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", ptr %_5, i32 0, i32 1
  %13 = load ptr, ptr %12, align 8
  %14 = ptrtoint ptr %13 to i64
  %15 = icmp eq i64 %14, 0
  %_10 = select i1 %15, i64 0, i64 1
  %16 = icmp eq i64 %_10, 1
  br i1 %16, label %bb6, label %bb8

bb6:                                              ; preds = %bb5
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %parent_edge, ptr align 8 %_5, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_13, ptr align 8 %parent_edge, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>,alloc::collections::btree::node::marker::Edge>::forget_node_type
  invoke void @"_ZN5alloc11collections5btree4node180Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$$C$alloc..collections..btree..node..marker..Edge$GT$16forget_node_type17hc4f3ad6111d770f2E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %_12, ptr %_13)
          to label %bb7 unwind label %cleanup

bb8:                                              ; preds = %bb5
  ret void

bb7:                                              ; preds = %bb6
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %edge, ptr align 8 %_12, i64 24, i1 false)
  br label %bb2
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_next
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$17deallocating_next17h077ae5518b094e4dE"(ptr sret(%"core::option::Option<(alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>)>") %0, ptr %self) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %1 = alloca { ptr, i32 }, align 8
  %slot = alloca %"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", align 8
  %self1 = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", align 8
  %tmp = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", align 8
  %_24 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %parent_edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_19 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_17 = alloca %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", align 8
  %last_edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_15 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %_12 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %_11 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %_10 = alloca { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }, align 8
  %kv = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %_7 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_6 = alloca %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>", align 8
  %_5 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %_4 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>", align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_4, ptr align 8 %self, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>::forget_node_type
  invoke void @"_ZN5alloc11collections5btree4node176Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$16forget_node_type17hb5ae9013da0ef86aE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %edge, ptr %_4)
          to label %bb1 unwind label %cleanup

bb17:                                             ; preds = %cleanup
  %2 = load ptr, ptr %1, align 8
  %3 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  %4 = load i32, ptr %3, align 8
  %5 = insertvalue { ptr, i32 } undef, ptr %2, 0
  %6 = insertvalue { ptr, i32 } %5, i32 %4, 1
  resume { ptr, i32 } %6

cleanup:                                          ; preds = %bb19, %bb13, %bb9, %bb8, %bb4, %bb2, %start
  %7 = landingpad { ptr, i32 }
          cleanup
  %8 = extractvalue { ptr, i32 } %7, 0
  %9 = extractvalue { ptr, i32 } %7, 1
  %10 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 0
  store ptr %8, ptr %10, align 8
  %11 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  store i32 %9, ptr %11, align 8
  br label %bb17

bb1:                                              ; preds = %start
  br label %bb2

bb2:                                              ; preds = %bb14, %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_7, ptr align 8 %edge, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,NodeType>,alloc::collections::btree::node::marker::Edge>::right_kv
  invoke void @"_ZN5alloc11collections5btree4node139Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..Edge$GT$8right_kv17h0321542d8560f8f4E"(ptr sret(%"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>") %_6, ptr %_7)
          to label %bb3 unwind label %cleanup

bb3:                                              ; preds = %bb2
  %_8 = load i64, ptr %_6, align 8, !range !16, !noundef !3
  %12 = icmp eq i64 %_8, 0
  br i1 %12, label %bb6, label %bb4

bb6:                                              ; preds = %bb3
  %13 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>::Ok", ptr %_6, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %kv, ptr align 8 %13, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %kv, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %self1, ptr align 8 %tmp, i64 24, i1 false)
  br label %bb19

bb4:                                              ; preds = %bb3
  %14 = getelementptr inbounds %"core::result::Result<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>>::Err", ptr %_6, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %last_edge, ptr align 8 %14, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_19, ptr align 8 %last_edge, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<Node,Type>::into_node
  %15 = invoke { i64, ptr } @"_ZN5alloc11collections5btree4node25Handle$LT$Node$C$Type$GT$9into_node17h5f3e44ec526c8f25E"(ptr %_19)
          to label %bb8 unwind label %cleanup

bb5:                                              ; No predecessors!
  unreachable

bb8:                                              ; preds = %bb4
  %_18.0 = extractvalue { i64, ptr } %15, 0
  %_18.1 = extractvalue { i64, ptr } %15, 1
; invoke <alloc::alloc::Global as core::clone::Clone>::clone
  invoke void @"_ZN59_$LT$alloc..alloc..Global$u20$as$u20$core..clone..Clone$GT$5clone17hc7b86e212f6c4442E"(ptr align 1 %alloc)
          to label %bb9 unwind label %cleanup

bb9:                                              ; preds = %bb8
; invoke alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::LeafOrInternal>::deallocate_and_ascend
  invoke void @"_ZN5alloc11collections5btree4node127NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$21deallocate_and_ascend17h48bae09deb1280ebE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>") %_17, i64 %_18.0, ptr %_18.1)
          to label %bb10 unwind label %cleanup

bb10:                                             ; preds = %bb9
  %16 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Internal>, alloc::collections::btree::node::marker::Edge>>", ptr %_17, i32 0, i32 1
  %17 = load ptr, ptr %16, align 8
  %18 = ptrtoint ptr %17 to i64
  %19 = icmp eq i64 %18, 0
  %_22 = select i1 %19, i64 0, i64 1
  %20 = icmp eq i64 %_22, 0
  br i1 %20, label %bb11, label %bb13

bb11:                                             ; preds = %bb10
  %21 = getelementptr inbounds %"core::option::Option<(alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>)>", ptr %0, i32 0, i32 1
  store ptr null, ptr %21, align 8
  br label %bb15

bb13:                                             ; preds = %bb10
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %parent_edge, ptr align 8 %_17, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_24, ptr align 8 %parent_edge, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::Internal>,alloc::collections::btree::node::marker::Edge>::forget_node_type
  invoke void @"_ZN5alloc11collections5btree4node180Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..Internal$GT$$C$alloc..collections..btree..node..marker..Edge$GT$16forget_node_type17hc4f3ad6111d770f2E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::Edge>") %_5, ptr %_24)
          to label %bb14 unwind label %cleanup

bb12:                                             ; No predecessors!
  unreachable

bb14:                                             ; preds = %bb13
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %edge, ptr align 8 %_5, i64 24, i1 false)
  br label %bb2

bb15:                                             ; preds = %bb7, %bb11
  ret void

bb19:                                             ; preds = %bb6
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %slot, ptr align 8 %self1, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_12, ptr align 8 %slot, i64 24, i1 false)
; invoke alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>,alloc::collections::btree::node::marker::KV>>::next_leaf_edge
  invoke void @"_ZN5alloc11collections5btree8navigate235_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$C$alloc..collections..btree..node..marker..KV$GT$$GT$14next_leaf_edge17hd342fc45b3b9ddd0E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %_11, ptr %_12)
          to label %bb7 unwind label %cleanup

bb7:                                              ; preds = %bb19
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_15, ptr align 8 %kv, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_10, ptr align 8 %_11, i64 24, i1 false)
  %22 = getelementptr inbounds { %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }, ptr %_10, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %22, ptr align 8 %_15, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_10, i64 48, i1 false)
  br label %bb15
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_next_unchecked
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$27deallocating_next_unchecked17hdaa3082be25dfb2dE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %_4 = alloca %"[closure@alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>::deallocating_next_unchecked<alloc::alloc::Global>::{closure#0}]", align 1
; call alloc::collections::btree::mem::replace
  call void @_ZN5alloc11collections5btree3mem7replace17h27db3d96b45b3f37E(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %0, ptr align 8 %self)
  ret void
}

; alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_next_unchecked::{{closure}}
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$27deallocating_next_unchecked28_$u7b$$u7b$closure$u7d$$u7d$17hfa9fcfd18cbdac44E"(ptr sret({ %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>" }) %0, ptr %leaf_edge) unnamed_addr #1 {
start:
  %_4 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %self = alloca %"core::option::Option<(alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>)>", align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_4, ptr align 8 %leaf_edge, i64 24, i1 false)
; call alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_next
  call void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$17deallocating_next17h077ae5518b094e4dE"(ptr sret(%"core::option::Option<(alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>)>") %self, ptr %_4)
  %1 = getelementptr inbounds %"core::option::Option<(alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>, alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>)>", ptr %self, i32 0, i32 1
  %2 = load ptr, ptr %1, align 8
  %3 = ptrtoint ptr %2 to i64
  %4 = icmp eq i64 %3, 0
  %_6 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_6, 0
  br i1 %5, label %bb2, label %bb4

bb2:                                              ; preds = %start
; call core::panicking::panic
  call void @_ZN4core9panicking5panic17h545818946343732bE(ptr align 1 @alloc146, i64 43, ptr align 8 @alloc145) #11
  unreachable

bb4:                                              ; preds = %start
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %self, i64 48, i1 false)
  ret void

bb3:                                              ; No predecessors!
  unreachable
}

; alloc::collections::btree::navigate::LazyLeafRange<BorrowType,K,V>::init_front
; Function Attrs: nonlazybind uwtable
define internal align 8 ptr @"_ZN5alloc11collections5btree8navigate39LazyLeafRange$LT$BorrowType$C$K$C$V$GT$10init_front17h4cd54159a6b1eee1E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %tmp = alloca { i64, ptr }, align 8
  %_8 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %_7 = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_6 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %0 = alloca ptr, align 8
  %1 = load i64, ptr %self, align 8, !range !17, !noundef !3
  %2 = icmp eq i64 %1, 2
  %_4 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_4, 1
  br i1 %3, label %bb1, label %bb4

bb1:                                              ; preds = %start
  %_3 = load i64, ptr %self, align 8, !range !16, !noundef !3
  %4 = icmp eq i64 %_3, 0
  br i1 %4, label %bb2, label %bb4

bb4:                                              ; preds = %bb2, %bb1, %start
  %5 = load i64, ptr %self, align 8, !range !17, !noundef !3
  %6 = icmp eq i64 %5, 2
  %_13 = select i1 %6, i64 0, i64 1
  %7 = icmp eq i64 %_13, 0
  br i1 %7, label %bb8, label %bb5

bb2:                                              ; preds = %bb1
  %root = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Root", ptr %self, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %root, i64 16, i1 false)
  %8 = getelementptr inbounds { i64, ptr }, ptr %tmp, i32 0, i32 0
  %self.0 = load i64, ptr %8, align 8
  %9 = getelementptr inbounds { i64, ptr }, ptr %tmp, i32 0, i32 1
  %self.1 = load ptr, ptr %9, align 8
; call alloc::collections::btree::navigate::<impl alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>>::first_leaf_edge
  call void @"_ZN5alloc11collections5btree8navigate142_$LT$impl$u20$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$GT$15first_leaf_edge17h8db01edec0751c55E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %_8, i64 %self.0, ptr %self.1)
  %10 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Edge", ptr %_7, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %10, ptr align 8 %_8, i64 24, i1 false)
  store i64 1, ptr %_7, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_6, ptr align 8 %_7, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %self, ptr align 8 %_6, i64 32, i1 false)
  br label %bb4

bb8:                                              ; preds = %bb4
  store ptr null, ptr %0, align 8
  br label %bb10

bb5:                                              ; preds = %bb4
  %_12 = load i64, ptr %self, align 8, !range !16, !noundef !3
  %11 = icmp eq i64 %_12, 0
  br i1 %11, label %bb6, label %bb9

bb6:                                              ; preds = %bb5
; call core::hint::unreachable_unchecked
  call void @_ZN4core4hint21unreachable_unchecked17hf62cde02ee11bb42E() #11
  unreachable

bb9:                                              ; preds = %bb5
  %edge = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Edge", ptr %self, i32 0, i32 1
  store ptr %edge, ptr %0, align 8
  br label %bb10

bb7:                                              ; No predecessors!
  unreachable

bb10:                                             ; preds = %bb8, %bb9
  %12 = load ptr, ptr %0, align 8, !align !4
  ret ptr %12
}

; alloc::collections::btree::navigate::LazyLeafRange<BorrowType,K,V>::none
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate39LazyLeafRange$LT$BorrowType$C$K$C$V$GT$4none17h7c9883f987bb6c7aE"(ptr sret(%"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %0) unnamed_addr #0 {
start:
  %_2 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %_1 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  store i64 2, ptr %_1, align 8
  store i64 2, ptr %_2, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_1, i64 32, i1 false)
  %1 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", ptr %0, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %1, ptr align 8 %_2, i64 32, i1 false)
  ret void
}

; alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying,K,V>::take_front
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate75LazyLeafRange$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$GT$10take_front17h3f89dd6bc6139b21E"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %_20 = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %v = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_17 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %_15 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %edge = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %_12 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %val = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %self1 = alloca %"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %_3 = alloca %"core::ops::control_flow::ControlFlow<core::option::Option<core::convert::Infallible>, alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %_2 = alloca %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  store i64 2, ptr %_17, align 8
; call core::mem::replace
  call void @_ZN4core3mem7replace17h6f2a57b6a070d255E(ptr sret(%"core::option::Option<alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>") %self1, ptr align 8 %self, ptr %_17)
  %1 = load i64, ptr %self1, align 8, !range !17, !noundef !3
  %2 = icmp eq i64 %1, 2
  %_18 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_18, 0
  br i1 %3, label %bb12, label %bb14

bb12:                                             ; preds = %start
  store i64 2, ptr %_3, align 8
  br label %bb1

bb14:                                             ; preds = %start
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %v, ptr align 8 %self1, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_20, ptr align 8 %v, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_3, ptr align 8 %_20, i64 32, i1 false)
  br label %bb1

bb13:                                             ; No predecessors!
  unreachable

bb1:                                              ; preds = %bb12, %bb14
  %4 = load i64, ptr %_3, align 8, !range !17, !noundef !3
  %5 = icmp eq i64 %4, 2
  %_6 = select i1 %5, i64 1, i64 0
  %6 = icmp eq i64 %_6, 0
  br i1 %6, label %bb2, label %bb4

bb2:                                              ; preds = %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %val, ptr align 8 %_3, i64 32, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_2, ptr align 8 %val, i64 32, i1 false)
  %_10 = load i64, ptr %_2, align 8, !range !16, !noundef !3
  %7 = icmp eq i64 %_10, 0
  br i1 %7, label %bb7, label %bb5

bb4:                                              ; preds = %bb1
  %8 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>", ptr %0, i32 0, i32 1
  store ptr null, ptr %8, align 8
  br label %bb10

bb3:                                              ; No predecessors!
  unreachable

bb10:                                             ; preds = %bb9, %bb4
  ret void

bb7:                                              ; preds = %bb2
  %9 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Root", ptr %_2, i32 0, i32 1
  %10 = getelementptr inbounds { i64, ptr }, ptr %9, i32 0, i32 0
  %root.0 = load i64, ptr %10, align 8
  %11 = getelementptr inbounds { i64, ptr }, ptr %9, i32 0, i32 1
  %root.1 = load ptr, ptr %11, align 8, !nonnull !3, !noundef !3
; call alloc::collections::btree::navigate::<impl alloc::collections::btree::node::NodeRef<BorrowType,K,V,alloc::collections::btree::node::marker::LeafOrInternal>>::first_leaf_edge
  call void @"_ZN5alloc11collections5btree8navigate142_$LT$impl$u20$alloc..collections..btree..node..NodeRef$LT$BorrowType$C$K$C$V$C$alloc..collections..btree..node..marker..LeafOrInternal$GT$$GT$15first_leaf_edge17h8db01edec0751c55E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>") %_12, i64 %root.0, ptr %root.1)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_12, i64 24, i1 false)
  br label %bb9

bb5:                                              ; preds = %bb2
  %12 = getelementptr inbounds %"alloc::collections::btree::navigate::LazyLeafHandle<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>::Edge", ptr %_2, i32 0, i32 1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %edge, ptr align 8 %12, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_15, ptr align 8 %edge, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_15, i64 24, i1 false)
  br label %bb9

bb6:                                              ; No predecessors!
  unreachable

bb9:                                              ; preds = %bb7, %bb5
  br label %bb10
}

; alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying,K,V>::deallocating_end
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate75LazyLeafRange$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$GT$16deallocating_end17hec7fe9a69531f670E"(ptr align 8 %self) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_9 = alloca i8, align 1
  %_7 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %front = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>", align 8
  %_3 = alloca %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>", align 8
  store i8 1, ptr %_9, align 1
; invoke alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying,K,V>::take_front
  invoke void @"_ZN5alloc11collections5btree8navigate75LazyLeafRange$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$GT$10take_front17h3f89dd6bc6139b21E"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>") %_3, ptr align 8 %self)
          to label %bb1 unwind label %cleanup

bb9:                                              ; preds = %cleanup
  %1 = load i8, ptr %_9, align 1, !range !7, !noundef !3
  %2 = trunc i8 %1 to i1
  br i1 %2, label %bb8, label %bb6

cleanup:                                          ; preds = %bb2, %start
  %3 = landingpad { ptr, i32 }
          cleanup
  %4 = extractvalue { ptr, i32 } %3, 0
  %5 = extractvalue { ptr, i32 } %3, 1
  %6 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %4, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %5, ptr %7, align 8
  br label %bb9

bb1:                                              ; preds = %start
  %8 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::Leaf>, alloc::collections::btree::node::marker::Edge>>", ptr %_3, i32 0, i32 1
  %9 = load ptr, ptr %8, align 8
  %10 = ptrtoint ptr %9 to i64
  %11 = icmp eq i64 %10, 0
  %_5 = select i1 %11, i64 0, i64 1
  %12 = icmp eq i64 %_5, 1
  br i1 %12, label %bb2, label %bb4

bb2:                                              ; preds = %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %front, ptr align 8 %_3, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_7, ptr align 8 %front, i64 24, i1 false)
  store i8 0, ptr %_9, align 1
; invoke alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_end
  invoke void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$16deallocating_end17h36556c86a06bf3fbE"(ptr %_7)
          to label %bb3 unwind label %cleanup

bb4:                                              ; preds = %bb3, %bb1
  %13 = load i8, ptr %_9, align 1, !range !7, !noundef !3
  %14 = trunc i8 %13 to i1
  br i1 %14, label %bb7, label %bb5

bb3:                                              ; preds = %bb2
  br label %bb4

bb6:                                              ; preds = %bb8, %bb9
  %15 = load ptr, ptr %0, align 8
  %16 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %17 = load i32, ptr %16, align 8
  %18 = insertvalue { ptr, i32 } undef, ptr %15, 0
  %19 = insertvalue { ptr, i32 } %18, i32 %17, 1
  resume { ptr, i32 } %19

bb8:                                              ; preds = %bb9
  br label %bb6

bb5:                                              ; preds = %bb7, %bb4
  ret void

bb7:                                              ; preds = %bb4
  br label %bb5
}

; alloc::collections::btree::navigate::LazyLeafRange<alloc::collections::btree::node::marker::Dying,K,V>::deallocating_next_unchecked
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN5alloc11collections5btree8navigate75LazyLeafRange$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$GT$27deallocating_next_unchecked17h31e3a6e9582f3278E"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %0, ptr align 8 %self) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %1 = alloca { ptr, i32 }, align 8
  %_9 = alloca i8, align 1
  %self1 = alloca ptr, align 8
  store i8 1, ptr %_9, align 1
; invoke alloc::collections::btree::navigate::LazyLeafRange<BorrowType,K,V>::init_front
  %2 = invoke align 8 ptr @"_ZN5alloc11collections5btree8navigate39LazyLeafRange$LT$BorrowType$C$K$C$V$GT$10init_front17h4cd54159a6b1eee1E"(ptr align 8 %self)
          to label %bb1 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  %3 = load i8, ptr %_9, align 1, !range !7, !noundef !3
  %4 = trunc i8 %3 to i1
  br i1 %4, label %bb4, label %bb3

cleanup:                                          ; preds = %bb6, %bb8, %start
  %5 = landingpad { ptr, i32 }
          cleanup
  %6 = extractvalue { ptr, i32 } %5, 0
  %7 = extractvalue { ptr, i32 } %5, 1
  %8 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 0
  store ptr %6, ptr %8, align 8
  %9 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  store i32 %7, ptr %9, align 8
  br label %bb5

bb1:                                              ; preds = %start
  store ptr %2, ptr %self1, align 8
  %10 = load ptr, ptr %self1, align 8
  %11 = ptrtoint ptr %10 to i64
  %12 = icmp eq i64 %11, 0
  %_10 = select i1 %12, i64 0, i64 1
  %13 = icmp eq i64 %_10, 0
  br i1 %13, label %bb6, label %bb8

bb6:                                              ; preds = %bb1
; invoke core::panicking::panic
  invoke void @_ZN4core9panicking5panic17h545818946343732bE(ptr align 1 @alloc146, i64 43, ptr align 8 @alloc148) #11
          to label %unreachable unwind label %cleanup

bb8:                                              ; preds = %bb1
  %front = load ptr, ptr %self1, align 8, !nonnull !3, !align !4, !noundef !3
  store i8 0, ptr %_9, align 1
; invoke alloc::collections::btree::navigate::<impl alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,alloc::collections::btree::node::marker::Leaf>,alloc::collections::btree::node::marker::Edge>>::deallocating_next_unchecked
  invoke void @"_ZN5alloc11collections5btree8navigate263_$LT$impl$u20$alloc..collections..btree..node..Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$alloc..collections..btree..node..marker..Leaf$GT$$C$alloc..collections..btree..node..marker..Edge$GT$$GT$27deallocating_next_unchecked17hdaa3082be25dfb2dE"(ptr sret(%"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>") %0, ptr align 8 %front)
          to label %bb2 unwind label %cleanup

bb7:                                              ; No predecessors!
  unreachable

bb2:                                              ; preds = %bb8
  ret void

unreachable:                                      ; preds = %bb6
  unreachable

bb3:                                              ; preds = %bb4, %bb5
  %14 = load ptr, ptr %1, align 8
  %15 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  %16 = load i32, ptr %15, align 8
  %17 = insertvalue { ptr, i32 } undef, ptr %14, 0
  %18 = insertvalue { ptr, i32 } %17, i32 %16, 1
  resume { ptr, i32 } %18

bb4:                                              ; preds = %bb5
  br label %bb3
}

; alloc::alloc::box_free
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN5alloc5alloc8box_free17h5622a6443d75e9faE(ptr %0) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %1 = alloca { ptr, i32 }, align 8
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %unique = alloca ptr, align 8
  %_15 = alloca ptr, align 8
  %layout = alloca { i64, i64 }, align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  %ptr = alloca ptr, align 8
  store ptr %0, ptr %ptr, align 8
  %self = load ptr, ptr %ptr, align 8, !nonnull !3, !noundef !3
  store i64 24, ptr %4, align 8
  %size = load i64, ptr %4, align 8
  %self1 = load ptr, ptr %ptr, align 8, !nonnull !3, !noundef !3
  store i64 8, ptr %3, align 8
  %align = load i64, ptr %3, align 8
  store i64 %align, ptr %2, align 8
  %_28 = load i64, ptr %2, align 8, !range !14, !noundef !3
  br label %bb7

bb7:                                              ; preds = %start
  store i64 %size, ptr %layout, align 8
  %5 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  store i64 %_28, ptr %5, align 8
  %self2 = load ptr, ptr %ptr, align 8, !nonnull !3, !noundef !3
  store ptr %self2, ptr %pointer, align 8
  %_44 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_44, ptr %unique, align 8
  %self3 = load ptr, ptr %unique, align 8, !nonnull !3, !noundef !3
  store ptr %self3, ptr %_15, align 8
  %6 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  %_18.0 = load i64, ptr %6, align 8
  %7 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  %_18.1 = load i64, ptr %7, align 8, !range !14, !noundef !3
  %8 = load ptr, ptr %_15, align 8, !nonnull !3, !noundef !3
; invoke <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  invoke void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %alloc, ptr %8, i64 %_18.0, i64 %_18.1)
          to label %bb3 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  %9 = load ptr, ptr %1, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  %11 = load i32, ptr %10, align 8
  %12 = insertvalue { ptr, i32 } undef, ptr %9, 0
  %13 = insertvalue { ptr, i32 } %12, i32 %11, 1
  resume { ptr, i32 } %13

cleanup:                                          ; preds = %bb7
  %14 = landingpad { ptr, i32 }
          cleanup
  %15 = extractvalue { ptr, i32 } %14, 0
  %16 = extractvalue { ptr, i32 } %14, 1
  %17 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 0
  store ptr %15, ptr %17, align 8
  %18 = getelementptr inbounds { ptr, i32 }, ptr %1, i32 0, i32 1
  store i32 %16, ptr %18, align 8
  br label %bb5

bb3:                                              ; preds = %bb7
  ret void
}

; alloc::alloc::box_free
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN5alloc5alloc8box_free17ha6e557d1a719ef37E(ptr %0, i64 %1) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %2 = alloca { ptr, i32 }, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %unique = alloca ptr, align 8
  %_15 = alloca ptr, align 8
  %layout = alloca { i64, i64 }, align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  %ptr = alloca { ptr, i64 }, align 8
  %6 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  store ptr %0, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  store i64 %1, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  %self.0 = load ptr, ptr %8, align 8, !nonnull !3, !noundef !3
  %9 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  %self.1 = load i64, ptr %9, align 8
  %10 = mul nsw i64 %self.1, 1
  store i64 %10, ptr %5, align 8
  %size = load i64, ptr %5, align 8
  %11 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  %self.01 = load ptr, ptr %11, align 8, !nonnull !3, !noundef !3
  %12 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  %self.12 = load i64, ptr %12, align 8
  %13 = mul nsw i64 %self.12, 1
  store i64 1, ptr %4, align 8
  %align = load i64, ptr %4, align 8
  store i64 %align, ptr %3, align 8
  %_28 = load i64, ptr %3, align 8, !range !14, !noundef !3
  br label %bb7

bb7:                                              ; preds = %start
  store i64 %size, ptr %layout, align 8
  %14 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  store i64 %_28, ptr %14, align 8
  %15 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  %self.03 = load ptr, ptr %15, align 8, !nonnull !3, !noundef !3
  %16 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  %self.14 = load i64, ptr %16, align 8
  store ptr %self.03, ptr %pointer, align 8
  %_44 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_44, ptr %unique, align 8
  %self = load ptr, ptr %unique, align 8, !nonnull !3, !noundef !3
  store ptr %self, ptr %_15, align 8
  %17 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  %_18.0 = load i64, ptr %17, align 8
  %18 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  %_18.1 = load i64, ptr %18, align 8, !range !14, !noundef !3
  %19 = load ptr, ptr %_15, align 8, !nonnull !3, !noundef !3
; invoke <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  invoke void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %alloc, ptr %19, i64 %_18.0, i64 %_18.1)
          to label %bb3 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  %20 = load ptr, ptr %2, align 8
  %21 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %22 = load i32, ptr %21, align 8
  %23 = insertvalue { ptr, i32 } undef, ptr %20, 0
  %24 = insertvalue { ptr, i32 } %23, i32 %22, 1
  resume { ptr, i32 } %24

cleanup:                                          ; preds = %bb7
  %25 = landingpad { ptr, i32 }
          cleanup
  %26 = extractvalue { ptr, i32 } %25, 0
  %27 = extractvalue { ptr, i32 } %25, 1
  %28 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %26, ptr %28, align 8
  %29 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %27, ptr %29, align 8
  br label %bb5

bb3:                                              ; preds = %bb7
  ret void
}

; alloc::alloc::box_free
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN5alloc5alloc8box_free17hdddc60ee21b60f9cE(ptr %0, i64 %1) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %2 = alloca { ptr, i32 }, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %unique = alloca ptr, align 8
  %_15 = alloca ptr, align 8
  %layout = alloca { i64, i64 }, align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  %ptr = alloca { ptr, i64 }, align 8
  %6 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  store ptr %0, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  store i64 %1, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  %self.0 = load ptr, ptr %8, align 8, !nonnull !3, !noundef !3
  %9 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  %self.1 = load i64, ptr %9, align 8
  %10 = mul nsw i64 %self.1, 4
  store i64 %10, ptr %5, align 8
  %size = load i64, ptr %5, align 8
  %11 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  %self.01 = load ptr, ptr %11, align 8, !nonnull !3, !noundef !3
  %12 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  %self.12 = load i64, ptr %12, align 8
  %13 = mul nsw i64 %self.12, 4
  store i64 4, ptr %4, align 8
  %align = load i64, ptr %4, align 8
  store i64 %align, ptr %3, align 8
  %_28 = load i64, ptr %3, align 8, !range !14, !noundef !3
  br label %bb7

bb7:                                              ; preds = %start
  store i64 %size, ptr %layout, align 8
  %14 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  store i64 %_28, ptr %14, align 8
  %15 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 0
  %self.03 = load ptr, ptr %15, align 8, !nonnull !3, !noundef !3
  %16 = getelementptr inbounds { ptr, i64 }, ptr %ptr, i32 0, i32 1
  %self.14 = load i64, ptr %16, align 8
  store ptr %self.03, ptr %pointer, align 8
  %_44 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_44, ptr %unique, align 8
  %self = load ptr, ptr %unique, align 8, !nonnull !3, !noundef !3
  store ptr %self, ptr %_15, align 8
  %17 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  %_18.0 = load i64, ptr %17, align 8
  %18 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  %_18.1 = load i64, ptr %18, align 8, !range !14, !noundef !3
  %19 = load ptr, ptr %_15, align 8, !nonnull !3, !noundef !3
; invoke <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  invoke void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %alloc, ptr %19, i64 %_18.0, i64 %_18.1)
          to label %bb3 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  %20 = load ptr, ptr %2, align 8
  %21 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %22 = load i32, ptr %21, align 8
  %23 = insertvalue { ptr, i32 } undef, ptr %20, 0
  %24 = insertvalue { ptr, i32 } %23, i32 %22, 1
  resume { ptr, i32 } %24

cleanup:                                          ; preds = %bb7
  %25 = landingpad { ptr, i32 }
          cleanup
  %26 = extractvalue { ptr, i32 } %25, 0
  %27 = extractvalue { ptr, i32 } %25, 1
  %28 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %26, ptr %28, align 8
  %29 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %27, ptr %29, align 8
  br label %bb5

bb3:                                              ; preds = %bb7
  ret void
}

; alloc::alloc::box_free
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN5alloc5alloc8box_free17hed4c17db642cc9b9E(ptr %0, ptr align 8 %1) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %2 = alloca { ptr, i32 }, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %unique = alloca ptr, align 8
  %_15 = alloca ptr, align 8
  %layout = alloca { i64, i64 }, align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  %ptr = alloca { ptr, ptr }, align 8
  %6 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  store ptr %0, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  store ptr %1, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  %self.0 = load ptr, ptr %8, align 8, !nonnull !3, !noundef !3
  %9 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  %self.1 = load ptr, ptr %9, align 8, !nonnull !3, !align !4, !noundef !3
  %10 = getelementptr inbounds i64, ptr %self.1, i64 1
  %11 = load i64, ptr %10, align 8, !invariant.load !3
  %12 = getelementptr inbounds i64, ptr %self.1, i64 2
  %13 = load i64, ptr %12, align 8, !range !18, !invariant.load !3
  store i64 %11, ptr %5, align 8
  %size = load i64, ptr %5, align 8
  %14 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  %self.01 = load ptr, ptr %14, align 8, !nonnull !3, !noundef !3
  %15 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  %self.12 = load ptr, ptr %15, align 8, !nonnull !3, !align !4, !noundef !3
  %16 = getelementptr inbounds i64, ptr %self.12, i64 1
  %17 = load i64, ptr %16, align 8, !invariant.load !3
  %18 = getelementptr inbounds i64, ptr %self.12, i64 2
  %19 = load i64, ptr %18, align 8, !range !18, !invariant.load !3
  store i64 %19, ptr %4, align 8
  %align = load i64, ptr %4, align 8
  store i64 %align, ptr %3, align 8
  %_28 = load i64, ptr %3, align 8, !range !14, !noundef !3
  br label %bb7

bb7:                                              ; preds = %start
  store i64 %size, ptr %layout, align 8
  %20 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  store i64 %_28, ptr %20, align 8
  %21 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  %self.03 = load ptr, ptr %21, align 8, !nonnull !3, !noundef !3
  %22 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  %self.14 = load ptr, ptr %22, align 8, !nonnull !3, !align !4, !noundef !3
  store ptr %self.03, ptr %pointer, align 8
  %_44 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_44, ptr %unique, align 8
  %self = load ptr, ptr %unique, align 8, !nonnull !3, !noundef !3
  store ptr %self, ptr %_15, align 8
  %23 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  %_18.0 = load i64, ptr %23, align 8
  %24 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  %_18.1 = load i64, ptr %24, align 8, !range !14, !noundef !3
  %25 = load ptr, ptr %_15, align 8, !nonnull !3, !noundef !3
; invoke <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  invoke void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %alloc, ptr %25, i64 %_18.0, i64 %_18.1)
          to label %bb3 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  %26 = load ptr, ptr %2, align 8
  %27 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %28 = load i32, ptr %27, align 8
  %29 = insertvalue { ptr, i32 } undef, ptr %26, 0
  %30 = insertvalue { ptr, i32 } %29, i32 %28, 1
  resume { ptr, i32 } %30

cleanup:                                          ; preds = %bb7
  %31 = landingpad { ptr, i32 }
          cleanup
  %32 = extractvalue { ptr, i32 } %31, 0
  %33 = extractvalue { ptr, i32 } %31, 1
  %34 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %32, ptr %34, align 8
  %35 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %33, ptr %35, align 8
  br label %bb5

bb3:                                              ; preds = %bb7
  ret void
}

; alloc::alloc::box_free
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @_ZN5alloc5alloc8box_free17hf6cafae124ae9d34E(ptr %0, ptr align 8 %1) unnamed_addr #1 personality ptr @rust_eh_personality {
start:
  %2 = alloca { ptr, i32 }, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %unique = alloca ptr, align 8
  %_15 = alloca ptr, align 8
  %layout = alloca { i64, i64 }, align 8
  %alloc = alloca %"alloc::alloc::Global", align 1
  %ptr = alloca { ptr, ptr }, align 8
  %6 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  store ptr %0, ptr %6, align 8
  %7 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  store ptr %1, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  %self.0 = load ptr, ptr %8, align 8, !nonnull !3, !noundef !3
  %9 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  %self.1 = load ptr, ptr %9, align 8, !nonnull !3, !align !4, !noundef !3
  %10 = getelementptr inbounds i64, ptr %self.1, i64 1
  %11 = load i64, ptr %10, align 8, !invariant.load !3
  %12 = getelementptr inbounds i64, ptr %self.1, i64 2
  %13 = load i64, ptr %12, align 8, !range !18, !invariant.load !3
  store i64 %11, ptr %5, align 8
  %size = load i64, ptr %5, align 8
  %14 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  %self.01 = load ptr, ptr %14, align 8, !nonnull !3, !noundef !3
  %15 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  %self.12 = load ptr, ptr %15, align 8, !nonnull !3, !align !4, !noundef !3
  %16 = getelementptr inbounds i64, ptr %self.12, i64 1
  %17 = load i64, ptr %16, align 8, !invariant.load !3
  %18 = getelementptr inbounds i64, ptr %self.12, i64 2
  %19 = load i64, ptr %18, align 8, !range !18, !invariant.load !3
  store i64 %19, ptr %4, align 8
  %align = load i64, ptr %4, align 8
  store i64 %align, ptr %3, align 8
  %_28 = load i64, ptr %3, align 8, !range !14, !noundef !3
  br label %bb7

bb7:                                              ; preds = %start
  store i64 %size, ptr %layout, align 8
  %20 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  store i64 %_28, ptr %20, align 8
  %21 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 0
  %self.03 = load ptr, ptr %21, align 8, !nonnull !3, !noundef !3
  %22 = getelementptr inbounds { ptr, ptr }, ptr %ptr, i32 0, i32 1
  %self.14 = load ptr, ptr %22, align 8, !nonnull !3, !align !4, !noundef !3
  store ptr %self.03, ptr %pointer, align 8
  %_44 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_44, ptr %unique, align 8
  %self = load ptr, ptr %unique, align 8, !nonnull !3, !noundef !3
  store ptr %self, ptr %_15, align 8
  %23 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  %_18.0 = load i64, ptr %23, align 8
  %24 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  %_18.1 = load i64, ptr %24, align 8, !range !14, !noundef !3
  %25 = load ptr, ptr %_15, align 8, !nonnull !3, !noundef !3
; invoke <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  invoke void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %alloc, ptr %25, i64 %_18.0, i64 %_18.1)
          to label %bb3 unwind label %cleanup

bb5:                                              ; preds = %cleanup
  %26 = load ptr, ptr %2, align 8
  %27 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  %28 = load i32, ptr %27, align 8
  %29 = insertvalue { ptr, i32 } undef, ptr %26, 0
  %30 = insertvalue { ptr, i32 } %29, i32 %28, 1
  resume { ptr, i32 } %30

cleanup:                                          ; preds = %bb7
  %31 = landingpad { ptr, i32 }
          cleanup
  %32 = extractvalue { ptr, i32 } %31, 0
  %33 = extractvalue { ptr, i32 } %31, 1
  %34 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 0
  store ptr %32, ptr %34, align 8
  %35 = getelementptr inbounds { ptr, i32 }, ptr %2, i32 0, i32 1
  store i32 %33, ptr %35, align 8
  br label %bb5

bb3:                                              ; preds = %bb7
  ret void
}

; alloc::boxed::Box<T,A>::from_raw_in
; Function Attrs: inlinehint nonlazybind uwtable
define internal align 8 ptr @"_ZN5alloc5boxed16Box$LT$T$C$A$GT$11from_raw_in17h3a80057e5a6ecbadE"(ptr %raw) unnamed_addr #1 {
start:
  %_6 = alloca ptr, align 8
  %_3 = alloca ptr, align 8
  %0 = alloca ptr, align 8
  store ptr %raw, ptr %_6, align 8
  %1 = load ptr, ptr %_6, align 8, !nonnull !3, !noundef !3
  store ptr %1, ptr %_3, align 8
  %2 = load ptr, ptr %_3, align 8, !nonnull !3, !noundef !3
  store ptr %2, ptr %0, align 8
  %3 = load ptr, ptr %0, align 8, !nonnull !3, !align !4, !noundef !3
  ret ptr %3
}

; alloc::raw_vec::RawVec<T,A>::current_memory
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17h4424370672e8098cE"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %1 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %_10 = alloca ptr, align 8
  %_8 = alloca { ptr, { i64, i64 } }, align 8
  %_2 = alloca i8, align 1
  br i1 false, label %bb1, label %bb2

bb2:                                              ; preds = %start
  %_4 = load i64, ptr %self, align 8
  %_3 = icmp eq i64 %_4, 0
  %2 = zext i1 %_3 to i8
  store i8 %2, ptr %_2, align 1
  br label %bb3

bb1:                                              ; preds = %start
  store i8 1, ptr %_2, align 1
  br label %bb3

bb3:                                              ; preds = %bb2, %bb1
  %3 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %4 = trunc i8 %3 to i1
  br i1 %4, label %bb4, label %bb5

bb5:                                              ; preds = %bb3
  %n = load i64, ptr %self, align 8
  store i64 8, ptr %1, align 8
  %_14 = load i64, ptr %1, align 8, !range !14, !noundef !3
; call core::alloc::layout::Layout::array::inner
  %5 = call { i64, i64 } @_ZN4core5alloc6layout6Layout5array5inner17h867f7bb670fb4284E(i64 8, i64 %_14, i64 %n)
  %_6.0 = extractvalue { i64, i64 } %5, 0
  %_6.1 = extractvalue { i64, i64 } %5, 1
; call core::result::Result<T,E>::unwrap_unchecked
  %6 = call { i64, i64 } @"_ZN4core6result19Result$LT$T$C$E$GT$16unwrap_unchecked17h2b9eb42ba2a15764E"(i64 %_6.0, i64 %_6.1, ptr align 8 @alloc150)
  %layout.0 = extractvalue { i64, i64 } %6, 0
  %layout.1 = extractvalue { i64, i64 } %6, 1
  %7 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %7, align 8, !nonnull !3, !noundef !3
  store ptr %self1, ptr %pointer, align 8
  %_31 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_31, ptr %_10, align 8
  %8 = load ptr, ptr %_10, align 8, !nonnull !3, !noundef !3
; call <T as core::convert::Into<U>>::into
  %_9 = call ptr @"_ZN50_$LT$T$u20$as$u20$core..convert..Into$LT$U$GT$$GT$4into17hbd8c27f68e5b0f88E"(ptr %8)
  store ptr %_9, ptr %_8, align 8
  %9 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_8, i32 0, i32 1
  %10 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 0
  store i64 %layout.0, ptr %10, align 8
  %11 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 1
  store i64 %layout.1, ptr %11, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_8, i64 24, i1 false)
  br label %bb8

bb4:                                              ; preds = %bb3
  %12 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %0, i32 0, i32 1
  store i64 0, ptr %12, align 8
  br label %bb8

bb8:                                              ; preds = %bb5, %bb4
  ret void
}

; alloc::raw_vec::RawVec<T,A>::current_memory
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17ha1bc40faff3957d8E"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %1 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %_10 = alloca ptr, align 8
  %_8 = alloca { ptr, { i64, i64 } }, align 8
  %_2 = alloca i8, align 1
  br i1 false, label %bb1, label %bb2

bb2:                                              ; preds = %start
  %_4 = load i64, ptr %self, align 8
  %_3 = icmp eq i64 %_4, 0
  %2 = zext i1 %_3 to i8
  store i8 %2, ptr %_2, align 1
  br label %bb3

bb1:                                              ; preds = %start
  store i8 1, ptr %_2, align 1
  br label %bb3

bb3:                                              ; preds = %bb2, %bb1
  %3 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %4 = trunc i8 %3 to i1
  br i1 %4, label %bb4, label %bb5

bb5:                                              ; preds = %bb3
  %n = load i64, ptr %self, align 8
  store i64 8, ptr %1, align 8
  %_14 = load i64, ptr %1, align 8, !range !14, !noundef !3
; call core::alloc::layout::Layout::array::inner
  %5 = call { i64, i64 } @_ZN4core5alloc6layout6Layout5array5inner17h867f7bb670fb4284E(i64 16, i64 %_14, i64 %n)
  %_6.0 = extractvalue { i64, i64 } %5, 0
  %_6.1 = extractvalue { i64, i64 } %5, 1
; call core::result::Result<T,E>::unwrap_unchecked
  %6 = call { i64, i64 } @"_ZN4core6result19Result$LT$T$C$E$GT$16unwrap_unchecked17h2b9eb42ba2a15764E"(i64 %_6.0, i64 %_6.1, ptr align 8 @alloc150)
  %layout.0 = extractvalue { i64, i64 } %6, 0
  %layout.1 = extractvalue { i64, i64 } %6, 1
  %7 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %7, align 8, !nonnull !3, !noundef !3
  store ptr %self1, ptr %pointer, align 8
  %_31 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_31, ptr %_10, align 8
  %8 = load ptr, ptr %_10, align 8, !nonnull !3, !noundef !3
; call <T as core::convert::Into<U>>::into
  %_9 = call ptr @"_ZN50_$LT$T$u20$as$u20$core..convert..Into$LT$U$GT$$GT$4into17hbd8c27f68e5b0f88E"(ptr %8)
  store ptr %_9, ptr %_8, align 8
  %9 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_8, i32 0, i32 1
  %10 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 0
  store i64 %layout.0, ptr %10, align 8
  %11 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 1
  store i64 %layout.1, ptr %11, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_8, i64 24, i1 false)
  br label %bb8

bb4:                                              ; preds = %bb3
  %12 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %0, i32 0, i32 1
  store i64 0, ptr %12, align 8
  br label %bb8

bb8:                                              ; preds = %bb5, %bb4
  ret void
}

; alloc::raw_vec::RawVec<T,A>::current_memory
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17hfa40f89bea1349cfE"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %1 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %_10 = alloca ptr, align 8
  %_8 = alloca { ptr, { i64, i64 } }, align 8
  %_2 = alloca i8, align 1
  br i1 false, label %bb1, label %bb2

bb2:                                              ; preds = %start
  %_4 = load i64, ptr %self, align 8
  %_3 = icmp eq i64 %_4, 0
  %2 = zext i1 %_3 to i8
  store i8 %2, ptr %_2, align 1
  br label %bb3

bb1:                                              ; preds = %start
  store i8 1, ptr %_2, align 1
  br label %bb3

bb3:                                              ; preds = %bb2, %bb1
  %3 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %4 = trunc i8 %3 to i1
  br i1 %4, label %bb4, label %bb5

bb5:                                              ; preds = %bb3
  %n = load i64, ptr %self, align 8
  store i64 8, ptr %1, align 8
  %_14 = load i64, ptr %1, align 8, !range !14, !noundef !3
; call core::alloc::layout::Layout::array::inner
  %5 = call { i64, i64 } @_ZN4core5alloc6layout6Layout5array5inner17h867f7bb670fb4284E(i64 16, i64 %_14, i64 %n)
  %_6.0 = extractvalue { i64, i64 } %5, 0
  %_6.1 = extractvalue { i64, i64 } %5, 1
; call core::result::Result<T,E>::unwrap_unchecked
  %6 = call { i64, i64 } @"_ZN4core6result19Result$LT$T$C$E$GT$16unwrap_unchecked17h2b9eb42ba2a15764E"(i64 %_6.0, i64 %_6.1, ptr align 8 @alloc150)
  %layout.0 = extractvalue { i64, i64 } %6, 0
  %layout.1 = extractvalue { i64, i64 } %6, 1
  %7 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %7, align 8, !nonnull !3, !noundef !3
  store ptr %self1, ptr %pointer, align 8
  %_31 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_31, ptr %_10, align 8
  %8 = load ptr, ptr %_10, align 8, !nonnull !3, !noundef !3
; call <T as core::convert::Into<U>>::into
  %_9 = call ptr @"_ZN50_$LT$T$u20$as$u20$core..convert..Into$LT$U$GT$$GT$4into17hbd8c27f68e5b0f88E"(ptr %8)
  store ptr %_9, ptr %_8, align 8
  %9 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_8, i32 0, i32 1
  %10 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 0
  store i64 %layout.0, ptr %10, align 8
  %11 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 1
  store i64 %layout.1, ptr %11, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_8, i64 24, i1 false)
  br label %bb8

bb4:                                              ; preds = %bb3
  %12 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %0, i32 0, i32 1
  store i64 0, ptr %12, align 8
  br label %bb8

bb8:                                              ; preds = %bb5, %bb4
  ret void
}

; alloc::raw_vec::RawVec<T,A>::current_memory
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17hfebd549caca97675E"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %0, ptr align 8 %self) unnamed_addr #0 {
start:
  %1 = alloca i64, align 8
  %pointer = alloca ptr, align 8
  %_10 = alloca ptr, align 8
  %_8 = alloca { ptr, { i64, i64 } }, align 8
  %_2 = alloca i8, align 1
  br i1 false, label %bb1, label %bb2

bb2:                                              ; preds = %start
  %_4 = load i64, ptr %self, align 8
  %_3 = icmp eq i64 %_4, 0
  %2 = zext i1 %_3 to i8
  store i8 %2, ptr %_2, align 1
  br label %bb3

bb1:                                              ; preds = %start
  store i8 1, ptr %_2, align 1
  br label %bb3

bb3:                                              ; preds = %bb2, %bb1
  %3 = load i8, ptr %_2, align 1, !range !7, !noundef !3
  %4 = trunc i8 %3 to i1
  br i1 %4, label %bb4, label %bb5

bb5:                                              ; preds = %bb3
  %n = load i64, ptr %self, align 8
  store i64 1, ptr %1, align 8
  %_14 = load i64, ptr %1, align 8, !range !14, !noundef !3
; call core::alloc::layout::Layout::array::inner
  %5 = call { i64, i64 } @_ZN4core5alloc6layout6Layout5array5inner17h867f7bb670fb4284E(i64 1, i64 %_14, i64 %n)
  %_6.0 = extractvalue { i64, i64 } %5, 0
  %_6.1 = extractvalue { i64, i64 } %5, 1
; call core::result::Result<T,E>::unwrap_unchecked
  %6 = call { i64, i64 } @"_ZN4core6result19Result$LT$T$C$E$GT$16unwrap_unchecked17h2b9eb42ba2a15764E"(i64 %_6.0, i64 %_6.1, ptr align 8 @alloc150)
  %layout.0 = extractvalue { i64, i64 } %6, 0
  %layout.1 = extractvalue { i64, i64 } %6, 1
  %7 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %7, align 8, !nonnull !3, !noundef !3
  store ptr %self1, ptr %pointer, align 8
  %_31 = load ptr, ptr %pointer, align 8, !nonnull !3, !noundef !3
  store ptr %_31, ptr %_10, align 8
  %8 = load ptr, ptr %_10, align 8, !nonnull !3, !noundef !3
; call <T as core::convert::Into<U>>::into
  %_9 = call ptr @"_ZN50_$LT$T$u20$as$u20$core..convert..Into$LT$U$GT$$GT$4into17hbd8c27f68e5b0f88E"(ptr %8)
  store ptr %_9, ptr %_8, align 8
  %9 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_8, i32 0, i32 1
  %10 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 0
  store i64 %layout.0, ptr %10, align 8
  %11 = getelementptr inbounds { i64, i64 }, ptr %9, i32 0, i32 1
  store i64 %layout.1, ptr %11, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %0, ptr align 8 %_8, i64 24, i1 false)
  br label %bb8

bb4:                                              ; preds = %bb3
  %12 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %0, i32 0, i32 1
  store i64 0, ptr %12, align 8
  br label %bb8

bb8:                                              ; preds = %bb5, %bb4
  ret void
}

; <alloc::alloc::Global as core::alloc::Allocator>::deallocate
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %self, ptr %ptr, i64 %0, i64 %1) unnamed_addr #1 {
start:
  %_16 = alloca i64, align 8
  %layout1 = alloca { i64, i64 }, align 8
  %layout = alloca { i64, i64 }, align 8
  %2 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  store i64 %0, ptr %2, align 8
  %3 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  store i64 %1, ptr %3, align 8
  %_4 = load i64, ptr %layout, align 8
  %4 = icmp eq i64 %_4, 0
  br i1 %4, label %bb2, label %bb1

bb2:                                              ; preds = %start
  br label %bb3

bb1:                                              ; preds = %start
  %5 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 0
  %6 = load i64, ptr %5, align 8
  %7 = getelementptr inbounds { i64, i64 }, ptr %layout, i32 0, i32 1
  %8 = load i64, ptr %7, align 8, !range !14, !noundef !3
  %9 = getelementptr inbounds { i64, i64 }, ptr %layout1, i32 0, i32 0
  store i64 %6, ptr %9, align 8
  %10 = getelementptr inbounds { i64, i64 }, ptr %layout1, i32 0, i32 1
  store i64 %8, ptr %10, align 8
  %_11 = load i64, ptr %layout1, align 8
  %11 = getelementptr inbounds { i64, i64 }, ptr %layout1, i32 0, i32 1
  %self2 = load i64, ptr %11, align 8, !range !14, !noundef !3
  store i64 %self2, ptr %_16, align 8
  %_17 = load i64, ptr %_16, align 8, !range !14, !noundef !3
  %_18 = icmp uge i64 -9223372036854775808, %_17
  call void @llvm.assume(i1 %_18)
  %_19 = icmp ule i64 1, %_17
  call void @llvm.assume(i1 %_19)
  call void @__rust_dealloc(ptr %ptr, i64 %_11, i64 %_17) #14
  br label %bb3

bb3:                                              ; preds = %bb2, %bb1
  ret void
}

; <alloc::ffi::c_str::CString as core::ops::drop::Drop>::drop
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN68_$LT$alloc..ffi..c_str..CString$u20$as$u20$core..ops..drop..Drop$GT$4drop17h7a429437c6d8e0a4E"(ptr align 8 %self) unnamed_addr #1 {
start:
  %0 = getelementptr inbounds { ptr, i64 }, ptr %self, i32 0, i32 0
  %_4.0 = load ptr, ptr %0, align 8, !nonnull !3, !align !9, !noundef !3
  %1 = getelementptr inbounds { ptr, i64 }, ptr %self, i32 0, i32 1
  %_4.1 = load i64, ptr %1, align 8
; call <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked_mut
  %_10 = call ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$17get_unchecked_mut17h0f4bd60d0edd4defE"(i64 0, ptr %_4.0, i64 %_4.1)
  store i8 0, ptr %_10, align 1
  ret void
}

; <std::os::fd::owned::OwnedFd as core::ops::drop::Drop>::drop
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN69_$LT$std..os..fd..owned..OwnedFd$u20$as$u20$core..ops..drop..Drop$GT$4drop17h727ef452e9e8af54E"(ptr align 4 %self) unnamed_addr #1 {
start:
  %_3 = load i32, ptr %self, align 4
  %_2 = call i32 @close(i32 %_3)
  ret void
}

; <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h33a13d81e8d11650E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_18 = alloca { ptr, i64 }, align 8
  %_17 = alloca %"core::ptr::metadata::PtrRepr<[u8]>", align 8
  %0 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
; call core::ptr::mut_ptr::<impl *mut T>::is_null
  %_8 = call zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17hc8c85d3458796f4dE"(ptr %self1)
  %_7 = xor i1 %_8, true
  call void @llvm.assume(i1 %_7)
  %1 = getelementptr inbounds %"alloc::vec::Vec<u8>", ptr %self, i32 0, i32 1
  %len = load i64, ptr %1, align 8
  store ptr %self1, ptr %_18, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  store i64 %len, ptr %2, align 8
  %3 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 0
  %4 = load ptr, ptr %3, align 8
  %5 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  %6 = load i64, ptr %5, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  store ptr %4, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  store i64 %6, ptr %8, align 8
  %9 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  %_2.0 = load ptr, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  %_2.1 = load i64, ptr %10, align 8
  ret void
}

; <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h512452bd996d1bb9E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_18 = alloca { ptr, i64 }, align 8
  %_17 = alloca %"core::ptr::metadata::PtrRepr<[alloc::boxed::Box<dyn core::ops::function::FnMut() -> core::result::Result<(), std::io::error::Error> + core::marker::Send + core::marker::Sync>]>", align 8
  %0 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
; call core::ptr::mut_ptr::<impl *mut T>::is_null
  %_8 = call zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17h1c7864747c3c4ee2E"(ptr %self1)
  %_7 = xor i1 %_8, true
  call void @llvm.assume(i1 %_7)
  %1 = getelementptr inbounds %"alloc::vec::Vec<alloc::boxed::Box<dyn core::ops::function::FnMut() -> core::result::Result<(), std::io::error::Error> + core::marker::Send + core::marker::Sync>>", ptr %self, i32 0, i32 1
  %len = load i64, ptr %1, align 8
  store ptr %self1, ptr %_18, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  store i64 %len, ptr %2, align 8
  %3 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 0
  %4 = load ptr, ptr %3, align 8
  %5 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  %6 = load i64, ptr %5, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  store ptr %4, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  store i64 %6, ptr %8, align 8
  %9 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  %_2.0 = load ptr, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  %_2.1 = load i64, ptr %10, align 8
; call core::ptr::drop_in_place<[alloc::boxed::Box<dyn core::ops::function::FnMut<()>+Output = core::result::Result<(),std::io::error::Error>+core::marker::Send+core::marker::Sync>]>
  call void @"_ZN4core3ptr238drop_in_place$LT$$u5b$alloc..boxed..Box$LT$dyn$u20$core..ops..function..FnMut$LT$$LP$$RP$$GT$$u2b$Output$u20$$u3d$$u20$core..result..Result$LT$$LP$$RP$$C$std..io..error..Error$GT$$u2b$core..marker..Send$u2b$core..marker..Sync$GT$$u5d$$GT$17h5bdd31d476922e16E"(ptr %_2.0, i64 %_2.1)
  ret void
}

; <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h9edb5011854831adE"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_18 = alloca { ptr, i64 }, align 8
  %_17 = alloca %"core::ptr::metadata::PtrRepr<[alloc::ffi::c_str::CString]>", align 8
  %0 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
; call core::ptr::mut_ptr::<impl *mut T>::is_null
  %_8 = call zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17h0ad6e4575a979414E"(ptr %self1)
  %_7 = xor i1 %_8, true
  call void @llvm.assume(i1 %_7)
  %1 = getelementptr inbounds %"alloc::vec::Vec<alloc::ffi::c_str::CString>", ptr %self, i32 0, i32 1
  %len = load i64, ptr %1, align 8
  store ptr %self1, ptr %_18, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  store i64 %len, ptr %2, align 8
  %3 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 0
  %4 = load ptr, ptr %3, align 8
  %5 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  %6 = load i64, ptr %5, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  store ptr %4, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  store i64 %6, ptr %8, align 8
  %9 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  %_2.0 = load ptr, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  %_2.1 = load i64, ptr %10, align 8
; call core::ptr::drop_in_place<[alloc::ffi::c_str::CString]>
  call void @"_ZN4core3ptr57drop_in_place$LT$$u5b$alloc..ffi..c_str..CString$u5d$$GT$17h66826af11cfec2b6E"(ptr %_2.0, i64 %_2.1)
  ret void
}

; <alloc::vec::Vec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN70_$LT$alloc..vec..Vec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17hd517229758d55e44E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_18 = alloca { ptr, i64 }, align 8
  %_17 = alloca %"core::ptr::metadata::PtrRepr<[*const i8]>", align 8
  %0 = getelementptr inbounds { i64, ptr }, ptr %self, i32 0, i32 1
  %self1 = load ptr, ptr %0, align 8, !nonnull !3, !noundef !3
; call core::ptr::mut_ptr::<impl *mut T>::is_null
  %_8 = call zeroext i1 @"_ZN4core3ptr7mut_ptr31_$LT$impl$u20$$BP$mut$u20$T$GT$7is_null17h1f4110d3e6493e0eE"(ptr %self1)
  %_7 = xor i1 %_8, true
  call void @llvm.assume(i1 %_7)
  %1 = getelementptr inbounds %"alloc::vec::Vec<*const i8>", ptr %self, i32 0, i32 1
  %len = load i64, ptr %1, align 8
  store ptr %self1, ptr %_18, align 8
  %2 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  store i64 %len, ptr %2, align 8
  %3 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 0
  %4 = load ptr, ptr %3, align 8
  %5 = getelementptr inbounds { ptr, i64 }, ptr %_18, i32 0, i32 1
  %6 = load i64, ptr %5, align 8
  %7 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  store ptr %4, ptr %7, align 8
  %8 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  store i64 %6, ptr %8, align 8
  %9 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 0
  %_2.0 = load ptr, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i64 }, ptr %_17, i32 0, i32 1
  %_2.1 = load i64, ptr %10, align 8
  ret void
}

; <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked
; Function Attrs: inlinehint nonlazybind uwtable
define internal ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$13get_unchecked17hded7e697effab513E"(i64 %self, ptr %slice.0, i64 %slice.1) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %1 = getelementptr inbounds ptr, ptr %slice.0, i64 %self
  store ptr %1, ptr %0, align 8
  %2 = load ptr, ptr %0, align 8
  ret ptr %2
}

; <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked_mut
; Function Attrs: inlinehint nonlazybind uwtable
define internal ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$17get_unchecked_mut17h0f4bd60d0edd4defE"(i64 %self, ptr %slice.0, i64 %slice.1) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %1 = getelementptr inbounds i8, ptr %slice.0, i64 %self
  store ptr %1, ptr %0, align 8
  %_15 = load ptr, ptr %0, align 8
  ret ptr %_15
}

; <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked_mut
; Function Attrs: inlinehint nonlazybind uwtable
define internal ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$17get_unchecked_mut17h8fede40c3f413513E"(i64 %self, ptr %slice.0, i64 %slice.1) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %1 = getelementptr inbounds %"core::mem::maybe_uninit::MaybeUninit<std::ffi::os_str::OsString>", ptr %slice.0, i64 %self
  store ptr %1, ptr %0, align 8
  %_15 = load ptr, ptr %0, align 8
  ret ptr %_15
}

; <usize as core::slice::index::SliceIndex<[T]>>::get_unchecked_mut
; Function Attrs: inlinehint nonlazybind uwtable
define internal ptr @"_ZN75_$LT$usize$u20$as$u20$core..slice..index..SliceIndex$LT$$u5b$T$u5d$$GT$$GT$17get_unchecked_mut17hfaa45761f33dde87E"(i64 %self, ptr %slice.0, i64 %slice.1) unnamed_addr #1 {
start:
  %0 = alloca ptr, align 8
  %1 = getelementptr inbounds %"core::mem::maybe_uninit::MaybeUninit<core::option::Option<std::ffi::os_str::OsString>>", ptr %slice.0, i64 %self
  store ptr %1, ptr %0, align 8
  %_15 = load ptr, ptr %0, align 8
  ret ptr %_15
}

; <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h0b69a92fd6af8944E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_2 = alloca %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", align 8
; call alloc::raw_vec::RawVec<T,A>::current_memory
  call void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17ha1bc40faff3957d8E"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %_2, ptr align 8 %self)
  %0 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %_2, i32 0, i32 1
  %1 = load i64, ptr %0, align 8, !range !15, !noundef !3
  %2 = icmp eq i64 %1, 0
  %_4 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_4, 1
  br i1 %3, label %bb2, label %bb4

bb2:                                              ; preds = %start
  %ptr = load ptr, ptr %_2, align 8, !nonnull !3, !noundef !3
  %4 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_2, i32 0, i32 1
  %5 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 0
  %layout.0 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 1
  %layout.1 = load i64, ptr %6, align 8, !range !14, !noundef !3
; call <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  call void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %self, ptr %ptr, i64 %layout.0, i64 %layout.1)
  br label %bb4

bb4:                                              ; preds = %bb2, %start
  ret void
}

; <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h7870d79897905bbcE"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_2 = alloca %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", align 8
; call alloc::raw_vec::RawVec<T,A>::current_memory
  call void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17h4424370672e8098cE"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %_2, ptr align 8 %self)
  %0 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %_2, i32 0, i32 1
  %1 = load i64, ptr %0, align 8, !range !15, !noundef !3
  %2 = icmp eq i64 %1, 0
  %_4 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_4, 1
  br i1 %3, label %bb2, label %bb4

bb2:                                              ; preds = %start
  %ptr = load ptr, ptr %_2, align 8, !nonnull !3, !noundef !3
  %4 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_2, i32 0, i32 1
  %5 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 0
  %layout.0 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 1
  %layout.1 = load i64, ptr %6, align 8, !range !14, !noundef !3
; call <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  call void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %self, ptr %ptr, i64 %layout.0, i64 %layout.1)
  br label %bb4

bb4:                                              ; preds = %bb2, %start
  ret void
}

; <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17h9170bbe0d5240382E"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_2 = alloca %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", align 8
; call alloc::raw_vec::RawVec<T,A>::current_memory
  call void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17hfa40f89bea1349cfE"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %_2, ptr align 8 %self)
  %0 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %_2, i32 0, i32 1
  %1 = load i64, ptr %0, align 8, !range !15, !noundef !3
  %2 = icmp eq i64 %1, 0
  %_4 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_4, 1
  br i1 %3, label %bb2, label %bb4

bb2:                                              ; preds = %start
  %ptr = load ptr, ptr %_2, align 8, !nonnull !3, !noundef !3
  %4 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_2, i32 0, i32 1
  %5 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 0
  %layout.0 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 1
  %layout.1 = load i64, ptr %6, align 8, !range !14, !noundef !3
; call <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  call void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %self, ptr %ptr, i64 %layout.0, i64 %layout.1)
  br label %bb4

bb4:                                              ; preds = %bb2, %start
  ret void
}

; <alloc::raw_vec::RawVec<T,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN77_$LT$alloc..raw_vec..RawVec$LT$T$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17ha6e917ec4bc18abdE"(ptr align 8 %self) unnamed_addr #0 {
start:
  %_2 = alloca %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", align 8
; call alloc::raw_vec::RawVec<T,A>::current_memory
  call void @"_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$14current_memory17hfebd549caca97675E"(ptr sret(%"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>") %_2, ptr align 8 %self)
  %0 = getelementptr inbounds %"core::option::Option<(core::ptr::non_null::NonNull<u8>, core::alloc::layout::Layout)>", ptr %_2, i32 0, i32 1
  %1 = load i64, ptr %0, align 8, !range !15, !noundef !3
  %2 = icmp eq i64 %1, 0
  %_4 = select i1 %2, i64 0, i64 1
  %3 = icmp eq i64 %_4, 1
  br i1 %3, label %bb2, label %bb4

bb2:                                              ; preds = %start
  %ptr = load ptr, ptr %_2, align 8, !nonnull !3, !noundef !3
  %4 = getelementptr inbounds { ptr, { i64, i64 } }, ptr %_2, i32 0, i32 1
  %5 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 0
  %layout.0 = load i64, ptr %5, align 8
  %6 = getelementptr inbounds { i64, i64 }, ptr %4, i32 0, i32 1
  %layout.1 = load i64, ptr %6, align 8, !range !14, !noundef !3
; call <alloc::alloc::Global as core::alloc::Allocator>::deallocate
  call void @"_ZN63_$LT$alloc..alloc..Global$u20$as$u20$core..alloc..Allocator$GT$10deallocate17hec2f2c168cbfcddeE"(ptr align 1 %self, ptr %ptr, i64 %layout.0, i64 %layout.1)
  br label %bb4

bb4:                                              ; preds = %bb2, %start
  ret void
}

; <std::io::error::repr_bitpacked::Repr as core::ops::drop::Drop>::drop
; Function Attrs: inlinehint nonlazybind uwtable
define internal void @"_ZN78_$LT$std..io..error..repr_bitpacked..Repr$u20$as$u20$core..ops..drop..Drop$GT$4drop17ha28e326fb8f179ebE"(ptr align 8 %self) unnamed_addr #1 {
start:
  %_2 = alloca %"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>", align 8
  %_3 = load ptr, ptr %self, align 8, !nonnull !3, !noundef !3
; call std::io::error::repr_bitpacked::decode_repr
  call void @_ZN3std2io5error14repr_bitpacked11decode_repr17h65ad2d8cd0204a41E(ptr sret(%"std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>") %_2, ptr %_3)
; call core::ptr::drop_in_place<std::io::error::ErrorData<alloc::boxed::Box<std::io::error::Custom>>>
  call void @"_ZN4core3ptr101drop_in_place$LT$std..io..error..ErrorData$LT$alloc..boxed..Box$LT$std..io..error..Custom$GT$$GT$$GT$17h7ea2541984c01b00E"(ptr %_2)
  ret void
}

; <std::io::error::repr_bitpacked::Repr as core::ops::drop::Drop>::drop::{{closure}}
; Function Attrs: inlinehint nonlazybind uwtable
define internal align 8 ptr @"_ZN78_$LT$std..io..error..repr_bitpacked..Repr$u20$as$u20$core..ops..drop..Drop$GT$4drop28_$u7b$$u7b$closure$u7d$$u7d$17hf09f9958e8268f64E"(ptr %p) unnamed_addr #1 {
start:
; call alloc::boxed::Box<T,A>::from_raw_in
  %0 = call align 8 ptr @"_ZN5alloc5boxed16Box$LT$T$C$A$GT$11from_raw_in17h3a80057e5a6ecbadE"(ptr %p)
  ret ptr %0
}

; <alloc::collections::btree::map::BTreeMap<K,V,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN99_$LT$alloc..collections..btree..map..BTreeMap$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17he33172d07d554a1cE"(ptr align 8 %self) unnamed_addr #0 {
start:
  %slot = alloca %"core::mem::manually_drop::ManuallyDrop<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %self1 = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %tmp = alloca %"core::mem::maybe_uninit::MaybeUninit<alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>>", align 8
  %_3 = alloca %"alloc::collections::btree::map::BTreeMap<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  %_x = alloca %"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>", align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %tmp, ptr align 8 %self, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %self1, ptr align 8 %tmp, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %slot, ptr align 8 %self1, i64 24, i1 false)
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_3, ptr align 8 %slot, i64 24, i1 false)
; call <alloc::collections::btree::map::BTreeMap<K,V,A> as core::iter::traits::collect::IntoIterator>::into_iter
  call void @"_ZN119_$LT$alloc..collections..btree..map..BTreeMap$LT$K$C$V$C$A$GT$$u20$as$u20$core..iter..traits..collect..IntoIterator$GT$9into_iter17h2b31531af56d7fb4E"(ptr sret(%"alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>>") %_x, ptr %_3)
; call core::ptr::drop_in_place<alloc::collections::btree::map::IntoIter<std::ffi::os_str::OsString,core::option::Option<std::ffi::os_str::OsString>>>
  call void @"_ZN4core3ptr152drop_in_place$LT$alloc..collections..btree..map..IntoIter$LT$std..ffi..os_str..OsString$C$core..option..Option$LT$std..ffi..os_str..OsString$GT$$GT$$GT$17h4d79f7da3a3c05bcE"(ptr %_x)
  ret void
}

; <alloc::collections::btree::map::IntoIter<K,V,A> as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
define internal void @"_ZN99_$LT$alloc..collections..btree..map..IntoIter$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$4drop17hd8dee439ba401171E"(ptr align 8 %self) unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_9 = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %guard = alloca ptr, align 8
  %kv = alloca %"alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>", align 8
  %_2 = alloca %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", align 8
  br label %bb1

bb1:                                              ; preds = %bb4, %start
; call alloc::collections::btree::map::IntoIter<K,V,A>::dying_next
  call void @"_ZN5alloc11collections5btree3map25IntoIter$LT$K$C$V$C$A$GT$10dying_next17h53e185146a41908aE"(ptr sret(%"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>") %_2, ptr align 8 %self)
  %1 = getelementptr inbounds %"core::option::Option<alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying, std::ffi::os_str::OsString, core::option::Option<std::ffi::os_str::OsString>, alloc::collections::btree::node::marker::LeafOrInternal>, alloc::collections::btree::node::marker::KV>>", ptr %_2, i32 0, i32 1
  %2 = load ptr, ptr %1, align 8
  %3 = ptrtoint ptr %2 to i64
  %4 = icmp eq i64 %3, 0
  %_4 = select i1 %4, i64 0, i64 1
  %5 = icmp eq i64 %_4, 1
  br i1 %5, label %bb3, label %bb5

bb3:                                              ; preds = %bb1
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %kv, ptr align 8 %_2, i64 24, i1 false)
  store ptr %self, ptr %guard, align 8
  call void @llvm.memcpy.p0.p0.i64(ptr align 8 %_9, ptr align 8 %kv, i64 24, i1 false)
; invoke alloc::collections::btree::node::Handle<alloc::collections::btree::node::NodeRef<alloc::collections::btree::node::marker::Dying,K,V,NodeType>,alloc::collections::btree::node::marker::KV>::drop_key_val
  invoke void @"_ZN5alloc11collections5btree4node173Handle$LT$alloc..collections..btree..node..NodeRef$LT$alloc..collections..btree..node..marker..Dying$C$K$C$V$C$NodeType$GT$$C$alloc..collections..btree..node..marker..KV$GT$12drop_key_val17h20aa62e5ca1dbd04E"(ptr %_9)
          to label %bb4 unwind label %cleanup

bb5:                                              ; preds = %bb1
  ret void

bb8:                                              ; preds = %cleanup
  br i1 true, label %bb7, label %bb6

cleanup:                                          ; preds = %bb3
  %6 = landingpad { ptr, i32 }
          cleanup
  %7 = extractvalue { ptr, i32 } %6, 0
  %8 = extractvalue { ptr, i32 } %6, 1
  %9 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %7, ptr %9, align 8
  %10 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %8, ptr %10, align 8
  br label %bb8

bb4:                                              ; preds = %bb3
  br label %bb1

bb6:                                              ; preds = %bb7, %bb8
  %11 = load ptr, ptr %0, align 8
  %12 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %13 = load i32, ptr %12, align 8
  %14 = insertvalue { ptr, i32 } undef, ptr %11, 0
  %15 = insertvalue { ptr, i32 } %14, i32 %13, 1
  resume { ptr, i32 } %15

bb7:                                              ; preds = %bb8
; invoke core::ptr::drop_in_place<<alloc::collections::btree::map::IntoIter<K,V,A> as core::ops::drop::Drop>::drop::DropGuard<std::ffi::os_str::OsString,core::option::Option<std::ffi::os_str::OsString>,alloc::alloc::Global>>
  invoke void @"_ZN4core3ptr250drop_in_place$LT$$LT$alloc..collections..btree..map..IntoIter$LT$K$C$V$C$A$GT$$u20$as$u20$core..ops..drop..Drop$GT$..drop..DropGuard$LT$std..ffi..os_str..OsString$C$core..option..Option$LT$std..ffi..os_str..OsString$GT$$C$alloc..alloc..Global$GT$$GT$17h5382c191cab528c6E"(ptr %guard) #12
          to label %bb6 unwind label %abort

abort:                                            ; preds = %bb7
  %16 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable
}

; rust::main
; Function Attrs: nonlazybind uwtable
define internal void @_ZN4rust4main17h4641d0cbfaf06b6cE() unnamed_addr #0 personality ptr @rust_eh_personality {
start:
  %0 = alloca { ptr, i32 }, align 8
  %_8 = alloca %"std::process::Command", align 8
  %_2 = alloca %"core::result::Result<std::process::Output, std::io::error::Error>", align 8
  %output = alloca %"std::process::Output", align 8
; call std::process::Command::new
  call void @_ZN3std7process7Command3new17h01276f3f68ec9555E(ptr sret(%"std::process::Command") %_8, ptr align 1 @alloc151, i64 2)
; invoke std::process::Command::arg
  %_6 = invoke align 8 ptr @_ZN3std7process7Command3arg17h138386b456ed717fE(ptr align 8 %_8, ptr align 1 @alloc152, i64 2)
          to label %bb2 unwind label %cleanup

bb8:                                              ; preds = %cleanup
; invoke core::ptr::drop_in_place<std::process::Command>
  invoke void @"_ZN4core3ptr42drop_in_place$LT$std..process..Command$GT$17hea50fcb0e002b880E"(ptr %_8) #12
          to label %bb9 unwind label %abort

cleanup:                                          ; preds = %bb4, %bb3, %bb2, %start
  %1 = landingpad { ptr, i32 }
          cleanup
  %2 = extractvalue { ptr, i32 } %1, 0
  %3 = extractvalue { ptr, i32 } %1, 1
  %4 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 0
  store ptr %2, ptr %4, align 8
  %5 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  store i32 %3, ptr %5, align 8
  br label %bb8

bb2:                                              ; preds = %start
; invoke std::process::Command::arg
  %_4 = invoke align 8 ptr @_ZN3std7process7Command3arg17h138386b456ed717fE(ptr align 8 %_6, ptr align 1 @alloc153, i64 10)
          to label %bb3 unwind label %cleanup

bb3:                                              ; preds = %bb2
; invoke std::process::Command::output
  invoke void @_ZN3std7process7Command6output17h56d71a286f166780E(ptr sret(%"core::result::Result<std::process::Output, std::io::error::Error>") %_2, ptr align 8 %_4)
          to label %bb4 unwind label %cleanup

bb4:                                              ; preds = %bb3
; invoke core::result::Result<T,E>::expect
  invoke void @"_ZN4core6result19Result$LT$T$C$E$GT$6expect17h97ef44f93ad4a754E"(ptr sret(%"std::process::Output") %output, ptr %_2, ptr align 1 @alloc154, i64 25, ptr align 8 @alloc156)
          to label %bb5 unwind label %cleanup

bb5:                                              ; preds = %bb4
; call core::ptr::drop_in_place<std::process::Command>
  call void @"_ZN4core3ptr42drop_in_place$LT$std..process..Command$GT$17hea50fcb0e002b880E"(ptr %_8)
; call core::ptr::drop_in_place<std::process::Output>
  call void @"_ZN4core3ptr41drop_in_place$LT$std..process..Output$GT$17ha110aab2eef55a12E"(ptr %output)
  ret void

abort:                                            ; preds = %bb8
  %6 = landingpad { ptr, i32 }
          cleanup
; call core::panicking::panic_no_unwind
  call void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() #13
  unreachable

bb9:                                              ; preds = %bb8
  %7 = load ptr, ptr %0, align 8
  %8 = getelementptr inbounds { ptr, i32 }, ptr %0, i32 0, i32 1
  %9 = load i32, ptr %8, align 8
  %10 = insertvalue { ptr, i32 } undef, ptr %7, 0
  %11 = insertvalue { ptr, i32 } %10, i32 %9, 1
  resume { ptr, i32 } %11
}

; Function Attrs: argmemonly nocallback nofree nounwind willreturn
declare void @llvm.memcpy.p0.p0.i64(ptr noalias nocapture writeonly, ptr noalias nocapture readonly, i64, i1 immarg) #4

; Function Attrs: nonlazybind uwtable
declare i32 @rust_eh_personality(i32, i32, i64, ptr, ptr) unnamed_addr #0

; core::panicking::panic
; Function Attrs: cold noinline noreturn nonlazybind uwtable
declare void @_ZN4core9panicking5panic17h545818946343732bE(ptr align 1, i64, ptr align 8) unnamed_addr #5

; std::rt::lang_start_internal
; Function Attrs: nonlazybind uwtable
declare i64 @_ZN3std2rt19lang_start_internal17hd16e6ff7bf05a444E(ptr align 1, ptr align 8, i64, ptr, i8) unnamed_addr #0

; std::sys::unix::process::process_common::Command::arg
; Function Attrs: nonlazybind uwtable
declare void @_ZN3std3sys4unix7process14process_common7Command3arg17hcd35fd9c76fd3186E(ptr align 8, ptr align 1, i64) unnamed_addr #0

; std::sys::unix::process::process_common::Command::new
; Function Attrs: nonlazybind uwtable
declare void @_ZN3std3sys4unix7process14process_common7Command3new17h2ed3339155d090bcE(ptr sret(%"std::sys::unix::process::process_common::Command"), ptr align 1, i64) unnamed_addr #0

; core::panicking::panic_no_unwind
; Function Attrs: cold noinline noreturn nounwind nonlazybind uwtable
declare void @_ZN4core9panicking15panic_no_unwind17h92a6926e5435022fE() unnamed_addr #6

; <alloc::collections::btree::mem::replace::PanicGuard as core::ops::drop::Drop>::drop
; Function Attrs: nonlazybind uwtable
declare void @"_ZN93_$LT$alloc..collections..btree..mem..replace..PanicGuard$u20$as$u20$core..ops..drop..Drop$GT$4drop17h1d57ee459902bc41E"(ptr align 1) unnamed_addr #0

; Function Attrs: inaccessiblememonly nocallback nofree nosync nounwind willreturn
declare void @llvm.assume(i1 noundef) #7

; Function Attrs: nocallback nofree nosync nounwind readnone willreturn
declare i1 @llvm.expect.i1(i1, i1) #8

; <std::io::error::Error as core::fmt::Debug>::fmt
; Function Attrs: nonlazybind uwtable
declare zeroext i1 @"_ZN58_$LT$std..io..error..Error$u20$as$u20$core..fmt..Debug$GT$3fmt17hd6baae1594d2a2f7E"(ptr align 8, ptr align 8) unnamed_addr #0

; core::result::unwrap_failed
; Function Attrs: cold noinline noreturn nonlazybind uwtable
declare void @_ZN4core6result13unwrap_failed17h62abf61e411aaa08E(ptr align 1, i64, ptr align 1, ptr align 8, ptr align 8) unnamed_addr #5

; Function Attrs: nounwind nonlazybind allockind("free") uwtable
declare void @__rust_dealloc(ptr allocptr, i64, i64) unnamed_addr #9

; Function Attrs: nonlazybind uwtable
declare i32 @close(i32) unnamed_addr #0

; std::process::Command::output
; Function Attrs: nonlazybind uwtable
declare void @_ZN3std7process7Command6output17h56d71a286f166780E(ptr sret(%"core::result::Result<std::process::Output, std::io::error::Error>"), ptr align 8) unnamed_addr #0

; Function Attrs: nonlazybind
define i32 @main(i32 %0, ptr %1) unnamed_addr #10 {
top:
  %2 = sext i32 %0 to i64
; call std::rt::lang_start
  %3 = call i64 @_ZN3std2rt10lang_start17h95a848735ce62164E(ptr @_ZN4rust4main17h4641d0cbfaf06b6cE, i64 %2, ptr %1, i8 0)
  %4 = trunc i64 %3 to i32
  ret i32 %4
}

attributes #0 = { nonlazybind uwtable "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #1 = { inlinehint nonlazybind uwtable "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #2 = { noinline nonlazybind uwtable "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #3 = { inlinehint noreturn nonlazybind uwtable "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #4 = { argmemonly nocallback nofree nounwind willreturn }
attributes #5 = { cold noinline noreturn nonlazybind uwtable "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #6 = { cold noinline noreturn nounwind nonlazybind uwtable "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #7 = { inaccessiblememonly nocallback nofree nosync nounwind willreturn }
attributes #8 = { nocallback nofree nosync nounwind readnone willreturn }
attributes #9 = { nounwind nonlazybind allockind("free") uwtable "alloc-family"="__rust_alloc" "probe-stack"="__rust_probestack" "target-cpu"="x86-64" }
attributes #10 = { nonlazybind "target-cpu"="x86-64" }
attributes #11 = { noreturn }
attributes #12 = { noinline }
attributes #13 = { noinline noreturn nounwind }
attributes #14 = { nounwind }

!llvm.module.flags = !{!0, !1, !2}

!0 = !{i32 7, !"PIC Level", i32 2}
!1 = !{i32 7, !"PIE Level", i32 2}
!2 = !{i32 2, !"RtLibUseGOT", i32 1}
!3 = !{}
!4 = !{i64 8}
!5 = !{i32 2595227}
!6 = !{i8 0, i8 42}
!7 = !{i8 0, i8 2}
!8 = !{i8 0, i8 41}
!9 = !{i64 1}
!10 = !{i8 0, i8 4}
!11 = !{i32 0, i32 4}
!12 = !{i8 0, i8 3}
!13 = !{i32 0, i32 5}
!14 = !{i64 1, i64 -9223372036854775807}
!15 = !{i64 0, i64 -9223372036854775807}
!16 = !{i64 0, i64 2}
!17 = !{i64 0, i64 3}
!18 = !{i64 1, i64 0}
