// predicates 
// &, |, ^, !, ==, !=, <, <=, >, >=


#include <stdio.h>

int test_preds_complement();
int test_preds();

int main(void)
{
	int res1;
	int res2;

	res1 = 0;
	res2 = 0;

	res1 = test_preds_complement();
    printf("%d \n", res1);

    res2 = test_preds();
    printf("%d \n", res2);

    return 0;
}

int test_preds_complement()
{
  int a = 5;
  int b = 3;
  int c = 0, d = 0, e = 0, f = 0, g = 0;

  c = (a & b);  // c = 1
  d = (a | b);  // d = 7
  e = (a ^ b);  // e = 6
  b = !a;       // b = 0
  g = ~f;       // 1's complement, ~0=(-1)=0xffffffff
  
  return (c+d+e+b+g); // 13
}

int test_preds()
{
  int a = 5;
  int b = 3;
  int c, d, e, f, g, h;
  
  c = (a == b); // seq, c = 0
  d = (a != b); // sne, d = 1
  e = (a < b);  // slt, e = 0
  f = (a <= b); // sle, f = 0
  g = (a > b);  // sgt, g = 1
  h = (a >= b); // sge, g = 1
  
  return (c+d+e+f+g+h); // 3
}