#pragma once
#pragma once
#include <unordered_map>
#include <string>

class ILDataManager
{

public:

	struct keywordData
	{
	public:
		int lineIndex;
		int wordStart;
		int wordEnd;
	};

	struct functionData
	{
		int functionBodyStartLine;
		int functionReturnLine;

		std::string prefix;
		std::string command;
		std::string type;
		std::string value;
		std::string suffix;

	public:
		std::string getFormattedReturnLine()
		{
			return prefix + command + " " + type + " " + value + suffix;
		}
	};

	// Data 

	std::unordered_map<std::string, std::vector<keywordData>> lookupTable = {

		// Local variables prefixed by %, global prefixed by @, metadata by !

		{"@.str", {}},
		{"declare", {}},
		{"define", {}},
		{"call", {}},
		{"ret",{}},
		{"!", {}},
		{"alloca", {}},
		{"store", {}},
		{"load", {}},
		{"call", {}},
		{"nsw", {}},
		{"icmp", {}},
		{"br", {}},
		{"label", {}},
		{"add", {}},
		{"sub", {}},
		{"mul", {}},
		{"sdiv", {}},
		{"srem", {}},
		{"eq", {}},
		{"ne", {}},
		{"slt", {}},
		{"sgt", {}},
		{"and", {}},
		{"or", {}},
		{"xor", {}}
	};

	std::vector<std::string> IlLines;
	std::vector<functionData> FunctionDataVector;

	// Methods

	void Initialize(std::string filePath);

private:
	void getFunctionDataVec();
	void ExtractILFromFile(std::string filePath);



};

