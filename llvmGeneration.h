#pragma once
#include <string>
#include <format>

class llvmGeneration
{

public:

	static std::string dumpStackVariable(const char* var);
	static std::string dumpAssignment(const char* var, const char* src);
	static std::string dumpLoad(int receiver, const char* var);
	static std::string dumpRead(const char* id);
	static std::string dumpNSWOperation(const char* reciever, const char* a, const char* b, const char* oper);
	static std::string dumpOperation(const char* reciever, const char* a, const char* b, const char* oper);
	static std::string dumpBooleanOperation(const char* receiver, const char* a, const char* b, const char* oper);
	static std::string dumpEqualsOperation(const char* reciever, const char* a, const char* b, const char* operation);
	static std::string dumpLabel(const char* name);
	static std::string dumpBranch(const char* booleanValue, const char* iftrue, const char* iffalse);
	static std::string dumpImmediateBranch(const char* label);
	static std::string dumpAddition(const char* reciever, const char* a, const char* b);
	static std::string dumpSubtraction(const char* reciever, const char* a, const char* b);
	static std::string dumpMultiplication(const char* reciever, const char* a, const char* b);
	static std::string dumpDivision(const char* reciever, const char* a, const char* b);
	static std::string dumpRemainder(const char* reciever, const char* a, const char* b);
	static std::string dumpEquals(const char* reciever, const char* a, const char* b);
	static std::string dumpNotEquals(const char* reciever, const char* a, const char* b);
	static std::string dumpLessThan(const char* reciever, const char* a, const char* b);
	static std::string dumpGreaterThan(const char* reciever, const char* a, const char* b);
	static std::string dumpAnd(const char* reciever, const char* a, const char* b);
	static std::string dumpOr(const char* reciever, const char* a, const char* b);
	static std::string dumpXor(const char* reciever, const char* a, const char* b);
	static std::string dumpPrintValue(const char* value);
	static std::string dumpFunctionPrefix(const char* name);
	static std::string dumpParameter(bool first);
	static std::string dumpFunctionStart();
	static std::string dumpFunctionEnd();
	static std::string dumpReturn(const char* expression);
	static std::string dumpCallPrefix(const char* receiver, const char* name);
	static std::string dumpCallArgument(const char* value);
	static std::string dumpCallComma();
	static std::string dumpCallSuffix();

};

