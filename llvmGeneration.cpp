#include "llvmGeneration.h"

std::string llvmGeneration::dumpStackVariable(const char* var) {
    return std::format("  {:s} = alloca i32, align 4", var);
}

std::string llvmGeneration::dumpAssignment(const char* var, const char* src) {
    return std::format("  store i32 {:s}, i32* {:s}, align 4", src, var);
}

std::string llvmGeneration::dumpLoad(int receiver, const char* var) {
    return std::format("  %{:d} = load i32, i32* {:s}", receiver, var);
}

std::string llvmGeneration::dumpRead(const char* id) {
    return std::format("  {:s} = call i32 @read_integer()", id);
}

std::string llvmGeneration::dumpNSWOperation(const char* reciever, const char* a, const char* b, const char* oper) {
    return std::format("  {:s} = {:s} nsw i32 {:s}, {:s}",
        reciever,
        oper,
        a,
        b
    );
}

std::string llvmGeneration::dumpOperation(const char* reciever, const char* a, const char* b, const char* oper) {
    return std::format("  {:s} = {:s} i32 {:s}, {:s}",
        reciever,
        oper,
        a,
        b
    );
}

std::string llvmGeneration::dumpBooleanOperation(const char* receiver, const char* a, const char* b, const char* oper) {
    return std::format("  {:s} = {:s} i1 {:s}, {:s}", receiver, oper, a, b);
}

std::string llvmGeneration::dumpEqualsOperation(const char* reciever, const char* a, const char* b, const char* operation) {
    return std::format("  {:s} = icmp {:s} i32 {:s}, {:s}", reciever, operation, a, b);
}

std::string llvmGeneration::dumpLabel(const char* name) {
    return std::format("  {:s}:", name);
}

std::string llvmGeneration::dumpBranch(const char* booleanValue, const char* iftrue, const char* iffalse) {
    return std::format("  br i1 {:s}, label %{:s}, label %{:s}", booleanValue, iftrue, iffalse);
}

std::string llvmGeneration::dumpImmediateBranch(const char* label) {
    return std::format("  br label %{:s}", label);
}

std::string llvmGeneration::dumpAddition(const char* reciever, const char* a, const char* b) { return dumpNSWOperation(reciever, a, b, "add"); }
std::string llvmGeneration::dumpSubtraction(const char* reciever, const char* a, const char* b) { return dumpNSWOperation(reciever, a, b, "sub"); }
std::string llvmGeneration::dumpMultiplication(const char* reciever, const char* a, const char* b) { return dumpNSWOperation(reciever, a, b, "mul"); }
std::string llvmGeneration::dumpDivision(const char* reciever, const char* a, const char* b) { return dumpOperation(reciever, a, b, "sdiv"); }
std::string llvmGeneration::dumpRemainder(const char* reciever, const char* a, const char* b) { return dumpOperation(reciever, a, b, "srem"); }
std::string llvmGeneration::dumpEquals(const char* reciever, const char* a, const char* b) { return dumpEqualsOperation(reciever, a, b, "eq"); }
std::string llvmGeneration::dumpNotEquals(const char* reciever, const char* a, const char* b) { return dumpEqualsOperation(reciever, a, b, "ne"); }
std::string llvmGeneration::dumpLessThan(const char* reciever, const char* a, const char* b) { return dumpEqualsOperation(reciever, a, b, "slt"); }
std::string llvmGeneration::dumpGreaterThan(const char* reciever, const char* a, const char* b) { return dumpEqualsOperation(reciever, a, b, "sgt"); }
std::string llvmGeneration::dumpAnd(const char* reciever, const char* a, const char* b) { return dumpBooleanOperation(reciever, a, b, "and"); }
std::string llvmGeneration::dumpOr(const char* reciever, const char* a, const char* b) { return dumpBooleanOperation(reciever, a, b, "or"); }
std::string llvmGeneration::dumpXor(const char* reciever, const char* a, const char* b) { return dumpBooleanOperation(reciever, a, b, "xor"); }

std::string llvmGeneration::dumpPrintValue(const char* value) {
    return std::format("  call void @print_integer(i32 {:s})", value);
}

std::string llvmGeneration::dumpFunctionPrefix(const char* name) {
    return std::format("define i32 @{:s}(", name);
}

std::string llvmGeneration::dumpParameter(bool first) {
    if (!first) std::string(", ");
    return std::string("i32");
}

std::string llvmGeneration::dumpFunctionStart() {
    return std::string(") {");
}

std::string llvmGeneration::dumpFunctionEnd() {
    return std::string("}");
}

std::string llvmGeneration::dumpReturn(const char* expression) {
    return std::format("  ret i32 {:s}", expression);
}

std::string llvmGeneration::dumpCallPrefix(const char* receiver, const char* name) {
    return std::format("  {:s} = call i32 @{:s}(", receiver, name);
}

std::string llvmGeneration::dumpCallArgument(const char* value) {
    return std::format("i32 {:s}", value);
}

std::string llvmGeneration::dumpCallComma() {
    return std::string(", ");
}

std::string llvmGeneration::dumpCallSuffix() {
    return std::string(")");
}