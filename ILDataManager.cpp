#include "ILDataManager.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>


void ILDataManager::Initialize(std::string filePath)
{
    ExtractILFromFile(filePath);
    getFunctionDataVec();
}

void ILDataManager::ExtractILFromFile(std::string filePath)
{
    std::string line;
    std::string word;

    // Count begins at 1 instead of 0 due to whitespace at EOF
    int count = 1;

    // Optional variable denotating where file was chopped
    int split_point = 0;

    // Open input LL file
    std::ifstream input_file(filePath);

    //Copy file lines into IlLines vector
    while (std::getline(input_file, line))
    {
        IlLines.push_back(line);
    }

    input_file.clear();
    input_file.seekg(0, input_file.end);
    std::string file_size_message = "File size: " + std::to_string(input_file.tellg());
    input_file.close();
    std::string num_line_message = "Line numbers: " + std::to_string(IlLines.size());


    // Cap memory on file reads
    if (atoi(file_size_message.c_str()) >= 10485760) {

        // Determine how many parts the file needs to be chopped into
        int required_chops = ((atoi(file_size_message.c_str())) / (10485760) + 1);

        std::cout << "File size exceeds 10MB memory cap. " << "\n" << "Splitting file into " << required_chops << " parts " << std::endl;

        // TODO: chop it up and store relevant data to restoring chopped files in correct order

    }

    // Continue processing
    else {

        // Loop over words and increment values in map
        for (int i = 0; i < IlLines.size(); i++) {

            auto line = IlLines[i];
            std::stringstream stream(line);

            while (stream >> word) {
                auto keyword = lookupTable.find(word);
                if (keyword != lookupTable.end()) {

                    auto keywordEndPosition = stream.tellg();
                    auto keyWordStartPosition = (keywordEndPosition - std::streamoff(keyword->first.length()));

                    keyword->second.push_back(
                        {
                            i,                          // lineIndex
                            (int)keyWordStartPosition,  // wordStart
                            (int)keywordEndPosition     // wordEnd
                        });
                }
            }
        }
    }
}

// Get a list of structs with data about each function and its' return line
void ILDataManager::getFunctionDataVec()
{
    std::vector<functionData> retLines;

    // Loop over ret keyword, prototype parsing logic for ret command

    std::vector<keywordData> returnKeywordDataVec = lookupTable["ret"];
    std::vector<keywordData> defineKeywordDataVec = lookupTable["define"];

    for (keywordData keyword : returnKeywordDataVec)
    {
        auto returnLineNumber = keyword.lineIndex;
        auto wordloc = keyword.wordStart;
        auto wordend = keyword.wordEnd;
        auto currentLine = IlLines[returnLineNumber];

        int functionDefineLineNumber = -1;
        for (keywordData defKeyword : defineKeywordDataVec)
        {
            if (defKeyword.lineIndex > returnLineNumber)
            {
                break;
            }
            functionDefineLineNumber = defKeyword.lineIndex;
        }

        int functionBodyStart = functionDefineLineNumber + 1;

        // Regex match the components of our line: spaces, "ret", type, value, spaces/newline
        std::regex regexp("(.*)(ret)\\s*([\\w\\d]*)\\s*([\\w\\d]*)(.*)");
        std::smatch matches;

        if (!regex_search(currentLine, matches, regexp)) {
            std::cout << "ERROR: no regex match";

            // TODO: throw exception? return?
        }

        // Store the start and end of our function body and the components of our line return line
        functionData functionReturnData = {
            functionBodyStart,
            returnLineNumber,
            matches[1], // prefix
            matches[2], // command
            matches[3], // type
            matches[4], // value
            matches[5]  // suffix
        };


        // Add the function info to the list 
        retLines.push_back(functionReturnData);

    }
    FunctionDataVector = retLines;
}
